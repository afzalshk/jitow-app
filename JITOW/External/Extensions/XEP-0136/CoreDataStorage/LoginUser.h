//
//  LoginUser.h
//  yupApp
//
//  Created by Irfan Malik on 10/16/15.
//  Copyright (c) 2015 LC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface LoginUser : NSManagedObject

@property (nonatomic, retain) NSString * bareJidStr;
@property (nonatomic, retain) NSString * fullname;
@property (nonatomic, retain) NSData * image;
@property (nonatomic, retain) NSString * message;
@property (nonatomic, retain) NSString * pin;
@property (nonatomic, retain) NSString * status;
@property (nonatomic, retain) NSNumber * userid;

@end
