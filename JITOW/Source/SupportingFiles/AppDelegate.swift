//
//  AppDelegate.swift
//  JITOW
//
//  Created by Yosmite on 2/9/16.
//  Copyright © 2016 LC. All rights reserved.
//

import UIKit
import CoreData
import MBProgressHUD
import Fabric
import Crashlytics

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var xmppStream: XMPPStream?
    var xmppcon:XMPPConnection?
    var isOpen: Bool = false
    var password: String = ""

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        setupStream()
        xmppcon?.setnewMessage(xmppStream)
        //testing line of codes
    
        let defaults = NSUserDefaults.standardUserDefaults()
        if let userid = defaults.stringForKey(kUserid)
        {
        
            print(userid)
            let signInVC:SigninViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("SigninViewController") as! SigninViewController
            signInVC.isFromRoot = true
            let navController = UINavigationController(rootViewController: signInVC)

            self.window?.rootViewController = navController
        }
       UINavigationBar.appearance().barTintColor = UIColor(red: 233.0/255, green: 229.0/255, blue: 237.0/255, alpha: 1.0)
//        UINavigationBar.appearance().titleTextAttributes =  [NSForegroundColorAttributeName: UIColor(red: 233.0/255, green: 77.0/255, blue: 88.0/255, alpha: 1.0)]
        
        
        UINavigationBar.appearance().titleTextAttributes =  [NSForegroundColorAttributeName: UIColor.blackColor()]
        
        
        let font:UIFont = UIFont(name: "HelveticaNeue-Medium", size: 12)!
        
        let titleTextAttributes = [NSForegroundColorAttributeName: UIColor(red: 233.0/255, green: 86.0/255, blue: 102.0/255, alpha: 1.0), NSFontAttributeName:font]
        UISegmentedControl.appearance().setTitleTextAttributes(titleTextAttributes, forState: .Selected)
        
        
        let titleTextAttributes2 = [NSForegroundColorAttributeName: UIColor(red: 42.0/255, green: 108.0/255, blue: 140.0/255, alpha: 1.0),NSFontAttributeName:font]
        UISegmentedControl.appearance().setTitleTextAttributes(titleTextAttributes2, forState: .Normal)
        
        
        
        
       
        
        registerForRemoteNot(application)
        
        Fabric.with([Crashlytics.self])
        
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        application.applicationIconBadgeNumber = 0
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    // MARK: - Core Data stack

    lazy var applicationDocumentsDirectory: NSURL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "com.lc.JITOW" in the application's documents Application Support directory.
        let urls = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
        return urls[urls.count-1]
    }()

    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = NSBundle.mainBundle().URLForResource("JITOW", withExtension: "momd")!
        return NSManagedObjectModel(contentsOfURL: modelURL)!
    }()

    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.URLByAppendingPathComponent("SingleViewCoreData.sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: url, options: nil)
        } catch {
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data"
            dict[NSLocalizedFailureReasonErrorKey] = failureReason

            dict[NSUnderlyingErrorKey] = error as! NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        
        return coordinator
    }()

    lazy var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .MainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
        }
    }
    
    
    func setupStream ()
    {
        xmppStream = XMPPStream()
        //assert(xmppStream == nil, "Method setupStream invoked multiple times")
        xmppcon = XMPPConnection()
        xmppcon?.setupEarySetting(xmppStream)
        
        xmppStream!.addDelegate(self, delegateQueue: dispatch_get_main_queue())
    }
    
    func goOffline() {
        let presence = XMPPPresence(type: "unavailable")
        xmppStream!.sendElement(presence)
    }
    
    func goOnline()
    {
        print("goOnline")
        let presence = XMPPPresence(type: "online")
        xmppStream!.sendElement(presence)
        
        NSNotificationCenter.defaultCenter().postNotificationName("userLogined", object: nil)
        
    }
    
    
    func connectXmpp() -> Bool
    {
        let jabberID: String? = NSUserDefaults.standardUserDefaults().objectForKey(kJabberid) as? String
        
        let myPassword: String? = NSUserDefaults.standardUserDefaults().objectForKey(kPassword) as? String
    
        print(jabberID)
        let server: String? = kBaseUrl
        if let stream = xmppStream
        {
            
            print(server)
            if !stream.isDisconnected() {
                
                return true
            }
            
            if jabberID == nil || myPassword == nil{
                print("no jabberID set:" + "\(jabberID)")
                print("no password set:" + "\(myPassword)")
                return false
            }
            stream.myJID = XMPPJID.jidWithString(jabberID)
            password = myPassword!
            do {
                try stream.connectWithTimeout(15)
                
            } catch  {
                let alert = UIAlertController(title: "Alert", message: "Cannot connect to ", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.Default, handler: nil))
                self.window?.rootViewController?.presentViewController(alert, animated: true, completion: nil)
                
                return false
            }
        }
        return true
    }
    
    
    
    func disconnect() {
        goOffline()
        xmppStream!.disconnect()
        //    print("disconnecting")
    }
    
    func xmppStreamConnectDidTimeout(sender: XMPPStream){
    
        var dict = [NSObject: AnyObject]()
        dict["status"] = "timeout"
        
        NSNotificationCenter.defaultCenter().postNotificationName("timeout", object: nil, userInfo: dict)
    }
    

    func xmppStreamDidConnect(sender: XMPPStream)
    {
        print("xmppStreamDidConnect")
        print(password)
        isOpen = true
        do {
            try xmppStream!.authenticateWithPassword(password)
            print("authentification successful")
            
            
        } catch {
            
            
            
        }
    }
    
    func xmppStreamdidNotAuthenticate(sender:XMPPStream, error:DDXMLElement){
        
        print(error.debugDescription)
        
        var dict = [NSObject: AnyObject]()
        dict["status"] = "failed"
        NSNotificationCenter.defaultCenter().postNotificationName("timeout", object: nil, userInfo: dict)
 
    }

    func xmppStreamDidAuthenticate(sender: XMPPStream)
    {
        print("didAuthenticate")
        
        //        NSTimer.scheduledTimerWithTimeInterval(0.2, target: self, selector: Selector("goOnline"), userInfo: nil, repeats:false)
        //
        
        goOnline()
    }
    
    func xmppStream(sender: XMPPStream?, didReceiveMessage: XMPPMessage?)
    {
        if let message:XMPPMessage = didReceiveMessage
        {
            
            if let msg: String = message.elementForName("attachment")?.stringValue()
            {
                print(msg)
                self.xmppcon!.ReciveMessage(sender, didReceiveMessage: didReceiveMessage)
            }
            
            if let msg: String = message.elementForName("body")?.stringValue()
            {
                if let from: String = message.attributeForName("from")?.stringValue()
                {
                    let m: NSMutableDictionary = [:]
                    m["msg"] = msg
                    m["sender"] = from
                    self.xmppcon!.ReciveMessage(sender, didReceiveMessage: didReceiveMessage)
                    print("messageReceived")
                }
            }
                
                
                
            else { return }
            
            
        }
    }

    func xmppStream(sender: XMPPStream?, didReceivePresence: XMPPPresence?)
    {
        
        
        if (didReceivePresence?.toStr() == didReceivePresence?.fromStr())
        {
            print(didReceivePresence?.fromStr())

            
        }
        else
        {
            
            var str :String?
            var fromStr :String?
            
            if let presence = didReceivePresence
            {
                let presenceType = presence.type()
                str = presence.type()
                
                var myStringArr = didReceivePresence?.fromStr().componentsSeparatedByString("@")
                
                fromStr = myStringArr! [0]
                
                
            }
            var dict = [NSObject: AnyObject]()
            dict["status"] = str
            print(fromStr)
            dict["from"] = fromStr
            NSNotificationCenter.defaultCenter().postNotificationName("onlineStatus", object: nil, userInfo: dict)
            
        }
        
//        if let presence = didReceivePresence
//        {
////            let presenceType = presence.type()
////            print(presenceType)
////            let myUsername = sender?.myJID.user
////            print(myUsername)
////            let presenceFromUser = presence.from().user
////            print(presenceFromUser)
//            
//            self.xmppcon!.accceptFriendRequest(presence)
//        }
    }
    
    func getContactList () -> NSFetchedResultsController
    {
        return self.xmppcon!.fetchedResultsController()
    }
    func sendFriendRequest(jid :String ,nickname :String)
    {
        self.xmppcon!.SendFriendRequest(jid, withNickname: nickname)
    }
    func getConversation (jid :String) -> NSArray
    {
        return  self.xmppcon!.loadarchivemsg(jid, andJid: xmppStream, withChatBool:true)
    }
    
    func registerForRemoteNot(application: UIApplication){
        
        let settings: UIUserNotificationSettings = UIUserNotificationSettings( forTypes: [.Badge,.Alert,.Sound], categories: nil )
        application.registerUserNotificationSettings( settings )
        application.registerForRemoteNotifications()
        UIDevice.currentDevice().identifierForVendor!.UUIDString
    }
    
    // Remote Notifications
    
    // Get Device Token
    
    func application( application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData )
    {
        let characterSet: NSCharacterSet = NSCharacterSet( charactersInString: "<>" )
        
        let deviceTokenString: String = ( deviceToken.description as NSString )
            .stringByTrimmingCharactersInSet( characterSet )
            .stringByReplacingOccurrencesOfString( " ", withString: "" ) as String
        print("PushNotif" + deviceTokenString)
        let defaults=NSUserDefaults()
        defaults.setObject(deviceTokenString, forKey:kDeviceToken)
        defaults.synchronize()
    }
    
    func application( application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError )
    {
        print( error.localizedDescription )
    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
        
        if let info = userInfo["aps"] as? Dictionary<String, AnyObject>
        {
            
            if (info["isblocked"] != nil){
                
        NSNotificationCenter.defaultCenter().postNotificationName("reloadVisibleContacts", object: nil)
                
                return
            }
            
            let customNotifVC : CustomNotificationViewController = CustomNotificationViewController(nibName: "CustomNotificationViewController", bundle: nil);
            
            let str:NSString = (info["alert"] as? NSString)!
            customNotifVC.view.tag = 999
            
            let mssgs = str.componentsSeparatedByString(":")
            
            customNotifVC.lbl_name.text = mssgs[0]
            customNotifVC.lbl_message.text = mssgs[1]
            
            
            
            
            self.window?.addSubview(customNotifVC.view)
            self.window?.bringSubviewToFront(customNotifVC.view)
            
            
             NSTimer.scheduledTimerWithTimeInterval(2, target: self, selector: Selector("hideCustomNotifView"), userInfo: nil, repeats:false)
            
            
        }

        
    }

    func hideCustomNotifView()
    {
        for view in self.window!.subviews
        {
            if view.tag == 999
            {
                view.removeFromSuperview()
            }
        }
    }
}

