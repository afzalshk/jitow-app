//
//  CustomCameraView.swift
//  JITOW
//
//  Created by Mac103 on 4/15/16.
//  Copyright © 2016 LC. All rights reserved.
//

import Foundation
import UIKit

protocol CustomOverlayDelegate{
    
    func didCancel(sender:UIButton,overlayView:CustomOverlayView)
    
    func didShoot(sender:UIButton,overlayView:CustomOverlayView)
    
    func didOnOffFlash(sender:UIButton,overlayView:CustomOverlayView)
    
    func clickTextView(sender:UIButton,overlayView:CustomOverlayView)
    
    func moveToFrontCam(sender:UIButton,overlayView:CustomOverlayView)
    
    func previewVideo(sender:UIButton,overlayView:CustomOverlayView)
    
    func moveToShare(sender:UIButton,overlayView:CustomOverlayView)
    
}


class CustomOverlayView: UIView,UITextViewDelegate {
    
    
    var delegate:CustomOverlayDelegate! = nil
    
    @IBOutlet weak var txtViewCaption: UITextView!
    @IBOutlet weak var btnCapture: UIButton!
    
    @IBOutlet weak var lblDuration: UILabel!
   

    @IBAction func btnCapturePressed(sender: UIButton) {
        
        delegate.didShoot(sender, overlayView: self)
        sender.selected = !sender.selected
        
    }
    
    @IBAction func btnCaptionTextPressed(sender: UIButton) {
        
        txtViewCaption.delegate = self
        delegate.clickTextView(sender, overlayView: self)
       
        
    }
    @IBAction func btnFlashPressed(sender: UIButton) {
        
        delegate.didOnOffFlash(sender, overlayView: self)
        sender.selected = !sender.selected
    }
    @IBAction func btnCancelPressed(sender: UIButton) {
        
        delegate.didCancel(sender, overlayView: self)
    }
    @IBAction func btnSharePressed(sender: UIButton) {
        
       delegate.moveToShare(sender, overlayView: self)
    }
    @IBAction func btnChangeDevice(sender: UIButton) {
        
        delegate.moveToFrontCam(sender, overlayView: self)
        
        sender.selected = !sender.selected
        
    }
    // TextView Delegate
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        
        if text == "\n"{
            textView.resignFirstResponder()
            return false
            
        }
        if textView.text.characters.count + (text.characters.count - range.length) == 60{
            
            textView.resignFirstResponder()
            return false

        }
        
        
        return true
        
    }
    @IBAction func btnPreviewPressed(sender: UIButton) {
        
         delegate.previewVideo(sender, overlayView: self)
    }
}