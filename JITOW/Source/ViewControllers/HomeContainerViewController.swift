//
//  HomeContainerViewController.swift
//  JITOW
//
//  Created by Irfan Malik on 2/10/16.
//  Copyright © 2016 LC. All rights reserved.
//

import UIKit

class HomeContainerViewController: UIViewController {
    
    @IBOutlet weak var quickChat_container: UIView!
    @IBOutlet weak var contact_Container: UIView!
   @IBOutlet weak var myprofile_Container: UIView!
    
    @IBOutlet weak var segmentControl: UISegmentedControl!
    
    var isPerformSegue:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        contact_Container.hidden = true
        myprofile_Container.hidden = true
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("relodContactIfVisible"), name:"reloadVisibleContacts", object: nil);
        

        segmentControl.layer.borderColor = UIColor.whiteColor().CGColor
        segmentControl.layer.borderWidth = 0.0;
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(animated: Bool) {
        
        self.navigationController?.navigationBarHidden = true
        
    }
    @IBAction func indexChange(segmentControl: UISegmentedControl)
    {
        switch segmentControl.selectedSegmentIndex
        {
            case 0:
                quickChat_container.hidden = false
                contact_Container.hidden = true
                myprofile_Container.hidden = true
            NSNotificationCenter.defaultCenter().postNotificationName("indexChanged", object: nil)
                
            NSNotificationCenter.defaultCenter().postNotificationName("reloadContactsData", object: nil)
                break;
            case 1:
                
                 NSNotificationCenter.defaultCenter().postNotificationName("indexChanged", object: nil)
                quickChat_container.hidden = true
                contact_Container.hidden = true
                myprofile_Container.hidden = false
                 isPerformSegue = true
            self.performSegueWithIdentifier("toProfileSection", sender: self)
            
                 break;
            
            case 2:
                
                 NSNotificationCenter.defaultCenter().postNotificationName("indexChanged", object: nil)
                 
                 
                quickChat_container.hidden = true
                contact_Container.hidden = false
                myprofile_Container.hidden = true
              isPerformSegue = true
        self.performSegueWithIdentifier("toFriendsMoments", sender: self)
            
                 break;
            
            default:
                break;
        }
    }
    
       override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func shouldPerformSegueWithIdentifier(identifier: String, sender: AnyObject!) -> Bool {
        
        
        if identifier == "toFriendsMoments" || identifier == "toProfileSection"{
            
            if(isPerformSegue){
            
                return true
            }else{
                return false
            }
        }
        return true
    }

    func relodContactIfVisible(){
        
        if quickChat_container.hidden == false{
        NSNotificationCenter.defaultCenter().postNotificationName("reloadContactsData", object: nil)

        }
    }


  
}
