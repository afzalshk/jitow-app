//
//  CommomMethods.swift
//  yupApp
//
//  Created by Mac101 on 7/7/15.
//  Copyright (c) 2015 LC. All rights reserved.
//

import Foundation
import UIKit
import SystemConfiguration

class CommomMethods
{
    
    class func showAlert(text: String ,view :UIViewController)
    {
        dispatch_async(dispatch_get_main_queue()) {
        let alert = UIAlertController(title: "JITOW", message: text, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "ok", style: UIAlertActionStyle.Default, handler: nil))
        view.presentViewController(alert, animated: true, completion: nil)
        }
    }


 class func isValidEmail(testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(testStr)
    }
    
    class func getAppDelegate() -> AppDelegate
    {
         let appDel = UIApplication.sharedApplication().delegate! as! AppDelegate
          return appDel
    }
    
    class func prefixTextField(textfield:UITextField){
        
        let lbl = UILabel(frame: CGRect(x: 0, y: 0, width: 20, height: textfield.frame.size.height))
        lbl.backgroundColor = UIColor.clearColor()
        textfield.leftView = lbl
        textfield.leftViewMode = UITextFieldViewMode.Always
        textfield.contentVerticalAlignment = UIControlContentVerticalAlignment.Center
        
        
        
        
    }
    
    class func getElapsedInterval(date:NSDate) -> String {
        
        var interval = NSCalendar.currentCalendar().components(.Year, fromDate: date, toDate: NSDate(), options: []).year
        
        if interval > 0 {
            return interval == 1 ? "\(interval)" + " " + "y" :
                "\(interval)" + " " + "yrs"
        }
        
        interval = NSCalendar.currentCalendar().components(.Month, fromDate: date, toDate: NSDate(), options: []).month
        if interval > 0 {
            return interval == 1 ? "\(interval)" + " " + "m" :
                "\(interval)" + " " + "m"
        }
        
        interval = NSCalendar.currentCalendar().components(.Day, fromDate: date, toDate: NSDate(), options: []).day
        if interval > 0 {
            return interval == 1 ? "\(interval)" + " " + "d" :
                "\(interval)" + " " + "d"
        }
        
        interval = NSCalendar.currentCalendar().components(.Hour, fromDate: date, toDate: NSDate(), options: []).hour
        if interval > 0 {
            return interval == 1 ? "\(interval)" + " " + "h" :
                "\(interval)" + " " + "hrs"
        }
        
        interval = NSCalendar.currentCalendar().components(.Minute, fromDate: date, toDate: NSDate(), options: []).minute
        if interval > 0 {
            return interval == 1 ? "\(interval)" + " " + "m" :
                "\(interval)" + " " + "mins"
        }
        
        return "now"
    }
    
}




public class Reachabilit
{
    
    class func isConnectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(sizeofValue(zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        let defaultRouteReachability = withUnsafePointer(&zeroAddress) {
            SCNetworkReachabilityCreateWithAddress(nil, UnsafePointer($0))
        }
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
    
    

    
}