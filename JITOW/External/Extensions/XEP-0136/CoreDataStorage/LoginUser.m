//
//  LoginUser.m
//  yupApp
//
//  Created by Irfan Malik on 10/16/15.
//  Copyright (c) 2015 LC. All rights reserved.
//

#import "LoginUser.h"


@implementation LoginUser

@dynamic bareJidStr;
@dynamic fullname;
@dynamic image;
@dynamic message;
@dynamic pin;
@dynamic status;
@dynamic userid;

@end
