//
//  ImageViewerVC.swift
//  JITOW
//
//  Created by Mac103 on 4/13/16.
//  Copyright © 2016 LC. All rights reserved.
//

import Foundation
import SDWebImage

class ImageViewerVC: UIViewController {

    @IBOutlet weak var imgEnlarge: UIImageView!
    var imageUrl:String?
    var imageData:String?
    
    
    override func viewDidLoad() {
        
        imgEnlarge.image = UIImage(named: "chat_imgplacehold")
        
        if imageData == nil {
            
            imgEnlarge.sd_setImageWithURL(NSURL(string: imageUrl!), placeholderImage: UIImage(named: "chat_imgplacehold"))
            
        }
        else{
            
            let imgData = NSData(base64EncodedString:imageData!, options:.IgnoreUnknownCharacters)
            
            imgEnlarge.image = UIImage(data: imgData!)
            
        }
        
        
    }


    @IBAction func btnBackPressed(sender: AnyObject) {
        
        self.dismissViewControllerAnimated(true, completion: nil)
        
    }

}