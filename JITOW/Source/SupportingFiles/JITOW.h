//
//  JITOW.h
//  JITOW
//
//  Created by Yosmite on 2/12/16.
//  Copyright © 2016 LC. All rights reserved.
//

#ifndef JITOW_h
#define JITOW_h

#define kUserid "userid"
#define kUserName "username"
#define kUserPhoneNumber "phoneNumber"
#define kBaseUrl "http://ec2-52-34-41-17.us-west-2.compute.amazonaws.com"
#define kBaseServerUrl "http://ec2-52-34-41-17.us-west-2.compute.amazonaws.com/index.php/"
#define kAppendedUrl "ec2-52-34-41-17.us-west-2.compute.amazonaws.com"
#define kAppendedJidUrl "ip-172-31-17-180"
#define kJabberid "Jid"
#define kDeviceToken "deviceToken"
#define kPassword "password"
#define kSMSCode "smsCode"
#define kxmmpBase "http://ec2-52-34-41-17.us-west-2.compute.amazonaws.com:9090/plugins/restapi/v1/users/"



#endif /* JITOW_h */
