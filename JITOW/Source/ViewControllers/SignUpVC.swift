//
//  SignUpVC.swift
//  JITOW
//
//  Created by Yosmite on 2/10/16.
//  Copyright © 2016 LC. All rights reserved.
//

import Foundation
import UIKit
class SignUpVC: UIViewController ,UITextFieldDelegate {

    @IBOutlet weak var txtDOB: UITextField!
    
    @IBOutlet weak var txtEmail: UITextField!
    
    @IBOutlet weak var txtPhoneNumber: UITextField!
    var datePicker = UIDatePicker()
    
    @IBOutlet weak var btnAgree: UIButton!
    
    @IBOutlet weak var txtTOS: UITextView!
    
    let termsAndConditionsURL = "http://www.jitow.com/terms";
    let privacyURL = "http://www.jitow.com/privacy";
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.txtDOB.delegate = self
        self.txtEmail.delegate = self
       
        self.txtPhoneNumber.delegate = self
        
        self.txtDOB.layer.cornerRadius = 5.0
        self.txtDOB.clipsToBounds = true
        
        self.txtEmail.layer.cornerRadius = 5.0
        self.txtEmail.clipsToBounds = true
        self.txtPhoneNumber.layer.cornerRadius = 5.0
        self.txtPhoneNumber.clipsToBounds = true
        
        CommomMethods.prefixTextField(self.txtDOB)
        CommomMethods.prefixTextField(self.txtEmail)
        
        CommomMethods.prefixTextField(self.txtPhoneNumber)
        
        txtTOS.contentOffset = CGPoint.zero
        
        
        let str = "I agree to the Terms Of Service and Privacy Policy"
        let attributedString = NSMutableAttributedString(string: str)
        var foundRange = attributedString.mutableString.rangeOfString("Terms Of Service")
        attributedString.addAttribute(NSLinkAttributeName, value: termsAndConditionsURL, range: foundRange)
        foundRange = attributedString.mutableString.rangeOfString("Privacy Policy")
        attributedString.addAttribute(NSLinkAttributeName, value: privacyURL, range: foundRange)
        txtTOS.attributedText = attributedString
        
   }

    @IBAction func btnSavePressed(sender: AnyObject) {
        
        self.view .endEditing(true)
        if(self.txtEmail.text == "" || CommomMethods.isValidEmail(self.txtEmail.text!) == false){
            CommomMethods.showAlert("Please enter email", view:self)
            return
        }
//                if (self.txtDOB.text == ""){
//            CommomMethods.showAlert("Please enter dob", view:self)
//            return
//        }
        
        if (self.txtPhoneNumber.text == ""){
            CommomMethods.showAlert("Please enter phone number", view:self)
            return
        }
        
        if btnAgree.selected == false{
            
            CommomMethods.showAlert("You must agree to terms of service and privacy policy to signup", view:self)
            return
            
        }
        
        let user:UserModel = UserModel.sharedInstance
        user.email = self.txtEmail.text
        

        
        if self.txtDOB.text == ""{
        
            user.dob = " "
        }
        else{
            
            user.dob = self.txtDOB.text
        }
        
        user.password = "123"
        user.phoneNumber = self.txtPhoneNumber.text
        
        self.performSegueWithIdentifier("toNext", sender: self)
        
        
           
    }
    @IBAction func moveBack(sender: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
     {
     textField.resignFirstResponder()
       return true
    }
    
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        
    }
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool{
        
        if textField.tag == 3{
            
            
            
            
            
            datePicker.datePickerMode = UIDatePickerMode.Date
            
            let toolBar = UIToolbar()
            toolBar.barStyle = .Default
            toolBar.translucent = true
            toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
            toolBar.sizeToFit()
            
            // Adds the buttons
            let doneButton = UIBarButtonItem(title: "Done", style: .Plain, target: self, action: "doneClick")
            let spaceButton = UIBarButtonItem(barButtonSystemItem: .FlexibleSpace, target: nil, action: nil)
            let cancelButton = UIBarButtonItem(title: "Cancel", style: .Plain, target: self, action: "cancelClick")
            toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
            toolBar.userInteractionEnabled = true
            
            textField.inputView = datePicker
            textField.inputAccessoryView = toolBar
            
            
        }
        
        return true
    }
    
    func doneClick() {
        
        let dateFormatter = NSDateFormatter()
        
        dateFormatter.dateFormat = "dd MMM yyyy"
        self.txtDOB.text = dateFormatter.stringFromDate(datePicker.date)
        self.txtDOB.resignFirstResponder()
        
        
    }
    
    func cancelClick() {
        self.txtDOB.resignFirstResponder()
    }
 
    @IBAction func btnAgreePressed(sender: UIButton) {
        
        sender.selected = !sender.selected
    }
    
    func textView(textView: UITextView, shouldInteractWithURL URL: NSURL, inRange characterRange: NSRange) -> Bool {
        if (URL.absoluteString == termsAndConditionsURL) {
            
            let termsVC:TermsVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("TermsVC") as! TermsVC
            self.navigationController?.pushViewController(termsVC, animated:true)
            
        } else if (URL.absoluteString == privacyURL) {
            
            let termsVC:TermsVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("TermsVC") as! TermsVC
            termsVC.isFromPrivacy = true
            self.navigationController?.pushViewController(termsVC, animated:true)
            
        }
        return false
    }

}