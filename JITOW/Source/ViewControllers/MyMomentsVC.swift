//
//  MyMomentsVC.swift
//  JITOW
//
//  Created by Mac103 on 4/27/16.
//  Copyright © 2016 LC. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import MBProgressHUD
class MyMomentsVC: UIViewController{
    
   
    
    @IBOutlet weak var lblMyTotalContacts: UILabel!
    @IBOutlet weak var lblMyTotalMomments: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var viewHeader: UIView!
    var userData:UserModel?
    var isPerformSegue:Bool = false
    override func viewDidLoad() {
        
        viewHeader.layer.cornerRadius = 10
        viewHeader.clipsToBounds = true
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("updateData"), name:"reloadData", object: nil)
        
        getMyMoments()
        
        
    }
    

    override func shouldPerformSegueWithIdentifier(identifier: String, sender: AnyObject!) -> Bool {
        
        if identifier == "toThumbnails"{
            
            if(isPerformSegue){
                return true
            }else{
                return false
            }
        }
        return true
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        isPerformSegue = false
        if segue.identifier == "toThumbnails"{
            
            let vc = segue.destinationViewController as! MyMommentsPages
            
            if userData?.momentsArray.count > 0{
                
                vc.pageTitles = (userData?.momentsArray.mutableCopy())! as! NSMutableArray
            }
            
            
            
            
        }
    }
    
    
    
    func getMyMoments(){
        
        MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        
        let str = kBaseServerUrl + "g_smoment"
        let fullUrl  = NSURL(string: str)
        
        let userid = NSUserDefaults.standardUserDefaults().objectForKey(kUserid) as? String
        
        print(userid)
        
        let newPost = ["user_id":userid!]as [String:AnyObject]
        
        
        
        let request = Alamofire.request(.POST, fullUrl!, parameters: newPost, encoding: .URL)
        request.responseJSON
            { response in
                
                if let anError = response.result.error
                {
                    
                    
                    
                    dispatch_async(dispatch_get_main_queue(),
                        {
                            
                            CommomMethods.showAlert("something went wrong please check your internet connection and try again later", view: self)
                            MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
                            
                            
                        }
                    )
                    
                    print("error calling POST on /posts")
                    print(anError)
                    
                }
                else if let data = response.data as NSData!
                {
                    
                    
                    let json = JSON(data:data)
                    
                    let status = json["status"].stringValue
                    
                    if (status == "ok")
                    {
                
                        
                        let d : Dictionary = json["response"].dictionaryValue
                        
                        
                        
                        
                        
                             let user = UserModel()
                                
                                if let str:NSString = d["user_id"]!.string{
                                    
                                    user.userid = str
                                    
                                }
                                if let str:NSString = d["user_name"]!.string{
                                    
                                    user.username = str
                                    
                                }
                                
                                
                                
                                if let str:NSString = d["dob"]!.string{
                                    
                                    user.dob = str
                                    
                                }
                                
                                if let str:NSString = d["email"]!.string{
                                    
                                    user.email = str
                                    
                                }
                                
                                if let str:NSString = d["phone_number"]!.string{
                                    
                                    user.phoneNumber = str
                                    
                                }
                                
                                if let str:NSString = d["video_path"]!.string{
                                    
                                    user.videoUrl = str
                                    
                                }
                        
                        if let str:NSString = d["total_contacts"]!.string{
                            
                            user.totalContacts = str
                            
                        }

                        
                        
                                
                                if let arr:NSArray = d["videos"]?.arrayObject{
                                    
                                    for index in 0..<arr.count {
                                        
                                        let video = VideoModel()
                                        
                                        let dict:NSDictionary = arr.objectAtIndex(index) as! NSDictionary
                                        
                                        
                                        if let str:NSString = dict["caption"]as? NSString{
                                            
                                            video.caption = str
                                            
                                        }
                                        
                                        
                              
                                        
                         

                                        if let str:NSString = dict["emoji_num"]as? NSString{
                                            
                                            if str != ""{
                                              
                                           var newstr = str.stringByReplacingOccurrencesOfString("[", withString: "")
                                                
                                        newstr = newstr.stringByReplacingOccurrencesOfString("]", withString: "")
                                                
                                           
                                                
                                            let emojis = newstr.componentsSeparatedByString(",")
                                                
                                     for str1 in emojis{
                                             
                                        
                                  video.emojiList.addObject(str1)
                
                
                                         }
                                                
                                            }
            
                                            
        }
                                        
                                        
                                        if let str:NSString = dict["file_path"]as? NSString{
                                            
                                            video.videoUrl = str
                                            
                                        }
                                        
                                        if let str:NSString = dict["file_thumbnail_path"]as? NSString{
                                            
                                            video.thumbnail_url = str
                                            
                                        }
                                        
                                        if let str:NSString = dict["date"] as? NSString{
                                            
                                            video.date = str
                                            
                                        }
                                        
                                        if let str:NSString = dict["id"]as? NSString{
                                            
                                            video.video_id = str
                                            
                                        }
                                        
                                        if let str:NSString = dict["emoji"]as? NSString{
                                            
                                            video.emojiCount = str
                                            
                                        }
                                        
                                        if let str:NSString = dict["views"]as? NSString{
                                            
                                            video.sceneCount = str
                                            
                                        }
                                        
                                        
                                        print(video.emojiCount)
                                        
                                        print(video.sceneCount)
                                        
                                        
                                        user.momentsArray .addObject(video)
                                        
                                        
                                        
                                        
                                    }
                                    
                                    
                                }
                        
                        
                        
                           self.userData = user
                            self.setData(user)
                                

                        
                        
                    }
                    
                    
                    dispatch_async(dispatch_get_main_queue(),
                        {
                            
                            MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
                            
                            
                        }
                    )
                    
                    
                }
                
                
        }
        
        
        
    }
    
    func setData(user:UserModel){
        
        lblUserName.text = user.username as? String
        lblMyTotalMomments.text = String(user.momentsArray.count)
        
        lblMyTotalContacts.text = user.totalContacts as? String
        
        UserModel.sharedInstance.username = user.username
        UserModel.sharedInstance.dob = user.dob
        UserModel.sharedInstance.email = user.email
        UserModel.sharedInstance.momentsArray = user.momentsArray
        UserModel.sharedInstance.totalContacts = user.totalContacts
        
        
        isPerformSegue = true
        self.performSegueWithIdentifier("toThumbnails", sender: self)
        
    }
    
    func updateData(){
        
        for vc:UIViewController in self.childViewControllers{
            
            if vc .isKindOfClass(MyMommentsPages){
                
                vc.willMoveToParentViewController(self)
                vc.didMoveToParentViewController(self)
                vc.view.removeFromSuperview()
                vc.removeFromParentViewController()
                
                
            }
        }
        
        self.getMyMoments()

    }
}