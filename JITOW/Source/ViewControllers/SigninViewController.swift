//
//  SigninViewController.swift
//  JITOW
//
//  Created by Irfan Malik on 2/10/16.
//  Copyright © 2016 LC. All rights reserved.
//

import UIKit
import MBProgressHUD
import Alamofire

class SigninViewController: UIViewController,smsVerificationDelegate {
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var txtUserName: UITextField!


    
    var isFromRoot:Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.txtUserName.layer.cornerRadius = 5.0
        self.txtUserName.clipsToBounds = true
        
    
        btnBack.hidden = false
        
        CommomMethods.prefixTextField(self.txtUserName)
      
        
          NSNotificationCenter.defaultCenter().addObserver(self, selector:"checkLogin", name:"userLogined", object: nil);
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector:"timeOut:", name:"timeout", object: nil);
        
        
        
        if isFromRoot == true{
            
            btnBack.hidden = true
            print(NSUserDefaults.standardUserDefaults().objectForKey(kUserName))
            
            print(NSUserDefaults.standardUserDefaults().objectForKey(kPassword))
            
            self.txtUserName.text = NSUserDefaults.standardUserDefaults().objectForKey(kUserName) as? String
            
            
            MBProgressHUD.showHUDAddedTo(self.view, animated: true)
            
            self.loginXmmp()
        }

        
        

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBarHidden = true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func moveBack(sender: AnyObject)
    {
      self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    @IBAction func btnLoginPressed(sender: AnyObject) {
        
        self.view .endEditing(true)
        
        if(self.txtUserName.text == ""){
            CommomMethods.showAlert("Please enter phone number", view:self)
            return
            
        }
        
        UserModel.sharedInstance.phoneNumber = txtUserName.text
        
        self.performSegueWithIdentifier("toNext", sender: self)
  
        
      
        
    }
    
    func login(){
        
        let str = kBaseServerUrl + "Signin"
        let fullUrl  = NSURL(string: str)
        
        
        var deviceToken:String
        
        if NSUserDefaults.standardUserDefaults().objectForKey(kDeviceToken) != nil{
            
            deviceToken = (NSUserDefaults.standardUserDefaults().objectForKey(kDeviceToken) as? String)!
            
        }
        else{
            
            deviceToken = " "
        }

       
        
        let newPost = ["username":self.txtUserName.text!,"password":"123", "apns_token":deviceToken] as [String:AnyObject]
        
        
        
        let request = Alamofire.request(.POST, fullUrl!, parameters: newPost, encoding: .URL)
        request.responseJSON
            { response in
                
                if let anError = response.result.error
                {
                    
                    
                    dispatch_async(dispatch_get_main_queue(),
                        {
                            
                    CommomMethods.showAlert("something went wrong please check your internet connection and try again later", view: self)
                            
                            
                            MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
                            
                            
                        }
                    )

                    
                    
                    print("error calling POST on /posts")
                    print(anError)
                   
                }
                else if let data = response.data as NSData!
                {
                   
                    
                    let json = JSON(data:data)
                    
                    let status = json["status"].stringValue
                    
                    if (status == "ok")
                    {
                        
                        if let str:NSString = json["response"]["id"].string{
                            
                            UserModel.sharedInstance.userid = str
                            
                            NSUserDefaults.standardUserDefaults().setObject(str, forKey: kUserid)
                            
                        }
                        if let str:NSString = json["response"]["username"].string{
                            
                            UserModel.sharedInstance.username = str
                            NSUserDefaults.standardUserDefaults().setObject(str, forKey: kUserName)
                            
                            UserModel.sharedInstance.password = "123"
                            NSUserDefaults.standardUserDefaults().setObject("123", forKey: kPassword)
                            
                        }
                        
                        
                        if let str:NSString = json["response"]["email"].string{
                            
                            UserModel.sharedInstance.email = str
                        }
                        if let str:NSString = json["response"]["dob"].string{
                            
                            UserModel.sharedInstance.dob = str
                        }
                        
                        if let str:NSString = json["response"]["total_contacts"].string{
                            
                            UserModel.sharedInstance.totalContacts = str
                        }
                        
                        
                        
                        
                        if let str:NSString = json["response"]["phone_number"].string{
                            
                            UserModel.sharedInstance.phoneNumber = str
                            
                            NSUserDefaults.standardUserDefaults().setObject(str, forKey: kUserPhoneNumber)
                        }
                        if let str:NSString = json["response"]["video_path"].string{
                            
                            UserModel.sharedInstance.videoUrl = str
                        }
                        
                        
                        NSUserDefaults.standardUserDefaults().synchronize()
                        
                        self.loginXmmp()
                       
                       
                        
                        
                    }
                    else{
                        
        dispatch_async(dispatch_get_main_queue(),
                            {
                                
            CommomMethods.showAlert(json["message"].stringValue, view: self)
            
            MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
                                
                                
                            }
                        )
                        
                        
                        
                       
                    }

                    
                    
                }
                
                
        }
        
        
        
        

    }
    
    func loginXmmp(){
        
        
     print(NSUserDefaults.standardUserDefaults().objectForKey(kUserPhoneNumber))
        
    let jabberID = NSUserDefaults.standardUserDefaults().objectForKey(kUserPhoneNumber) as! String + "@" + kAppendedUrl
        
    NSUserDefaults.standardUserDefaults().setObject(jabberID, forKey: kJabberid)
        
    NSUserDefaults.standardUserDefaults().synchronize()
        
    let appDel = UIApplication.sharedApplication().delegate! as! AppDelegate
                
    appDel.connectXmpp()
   
   
    }
    func checkLogin()
    {
       NSTimer.scheduledTimerWithTimeInterval(0.5, target: self, selector: "moveNext", userInfo: nil, repeats: false)
        
    
    }
    func moveNext(){
        
        MBProgressHUD.hideHUDForView(self.view, animated: true)
        
        self.performSegueWithIdentifier("toHome", sender: self)
    }
    
    func timeOut(n:NSNotification) {
        
        
        MBProgressHUD.hideHUDForView(self.view, animated: true)
        
        let data = n.userInfo! as NSDictionary
        
            if let s : NSString = data.objectForKey("status") as? NSString{
            
                if s == "timeout"{
                    
                    
                     CommomMethods.showAlert("something went wrong please check your internet connection and try again later", view: self)
                }
                else if s == "failed"{
                
                     CommomMethods.showAlert("Invalid credentials please try agian with valid credentials", view: self)
                    
                }
                
                
        }
                
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    func codeVerified(){
        
        MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        
        self.login()
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "toNext"{
          
            let vc:SMSVerificationVC = segue.destinationViewController as! SMSVerificationVC
            vc.isFromLogin = true
            vc.delegate = self
            
        }
    }
}
