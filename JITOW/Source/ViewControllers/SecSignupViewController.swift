//
//  SecSignupViewController.swift
//  JITOW
//
//  Created by Irfan Malik on 2/10/16.
//  Copyright © 2016 LC. All rights reserved.
//

import UIKit
import MBProgressHUD
import MobileCoreServices
import MediaPlayer
import Alamofire

class SecSignupViewController: UIViewController,UITextFieldDelegate ,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    @IBOutlet weak var tf_userName: UITextField!
    @IBOutlet weak var view_container: UIView!
    var moviePlayer: MPMoviePlayerController?

    var videoUrl:NSURL?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tf_userName.delegate = self
        
        view_container.layer.borderWidth=1.0
        view_container.layer.masksToBounds = false
        view_container.layer.borderColor = UIColor.lightGrayColor().CGColor
        view_container.layer.cornerRadius = view_container.frame.size.height/2
        view_container.clipsToBounds = true
        
        let tap = UITapGestureRecognizer(target: self, action: Selector("handleTap:"))
        view_container.addGestureRecognizer(tap)
        

        CommomMethods.prefixTextField(self.tf_userName)
        
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector:"checkLogin", name:"userLogined", object: nil);
        
         NSNotificationCenter.defaultCenter().addObserver(self, selector:"timeOut:", name:"timeout", object: nil);
        
        
        // Do any additional setup after loading the view.
    }
    
    //MARK:- VideoRecording
    func handleTap(sender: UITapGestureRecognizer? = nil) {
        // handling code
        tf_userName.resignFirstResponder()
        if (UIImagePickerController.isSourceTypeAvailable(.Camera)) {
            if UIImagePickerController.availableCaptureModesForCameraDevice(.Front) != nil {
                let imag = UIImagePickerController()
                imag.delegate = self
                imag.sourceType = UIImagePickerControllerSourceType.Camera;
                imag.cameraDevice = UIImagePickerControllerCameraDevice.Front;
                

                imag.mediaTypes = [kUTTypeMovie as String];
                imag.allowsEditing = false
                imag.videoMaximumDuration = 10.0
                self.presentViewController(imag, animated: true, completion: nil)
                
                
            } else {
                CommomMethods.showAlert("Application cannot access the camera.", view: self)
                
            }
        } else {
            CommomMethods.showAlert("Application cannot access the camera.", view: self)
        }
        return

    }

    // Finished recording a video
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        print("Got a video")
        
        if let pickedVideo:NSURL = (info[UIImagePickerControllerMediaURL] as? NSURL) {
            
            print(pickedVideo);
            
            videoUrl = pickedVideo
            self.playVideo(pickedVideo)
            
            
            self.dismissViewControllerAnimated(true, completion: nil)
            
        }
        
        picker.dismissViewControllerAnimated(true, completion: {
            // Anything you want to happen when the user saves an video
        })
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController)
    {
        picker.dismissViewControllerAnimated(true, completion: {
            // Anything you want to happen when the user saves an video
        })
    }

    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func movebACK(sender: AnyObject) {
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController];
        
        for obj:UIViewController in viewControllers{
            
            if obj .isKindOfClass(SignUpVC){
                
                self.navigationController!.popToViewController(obj, animated: true);
            }
        }
        
        
    }
    
    //MARK :- UItextfield Delegetes
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }


    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        
    }
    
    @IBAction func btnSavePressed(sender: AnyObject) {
        
        if(self.tf_userName.text == ""){
            CommomMethods.showAlert("Please enter userName", view:self)
            return
            
        }
        if moviePlayer?.playbackState == MPMoviePlaybackState.Playing{
            
            moviePlayer?.pause()
        }
        
        
        MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        
        
        UserModel.sharedInstance.username = self.tf_userName.text
        
        
       
         newSignUp()
        
    }
    
    func newSignUp(){
        
        let str = kBaseServerUrl + "Signup"
        let fullUrl  = NSURL(string: str)
        
        var deviceToken:String
        
        if NSUserDefaults.standardUserDefaults().objectForKey(kDeviceToken) != nil{
            
            deviceToken = (NSUserDefaults.standardUserDefaults().objectForKey(kDeviceToken) as? String)!
            
        }
        else{
            
            deviceToken = " "
        }
    
        
        
        Alamofire.upload(
            .POST,
            fullUrl!,
            multipartFormData: { multipartFormData in
                
                if let url = self.videoUrl{
                    
                    multipartFormData.appendBodyPart(fileURL: url, name: "video")
                    
                }
                
                
                multipartFormData.appendBodyPart(data: UserModel.sharedInstance.username!.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)!, name :"username")
                multipartFormData.appendBodyPart(data: UserModel.sharedInstance.password!.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)!, name :"password")
                multipartFormData.appendBodyPart(data:UserModel.sharedInstance.email!.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)!, name :"email")
                multipartFormData.appendBodyPart(data:UserModel.sharedInstance.dob!.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)!, name :"dob")
                multipartFormData.appendBodyPart(data:UserModel.sharedInstance.phoneNumber!.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)!, name :"phone_number")
                multipartFormData.appendBodyPart(data:deviceToken.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)!, name :"apns_token")
                
            },
            encodingCompletion: { encodingResult in
                switch encodingResult {
                    
                case .Success(let upload, _, _):
                    upload.responseJSON { Response in
                        
            
                        if let data = Response.data as NSData!
                        {
                            let json = JSON(data:data)
                        
                            let status = json["status"].stringValue
                            
                            if (status == "ok")
                            {
                                
                                if let str:NSString = json["response"]["id"].string{
                                    
                                    UserModel.sharedInstance.userid = str
                                    
                                    NSUserDefaults.standardUserDefaults().setObject(str, forKey: kUserid)
                                    
                                }
                                if let str:NSString = json["response"]["username"].string{
                                    
                                    UserModel.sharedInstance.username = str
                                    NSUserDefaults.standardUserDefaults().setObject(str, forKey: kUserName)
                                    
                                    NSUserDefaults.standardUserDefaults().setObject(UserModel.sharedInstance.password, forKey: kPassword)
                                }
                                
                                if let str:NSString = json["response"]["email"].string{
                                    
                                    UserModel.sharedInstance.email = str
                                }
                                if let str:NSString = json["response"]["dob"].string{
                                    
                                    UserModel.sharedInstance.dob = str
                                }
                                if let str:NSString = json["response"]["phone_number"].string{
                                    
                                    UserModel.sharedInstance.phoneNumber = str
                                    
                                     NSUserDefaults.standardUserDefaults().setObject(str, forKey: kUserPhoneNumber)
                                }
                                if let str:NSString = json["response"]["video_path"].string{
                                    
                                    UserModel.sharedInstance.videoUrl = str
                                }
                                
                                let user = UserModel.sharedInstance
                                print(user)
                                
                                NSUserDefaults.standardUserDefaults().synchronize()
                                
                                self.registerXmmp()
                                
                                
                            }
                            else{
                                
                                
                                dispatch_async(dispatch_get_main_queue(),
                                    {
                                        
                                        CommomMethods.showAlert(json["message"].stringValue, view: self)
                                        MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
                                        
                                        
                                    }
                                )
                                
                                
                            }

                        
                        }
                        
                    }
                    
                case .Failure(let error):
                    
                    print(error)
                    
                    dispatch_async(dispatch_get_main_queue(),
                        {
                            
                            CommomMethods.showAlert("Something went wrong please check your internet connection and try again later", view: self)
                            MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
                            
                            
                        }
                    )

                
                }
            
            }
        )
    }

    
       
    func login(){
        
        let jabberID = UserModel.sharedInstance.phoneNumber as! String + "@" + kAppendedUrl
        
        NSUserDefaults.standardUserDefaults().setObject(jabberID, forKey: kJabberid)
        
        
        NSUserDefaults.standardUserDefaults().synchronize()
        
        let obj:[NSObject : AnyObject] = ["fullname":UserModel.sharedInstance.username! , "pin": UserModel.sharedInstance.username! ,"message": "", "user_thumbnail": "","status": "", "userid": UserModel.sharedInstance.userid!]
        
        
        CommomMethods.getAppDelegate().xmppcon?.addLoginUserProfile(obj as [NSObject : AnyObject])
        
        
        let appDel = UIApplication.sharedApplication().delegate! as! AppDelegate
        
        appDel.connectXmpp()
    }
    
    
    func checkLogin()
    {
        
      NSTimer.scheduledTimerWithTimeInterval(0.5, target: self, selector: "moveNext", userInfo: nil, repeats: false)
    }
    func moveNext(){
        
        MBProgressHUD.hideHUDForView(self.view, animated: true)
        
        self.performSegueWithIdentifier("toHome", sender: self)
    }
    
   
    
    func registerXmmp()
    {
        
        let Jid :String = UserModel.sharedInstance.phoneNumber as! String
        
        let error :NSError?
        let newPost = ["username":Jid,"name":Jid, "password":UserModel.sharedInstance.password as! String];
        let options = NSJSONWritingOptions()
        do {
            
            
            let postdata = try NSJSONSerialization.dataWithJSONObject(newPost, options: options)
            let url = NSURL(string: "http://ec2-52-34-41-17.us-west-2.compute.amazonaws.com:9090/plugins/restapi/v1/users")
            let req:NSMutableURLRequest = NSMutableURLRequest(URL:url!)
            req.HTTPMethod = "POST"
            req.addValue("application/json", forHTTPHeaderField: "Content-Type")
            req.addValue("application/json", forHTTPHeaderField: "Accept")
            req.addValue("r8zPpH9Bu4yrw6Gc", forHTTPHeaderField: "Authorization")
            req.HTTPBody = postdata
            print(req.debugDescription)
            let resp :NSURLResponse?
            let err :NSError?
            
            //let data : NSData? = NSURLConnection.sendSynchronousRequest(req, returningResponse: &resp, error: &err)
            let session = NSURLSession.sharedSession()
            session.dataTaskWithRequest(req, completionHandler: {(data, response, error) in
                if(data != nil)
                {
                    let json = JSON(data:data!)
                    print(json)
                    
                    let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
                    dispatch_async(dispatch_get_global_queue(priority, 0))
                        {
                            dispatch_async(dispatch_get_main_queue())
                                {
                                    
                                self.login()
                                    
                            }
                    }
                   
                    
                    
                    
                    
                }
                else
                {
                    
                    
                    dispatch_async(dispatch_get_main_queue(),
                        {
                            
                    CommomMethods.showAlert("Something went wrong please try again", view: self)
                            
                   MBProgressHUD.hideHUDForView(self.view, animated: true)
                            
                            
                        }
                    )
                    
                    
                    
                    
                }
                
            }).resume()
        } catch {
            
            MBProgressHUD.hideHUDForView(self.view, animated: true)
        }
    }

    func playVideo(url:NSURL){
        
        
        moviePlayer = MPMoviePlayerController(contentURL: url)
        if let player = moviePlayer {
            player.view.frame = view_container.bounds
            player.prepareToPlay()
            player.scalingMode = .AspectFill
            player.controlStyle = MPMovieControlStyle.None
            view_container.addSubview(player.view)
            player.play()
        }
    }
    
    
    func timeOut(n:NSNotification) {
        
        
        MBProgressHUD.hideHUDForView(self.view, animated: true)
        
        let data = n.userInfo! as NSDictionary
        
        if let s : NSString = data.objectForKey("status") as? NSString{
            
            if s == "timeout"{
                
                
                CommomMethods.showAlert("something went wrong please check your internet connection and try again later", view: self)
            }
            else if s == "failed"{
                
                CommomMethods.showAlert("Invalid credentials please try agian with valid credentials", view: self)
                
            }
            
            
        }
        
    }
}
