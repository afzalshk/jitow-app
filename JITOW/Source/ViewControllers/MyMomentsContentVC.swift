//
//  MyMomentsContentVC.swift
//  JITOW
//
//  Created by Mac103 on 4/27/16.
//  Copyright © 2016 LC. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage



class MyMomentsContentVC: UIViewController {
    
    @IBOutlet weak var imgMoment: UIImageView!
    var pageIndex: Int?
    var videoObj : VideoModel!
    
    override func viewDidLoad() {
        
        
        
        imgMoment.sd_setImageWithURL(NSURL(string:(videoObj.thumbnail_url as? String)! ), placeholderImage: UIImage(named: "chat_imgplacehold"))
        
        
        
    
    }
  
    @IBAction func tapThumbnailPressed(sender: AnyObject) {
        
        self.performSegueWithIdentifier("toPlayVideo", sender: self)
        
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "toPlayVideo"{
            
            let vc:PlayVideoVC = segue.destinationViewController as! PlayVideoVC
            vc.video = videoObj
            vc.isFromUser = true
            
            
        
        }
        
    }
  
    
    
}