//
//  TabelViewCell.h
//  yupApp
//
//  Created by Irfan Malik on 9/7/15.
//  Copyright (c) 2015 LC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface TabelViewCell : NSObject


+(CGSize) getSizeforText:(NSString *)message andSize:(UIView *)contentView;
+(CGSize)SizeforText:(NSString *)message andSize:(UIView *)view;
+(UIImage *)streatchImage;
+(UIImage *)recieverStreatchImage;
@end
