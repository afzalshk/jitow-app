//
//  MyprofileViewController.swift
//  JITOW
//
//  Created by Irfan Malik on 2/10/16.
//  Copyright © 2016 LC. All rights reserved.
//

import UIKit
import MessageUI

class MyprofileViewController: UIViewController,MFMailComposeViewControllerDelegate {

    
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblMomentsCount: UILabel!
    @IBOutlet weak var lblContactsCount: UILabel!
    @IBOutlet var viewsCollection: [UIView]!
    
    @IBAction func btnSettingPressed(sender: AnyObject) {
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        for v:UIView in viewsCollection{
            
            v.layer.cornerRadius = 5.0
            v.clipsToBounds = true
            
        }
        
       lblUserName.text = UserModel.sharedInstance.username as? String
        lblMomentsCount.text = String(UserModel.sharedInstance.momentsArray.count)
        
        lblContactsCount.text = UserModel.sharedInstance.totalContacts as? String
        
        
    }
    
    override func viewWillAppear(animated: Bool) {
        
        self.navigationController?.navigationBarHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnLogOutPressed(sender: AnyObject) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewControllerWithIdentifier("Login")
        let nav:UINavigationController = UINavigationController(rootViewController: vc)
        
        let del = UIApplication.sharedApplication().delegate
        del?.window!!.rootViewController = nav
        
    }

       
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func btnReportPressed(sender: AnyObject) {
        
        let mailComposeViewController = configuredMailComposeViewController()
        if MFMailComposeViewController.canSendMail() {
            self.presentViewController(mailComposeViewController, animated: true, completion: nil)
        } else {
            self.showSendMailErrorAlert()
        }
    }
    
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        
        mailComposerVC.setToRecipients(["abrahamniisi14@gmail.com"])
        mailComposerVC.setSubject("Report a Problem")
        mailComposerVC.setMessageBody("", isHTML: false)
        
        return mailComposerVC
    }
    
    func showSendMailErrorAlert() {
        
        CommomMethods .showAlert("Your device could not send e-mail.  Please check e-mail configuration and try again.", view: self)
    }
    
    // MARK: MFMailComposeViewControllerDelegate Method
    func mailComposeController(controller: MFMailComposeViewController!, didFinishWithResult result: MFMailComposeResult, error: NSError!) {
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
}
