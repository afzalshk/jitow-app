//
//  AddContactViewController.swift
//  JITOW
//
//  Created by Yosmite on 2/15/16.
//  Copyright © 2016 LC. All rights reserved.
//

import Foundation
import Alamofire
import MBProgressHUD


class AddContactViewController: UIViewController {
    
    
    @IBOutlet weak var txtUserPin: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnBackPressed(sender: AnyObject) {
        
        self.navigationController?.popViewControllerAnimated(true)
        
    }
    
    @IBAction func btnAddPressed(sender: AnyObject) {
        
        if(self.txtUserPin.text == ""){
            CommomMethods.showAlert("Please enter phone number to add", view:self)
            return
            
        }
        
        MBProgressHUD.showHUDAddedTo(self.view , animated: true)
    
        addFriend()
        
    }
    
    func addFriend(){
        
        let str = kBaseServerUrl + "addcontact"
        let fullUrl  = NSURL(string: str)
        
          let newPost = ["contact":self.txtUserPin.text!,"user_id":NSUserDefaults.standardUserDefaults().objectForKey(kUserid)!] as [String:AnyObject]
        
    
        let request = Alamofire.request(.POST, fullUrl!, parameters: newPost, encoding: .URL)
        request.responseJSON
            { response in
                
                if let anError = response.result.error
                {
                    print("error calling POST on /posts")
                    print(anError)
                    CommomMethods.showAlert("something went wrong please check your internet connection and try again later", view: self)
                    MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
                }
                else if let data = response.data as NSData!
                {
                    
                    
                    let json = JSON(data:data)
                    
                    let status = json["status"].stringValue
                    
                    if (status == "ok")
                    {
                        
                        
                        
                        let userName = self.txtUserPin.text
                        
                        let appDel = UIApplication.sharedApplication().delegate! as! AppDelegate
                       
                        let Jid :String = userName! + kAppendedUrl
                        print(Jid)
                        appDel.sendFriendRequest(Jid,nickname: "")
                        
                        NSTimer.scheduledTimerWithTimeInterval(0.3, target: self, selector: Selector("popBack"), userInfo: nil, repeats:false)
                        
                    }
                    else{
                        
                    CommomMethods.showAlert(json["message"].stringValue, view: self)
                        MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
                    }
                    
                    
                    
                }
                
                
        }
        
        
        
        
        
    }

    func popBack(){
        
        MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
        
         CommomMethods.showAlert("Friend request sent successfully", view: self)
         self.navigationController?.popViewControllerAnimated(true)
        
    }
    
}