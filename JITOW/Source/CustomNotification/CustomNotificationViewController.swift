//
//  CustomNotificationViewController.swift
//  JITOW
//
//  Created by Mac103 on 4/15/16.
//  Copyright © 2016 LC. All rights reserved.
//


import UIKit

class CustomNotificationViewController: UIViewController
{

    @IBOutlet weak var lbl_message: UILabel!
    @IBOutlet weak var lbl_name: UILabel!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.setHeight()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setHeight()
    {
        let height:CGFloat = UIScreen.mainScreen().bounds.size.height
        if(height == 667)
        {
            self.view.frame = CGRectMake(0,0,375,69)
        }
        else if (height == 736)
        {
            self.view.frame = CGRectMake(0,0,414,69)
        }
    }
    

   
}
