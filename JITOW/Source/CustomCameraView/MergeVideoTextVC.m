//
//  MergeVideoTextVC.m
//  JITOW
//
//  Created by Mac103 on 4/15/16.
//  Copyright © 2016 LC. All rights reserved.
//

#import "MergeVideoTextVC.h"
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <UIKit/UIKit.h>
@implementation MergeVideoTextVC
@synthesize delegate;

-(void)MixVideoWithText:(NSURL*)url withString:(NSString*)str withCheck:(BOOL)isFromPreview
{
    AVURLAsset* videoAsset = [[AVURLAsset alloc]initWithURL:url options:nil];
    AVMutableComposition* mixComposition = [AVMutableComposition composition];
    
    AVMutableCompositionTrack *compositionVideoTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];
    AVAssetTrack *clipVideoTrack = [[videoAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
    AVMutableCompositionTrack *compositionAudioTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
    AVAssetTrack *clipAudioTrack = [[videoAsset tracksWithMediaType:AVMediaTypeAudio] objectAtIndex:0];
    //If you need audio as well add the Asset Track for audio here
    
    [compositionVideoTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, videoAsset.duration) ofTrack:clipVideoTrack atTime:kCMTimeZero error:nil];
    [compositionAudioTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, videoAsset.duration) ofTrack:clipAudioTrack atTime:kCMTimeZero error:nil];
    
    [compositionVideoTrack setPreferredTransform:clipVideoTrack.preferredTransform];
    
    CGSize sizeOfVideo=[[[videoAsset tracksWithMediaType:AVMediaTypeVideo] firstObject] naturalSize];
    
    CATextLayer *titleLayer = [CATextLayer layer];
    titleLayer.string = str;
    titleLayer.wrapped = true;
    titleLayer.font = (__bridge CFTypeRef _Nullable)(@"Helvetica");
    titleLayer.fontSize = 13.0f;
    //?? titleLayer.shadowOpacity = 0.5;
    titleLayer.alignmentMode = kCAAlignmentCenter;
    titleLayer.frame=CGRectMake(50, 10, [[UIScreen mainScreen] bounds].size.width-100, 100);
    
    
    CALayer *parentLayer=[CALayer layer];
    CALayer *videoLayer=[CALayer layer];
    
    NSLog(@"what's here %f,%f",sizeOfVideo.width,sizeOfVideo.height);
    parentLayer.frame=CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, sizeOfVideo.height);
    videoLayer.frame=CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, sizeOfVideo.height);
    
    [parentLayer addSublayer:videoLayer];
    [parentLayer addSublayer:titleLayer];
    
    
    AVMutableVideoComposition *videoComposition=[AVMutableVideoComposition videoComposition] ;
    videoComposition.frameDuration=CMTimeMake(1, 30);
    NSLog(@"what's here %f,%f",sizeOfVideo.width,sizeOfVideo.height);
    videoComposition.renderSize=CGSizeMake([UIScreen mainScreen].bounds.size.width, sizeOfVideo.height);
    videoComposition.animationTool=[AVVideoCompositionCoreAnimationTool videoCompositionCoreAnimationToolWithPostProcessingAsVideoLayer:videoLayer inLayer:parentLayer];
    
    
    
    
    AVMutableVideoCompositionInstruction *instruction = [AVMutableVideoCompositionInstruction videoCompositionInstruction];
    instruction.timeRange = CMTimeRangeMake(kCMTimeZero, [mixComposition duration]);
    AVAssetTrack *videoTrack = [[mixComposition tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
    AVMutableVideoCompositionLayerInstruction* layerInstruction = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:videoTrack];
    [layerInstruction setTransform:videoTrack.preferredTransform atTime:kCMTimeZero];
    instruction.layerInstructions = [NSArray arrayWithObject:layerInstruction];
    videoComposition.instructions = [NSArray arrayWithObject: instruction];
    
    
    
    
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd_HH-mm-ss"];
    NSString *destinationPath = [documentsDirectory stringByAppendingFormat:@"/utput_%@.mov", [dateFormatter stringFromDate:[NSDate date]]];
    
    AVAssetExportSession *exportSession = [[AVAssetExportSession alloc] initWithAsset:mixComposition presetName:AVAssetExportPresetHighestQuality];
    
    
    
    exportSession.videoComposition=videoComposition;
    
    
    
    exportSession.outputURL = [NSURL fileURLWithPath:destinationPath];
    exportSession.outputFileType = AVFileTypeQuickTimeMovie;
    [exportSession exportAsynchronouslyWithCompletionHandler:^{
        switch (exportSession.status)
        {
            case AVAssetExportSessionStatusCompleted:
                NSLog(@"Export OK");
               
                
                if (destinationPath) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        
                            
                            if ([self.delegate respondsToSelector:@selector(videoTextMerged:withBool:)]) {
                                
                                [self.delegate videoTextMerged:exportSession.outputURL withBool:isFromPreview];
                            }

                        
                        
                        
                    });
                    
                    
                }
                
                
                break;
            case AVAssetExportSessionStatusFailed:
                NSLog (@"AVAssetExportSessionStatusFailed: %@", exportSession.error);
                break;
            case AVAssetExportSessionStatusCancelled:
                NSLog(@"Export Cancelled");
                break;
        }
    }];
}


@end
