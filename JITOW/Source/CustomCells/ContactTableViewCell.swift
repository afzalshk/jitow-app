//
//  ContactTableViewCell.swift
//  JITOW
//
//  Created by Irfan Malik on 2/11/16.
//  Copyright © 2016 LC. All rights reserved.
//

import UIKit

protocol UserContactsDelegate{
    
    func blockContact(sender:UIButton)
    
}

class ContactTableViewCell: UITableViewCell {
    
    var delegate:UserContactsDelegate! = nil

    @IBOutlet weak var btnBlock: UIButton!
    @IBOutlet weak var view_container: UIView!
    
    @IBOutlet weak var lblUserName: UILabel!
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
        // Do any additional setup after loading the view, typically from a nib.
        view_container.layer.cornerRadius = 4
        view_container.layer.borderWidth = 1.0
        view_container.layer.borderColor = UIColor(red: 217.0/255, green: 217.0/255, blue: 217.0/255, alpha: 1.0).CGColor
        
        
        
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func btnBlockPressed(sender: UIButton) {
        
        
        delegate?.blockContact(sender)
        
    }
    
}

protocol SharedMomentContactsDelegate{
    
    func selectContact(sender:UIButton)
    
}

class SharedMomentContacts: UITableViewCell {
    
    var delegate:SharedMomentContactsDelegate! = nil
    
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var btnSelectContact: UIButton!
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
    }

    @IBAction func btnSelectContactPressed(sender: UIButton) {
        
        delegate?.selectContact(sender)
        
        sender.selected = !sender.selected
        
    }

}





protocol BlockContactCellDelegate{
    
    func unblockContact(sender:UIButton)
    
}

class BlockContactCell: UITableViewCell {
    
    
    @IBOutlet weak var lblUserName: UILabel!
    var delegate:BlockContactCellDelegate! = nil
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
    }
    
    
    @IBAction func btnUnblockPressed(sender: UIButton) {
        
        delegate?.unblockContact(sender)
        
    }
    
}

