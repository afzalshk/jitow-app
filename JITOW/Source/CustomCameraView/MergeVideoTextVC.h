//
//  MergeVideoTextVC.h
//  JITOW
//
//  Created by Mac103 on 4/15/16.
//  Copyright © 2016 LC. All rights reserved.
//
#import <Foundation/Foundation.h>

@protocol videoMergedDelegate <NSObject>

-(void)videoTextMerged:(NSURL*)url withBool:(BOOL)isFromPreview;


@end


@interface MergeVideoTextVC : NSObject


-(void)MixVideoWithText:(NSURL*)url withString:(NSString*)str withCheck:(BOOL)isFromPreview;

@property (nonatomic, assign) id <videoMergedDelegate> delegate;


@end
