//
//  AddmeViewController.swift
//  JITOW
//
//  Created by Irfan Malik on 2/18/16.
//  Copyright © 2016 LC. All rights reserved.
//

import UIKit
import Alamofire
import MBProgressHUD

class AddmeViewController: UIViewController,AddMeCellDelegate {

    @IBOutlet weak var tabel: UITableView!
    
    var allData:NSMutableArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
         getData()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - UitabelView
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return allData.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> AddMeTableViewCell
    {
        var cell:AddMeTableViewCell? = self.tabel.dequeueReusableCellWithIdentifier("Cell") as? AddMeTableViewCell
        if (cell == nil) {
            let nib:NSArray = NSBundle.mainBundle().loadNibNamed("AddMeTableViewCell", owner: self, options: nil)
            cell = nib.objectAtIndex(0) as? AddMeTableViewCell
        }
        cell?.selectionStyle = UITableViewCellSelectionStyle.None
        
        let user:UserModel = allData.objectAtIndex(indexPath.row) as! UserModel
        
    
        
        cell?.lblUserName.text = user.username as? String

        
        cell?.delegate = self
        
        return cell!
    }

    
    
    func getData(){
        
        
        MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        
        let str = kBaseServerUrl + "p_contact"
        let fullUrl  = NSURL(string: str)
        
        let userid = NSUserDefaults.standardUserDefaults().objectForKey(kUserid) as? String
        
        let newPost = ["user_id":userid!]as [String:AnyObject]
        
        
        
        let request = Alamofire.request(.POST, fullUrl!, parameters: newPost, encoding: .URL)
        request.responseJSON
            { response in
                
                if let anError = response.result.error
                {
                    
                    
                    
                    dispatch_async(dispatch_get_main_queue(),
                        {
                            
                CommomMethods.showAlert("something went wrong please check your internet connection and try again later", view: self)
                            MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
                            
                            
                        }
                    )
                    
                    print("error calling POST on /posts")
                    print(anError)
                    
                }
                else if let data = response.data as NSData!
                {
                    
                    
                    let json = JSON(data:data)
                    
                    let status = json["status"].stringValue
                    
                    if (status == "ok")
                    {
                        
                        let pendingRequests : Array = json["response"].arrayValue
                        
                        print(pendingRequests)
                        
                        if pendingRequests.count > 0{
                            
                            
                            for d in pendingRequests{
                               
                                
                          let user = UserModel()
                                
                                if let str:NSString = d["id"].string{
                                    
                                    user.userid = str
                                    
                                }
                                if let str:NSString = d["username"].string{
                                    
                                   user.username = str
                                    
                                }
                                
                                
                                if let str:NSString = d["email"].string{
                                    
                                    user.email = str
                                   
                                }
                                if let str:NSString = d["dob"].string{
                                    
                                    user.dob = str
                                }
                                if let str:NSString = d["phone_number"].string{
                                    
                                   user.phoneNumber = str
                                }
                                if let str:NSString = d["video_path"].string{
                                    
                                    user.videoUrl = str
                                   
                                }

                                 self.allData.addObject(user)
                                
                            }
                            
                           print(self.allData)
                           self.tabel.reloadData()
                            
                        }
                        
                        else{
                            
                            CommomMethods.showAlert("No data to display", view: self)
                        }
                        
                        
        
                    }
                    else{
                        
                        CommomMethods.showAlert(json["message"].stringValue, view: self)
                       
                    }
                    
                    dispatch_async(dispatch_get_main_queue(),
                        {
                            
                           MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
                            
                            
                        }
                    )
                    
                    
                }
                
                
        }
        
        
        
        
        
    }

    func updateRequestStatus(sender:UIButton,status:Int){
        
        let touchPoint = sender.convertPoint(CGPoint.zero, toView: self.tabel)
        
        let indexPath = self.tabel .indexPathForRowAtPoint(touchPoint)
        
        if status == 1{
           
            let usr = allData.objectAtIndex((indexPath?.row)!) as! UserModel
            
            let str = kBaseServerUrl + "a_contact"
            let fullUrl  = NSURL(string: str)
        
            let newPost = ["contact":usr.phoneNumber!,"user_id":NSUserDefaults.standardUserDefaults().objectForKey(kUserid)!] as [String:AnyObject]
            
            
            let request = Alamofire.request(.POST, fullUrl!, parameters: newPost, encoding: .URL)
            request.responseJSON
                { response in
                    
                    if let anError = response.result.error
                    {
                        print("error calling POST on /posts")
                        print(anError)
 
                        MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
                    }
                    else if let data = response.data as NSData!
                    {
                        
                        
                        let json = JSON(data:data)
                        
                        let status = json["status"].stringValue
                        
                        if (status == "ok")
                        {
                            
                            if let str:NSString = json["contact"].string{
                                
                                let appDel = UIApplication.sharedApplication().delegate! as! AppDelegate
                                
                                let Jid :String = (str as String) + kAppendedUrl
                                print(Jid)
                                appDel.xmppcon?.accceptFriendRequest(Jid)
                                
                            }
                            
                        }
                        
                        
                        
                        
        
                        MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
                        
                      
                        
                        
                        
                    }
                    
                    
            }
            
            
            
        }
        else{
            
            
            let usr = allData.objectAtIndex((indexPath?.row)!) as! UserModel
            
            let str = kBaseServerUrl + "r_contact"
            let fullUrl  = NSURL(string: str)
            
            let newPost = ["contact":usr.phoneNumber!,"user_id":NSUserDefaults.standardUserDefaults().objectForKey(kUserid)!] as [String:AnyObject]
            
            
            let request = Alamofire.request(.POST, fullUrl!, parameters: newPost, encoding: .URL)
            request.responseJSON
                { response in
                    
                    if let anError = response.result.error
                    {
                        print("error calling POST on /posts")
                        print(anError)
                        
                        MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
                    }
                    else if let data = response.data as NSData!
                    {
                        
                        
                    MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
                        
                        
                        
                        
                        
                    }
                    
                    
            }
            

            
            
        }
        
        allData.removeObjectAtIndex((indexPath?.row)!)
        self.tabel.beginUpdates()
        self.tabel!.deleteRowsAtIndexPaths([indexPath!], withRowAnimation: UITableViewRowAnimation.Fade)
        self.tabel.endUpdates()
        
        
        
    }

   
}
