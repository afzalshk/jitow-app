//
//  ChatViewController.swift
//  JITOW
//
//  Created by Irfan Malik on 2/15/16.
//  Copyright © 2016 LC. All rights reserved.
//

import UIKit
import MobileCoreServices
import SDWebImage
import Alamofire
import MBProgressHUD
import AVFoundation
import MediaPlayer


class ChatViewController: UIViewController,UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,TOCropViewControllerDelegate,SenderMediaCellDelegate,ReceiverMediaCellDelegate{
    
    var isKeyBoardopen :Bool = false
    var isUserOnline : Bool = false
    var Jid :String!
    var timer :NSTimer?
    var chatArr:NSArray?
    var toUser :String!
    
    
    @IBOutlet weak var tabel: UITableView!
    @IBOutlet weak var bottomView: NSLayoutConstraint!
    @IBOutlet weak var lblChatUserName: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    
    //Constraints
    @IBOutlet weak var mainViewHeight :NSLayoutConstraint!
    
    
    var moviePlayerCntroller:MPMoviePlayerViewController!
    
    
    @IBOutlet weak var txtMessage: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       self.registerNibCells()
        
      //  showOnline()
        
        txtMessage.delegate = self
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyBoardWillShow:"), name:UIKeyboardWillShowNotification, object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyBoardWillHide:"), name:UIKeyboardWillHideNotification, object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector:"updateStatus:", name:"onlineStatus", object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("getConverWithDelay"), name:"pushNotification", object: nil);
        

        
       // self.lblChatUserName.text = Jid
        getConver()
    }

    func registerNibCells()
    {
        let senderNibb = UINib(nibName: "ChatSenderTextTableViewCell", bundle: nil)
        tabel.registerNib(senderNibb, forCellReuseIdentifier: "sender")
        
        let senderMediaNibb = UINib(nibName: "SenderMediaCell", bundle: nil)
        tabel.registerNib(senderMediaNibb, forCellReuseIdentifier: "media")
        
        let recMediaNibb = UINib(nibName: "recieverImageCell", bundle: nil)
        tabel.registerNib(recMediaNibb, forCellReuseIdentifier: "rmedia")
        
        let recverNibb = UINib(nibName: "ChatReciverTextTableViewCell", bundle: nil)
        tabel.registerNib(recverNibb, forCellReuseIdentifier: "reciver")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: - Show Keyboard
    func keyBoardWillShow(notification: NSNotification)
    {
        isKeyBoardopen = true
        var info = notification.userInfo!
        let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue()
        UIView.animateWithDuration(0.5, animations:
            { () -> Void in
                
                self.bottomView.constant = +(keyboardFrame.size.height)
            })
    }
    //MARK: - Hide Keyboard
    func keyBoardWillHide(notification: NSNotification)
    {
        isKeyBoardopen = false
       UIView.animateWithDuration(0.5, animations:
            { () -> Void in
                
                self.bottomView.constant = 0.0
            })
    }
 
    override func viewWillAppear(animated: Bool) {
    }

    @IBAction func moveBack(sender: UIButton){
        self.navigationController?.popViewControllerAnimated(true)
    }

    
    //TableView Delegate methods
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
    
        let message : XMPPMessageArchiving_Message_CoreDataObject = chatArr![indexPath.row] as! XMPPMessageArchiving_Message_CoreDataObject
        let msg : XMPPMessage = message.message
        
        if let img: String = msg.elementForName("attachment")?.stringValue()
        {
            return 205;
        }
        else
        {
            
            let time  = msg.attributeForName("time")?.stringValue()
            if(time == "")
            {
                let messageBody :NSString = message.body
                if(messageBody == "audioMessage" || messageBody == "contact")
                {
                    return 60
                }
                else if (messageBody == "YOU TOOK A SCREEN SHOT OF CHAT!")
                {
                    return 80
                }
                else
                {
                    let size : CGSize = TabelViewCell.getSizeforText(messageBody as String, andSize: self.view)
                    var rowHeight :CGFloat = 65.0;
                    
                    //rowHeight =  size.height + 30
                    

                    if (size.height > 100.0)
                    {
                        rowHeight =  size.height + 20
                    }
                    else if (size.height > 70.0)
                    {
                        rowHeight =  size.height + 37
                    }
                        
                    else
                    {
                        rowHeight =  size.height + 27
                    }
                    
                    return rowHeight;
                }
            }
            else
            {
                let size : CGSize = TabelViewCell.SizeforText("Timed meeesage received. Touch and hold to view", andSize: self.view)
                return size.height + 47;
            }
        }

    }
        
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return chatArr!.count;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let message : XMPPMessageArchiving_Message_CoreDataObject = chatArr![indexPath.row] as! XMPPMessageArchiving_Message_CoreDataObject
        if (message.isOutgoing)
        {
            let msg : XMPPMessage = message.message
            if let img: String = msg.elementForName("attachment")?.stringValue()
            {
                let cell = tableView.dequeueReusableCellWithIdentifier("media") as! SenderMediaCell
                
                cell.delegate = self
                
                cell.imgPlaceHolder.layer.cornerRadius =   cell.imgPlaceHolder.frame.size.width/16
                cell.imgPlaceHolder.clipsToBounds = true
                cell.imgPlaceHolder.layer.borderWidth = 2.0;
                cell.imgPlaceHolder.layer.borderColor = UIColor(red: 150/255.0, green: 150/255.0, blue: 150/255.0, alpha: 1.0).CGColor
                cell.backgroundColor = UIColor.clearColor()
                cell.selectionStyle=UITableViewCellSelectionStyle.None
                
                
            
                cell.playIcon.hidden = true
                
                cell.animation_img_view.hidden = true
                
                
                
                
                if(img == "")
                {
                    
                    let urlString :String? = msg.attributeForName("thumbnail").stringValue()
                    let url = NSURL(string:urlString!)
            
                    let imgView = UIImageView()
                    
                    imgView.sd_setImageWithURL(url, completed: { (image, error, cacheType, url) in
                        
                        cell.imgPlaceHolder.image = image
                    })
                    
                    let imgeType :String = msg.attributeForName("msgtype").stringValue()
                    if(imgeType == "image")
                    {
                        cell.playIcon.hidden = true
                        
                    }
                    else{
                        
                        cell.playIcon.hidden = false
                    }
            
                    
                    
                }
                else
                {
                    
                    let imgeType :String = msg.attributeForName("msgtype").stringValue()
                    if(imgeType == "image")
                    {
                        cell.playIcon.hidden = true
                        
                    }
                    else if(imgeType == "video"  || imgeType == "playvideo")
                    {
                       cell.playIcon.hidden = false
                    }
                    
                    let imageData = NSData(base64EncodedString:img, options:.IgnoreUnknownCharacters)
                    let image = UIImage(data: imageData!)
                   
                    cell.imgPlaceHolder.image = image
                    
                }

                cell.contentView.userInteractionEnabled = false
                
                return cell
                
                
            
            }
            
            let cell = tableView.dequeueReusableCellWithIdentifier("sender") as! ChatSenderTextTableViewCell
            cell.selectionStyle=UITableViewCellSelectionStyle.None
            cell.backgroundColor = UIColor.clearColor()
            
            cell.lbl_status.text = message.status
            let data = message.body.dataUsingEncoding(NSUTF8StringEncoding)
            let str = NSString(data: data!, encoding: NSNonLossyASCIIStringEncoding)
            
            cell.lbl_messageText.text = str as? String
            cell.contentView.userInteractionEnabled = false
            return cell
        }
        else{
            
            let msg : XMPPMessage = message.message
            if let img: String = msg.elementForName("attachment")?.stringValue()
            {
                let cell:ReceiverMediaCell = tableView.dequeueReusableCellWithIdentifier("rmedia") as! ReceiverMediaCell
                
                cell.selectionStyle=UITableViewCellSelectionStyle.None
                
                cell.delegate = self
                
                
                cell.imgPlaceHolder.layer.cornerRadius =   cell.imgPlaceHolder.frame.size.width/16
                cell.imgPlaceHolder.clipsToBounds = true
                cell.imgPlaceHolder.layer.borderWidth = 2.0;
                cell.imgPlaceHolder.layer.borderColor = UIColor(red: 150/255.0, green: 150/255.0, blue: 150/255.0, alpha: 1.0).CGColor
                
                cell.showCustomLoadIndicator()
                
                if(img == "")
                {
                    
                    
                    let urlString :String? = msg.attributeForName("thumbnail").stringValue()
                    let url = NSURL(string:urlString!)

                    cell.imgPlaceHolder .sd_setImageWithURL(url, placeholderImage: UIImage(named: "chat_imgplacehold"), completed: { (image, error, cacheType, url) in
                        
                        cell.removeCustomLoadIndicator()
                        
                    })

                }
                else
                {
                    let imageData = NSData(base64EncodedString:img, options: .IgnoreUnknownCharacters)
                    let image = UIImage(data: imageData!)
                    cell.removeCustomLoadIndicator()
                    cell.imgPlaceHolder.image = image
                }
                
                let imgeType :String = msg.attributeForName("msgtype").stringValue()
 
                if(imgeType == "video")
                {
                    cell.playIcon.hidden = true
                    cell.showCustomLoadIndicator()
                    
                }
                else if (imgeType == "playvideo")
                {
                    cell.playIcon.hidden = false
                    cell.removeCustomLoadIndicator()
                    
                }
                else
                {
                    cell.playIcon.hidden = true
                    cell.removeCustomLoadIndicator()
                }

                
               cell.contentView.userInteractionEnabled = false
                
                return cell

            }
            
            let cell:ChatReciverTextTableViewCell = tableView.dequeueReusableCellWithIdentifier("reciver") as! ChatReciverTextTableViewCell
            cell.selectionStyle=UITableViewCellSelectionStyle.None
            
            let data = message.body.dataUsingEncoding(NSUTF8StringEncoding)
            let str = NSString(data: data!, encoding: NSNonLossyASCIIStringEncoding)
            cell.lbl_messageText.text = str as? String
            
            cell.backgroundColor = UIColor.clearColor()
            cell.contentView.userInteractionEnabled = false
            
            return cell
            
        }
    }
    
    func showOnline ()
    {
        if(isUserOnline==true)
        {
            self.lblStatus.text = "Online"
        }
        else
        {
            self.lblStatus.text = "offline"
        }
    }
    
    
    func updateStatus(n:NSNotification)
    {
        
        let data = n.userInfo! as NSDictionary
        
        if Jid == data.objectForKey("from") as? String{
            
            if let s : NSString = data.objectForKey("status") as? NSString{
                
              //  print(s)
                let str  = s
                if str == "available"{
                    
                    isUserOnline = true
                }
                else{
                    
                    isUserOnline = false
                }
                
            }
            
          //  timer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: Selector("showOnline"), userInfo: nil, repeats:false)
        }
    }
    
   func getConverWithDelay(){
    
    if (self.parentViewController != nil){
        
        
        let arr:NSArray = (self.parentViewController?.childViewControllers)!
        
        if arr.count > 0{
            
            for vc:UIViewController in (self.parentViewController?.childViewControllers)!{
                
                if vc .isKindOfClass(ChatViewController){
                    
                    
                    NSTimer.scheduledTimerWithTimeInterval(0.2, target: self, selector: Selector("getConver"), userInfo: nil, repeats:false)
                    
                    break
                    
                    
                }
            }
        
    }
    
    

        
    }
    
    
    
        }
    
    func getConver()
    {
        let appDel = UIApplication.sharedApplication().delegate! as! AppDelegate
//        print(Jid)
        let newJID = Jid + "@" + kAppendedJidUrl
//        print(newJID)
        chatArr = appDel.getConversation(newJID)
        
        self.tabel.reloadData()
        NSTimer.scheduledTimerWithTimeInterval(0.2, target: self, selector: Selector("updateTabelViewatScrollToBottom"), userInfo: nil, repeats:false)
    }
    
    func updateTabelViewatScrollToBottom()
    {
        let numberOfSections = self.tabel.numberOfSections
        let numberOfRows = self.tabel.numberOfRowsInSection(numberOfSections-1)
        
        if numberOfRows > 0
        {
            let indexPath = NSIndexPath(forRow: numberOfRows-1, inSection: (numberOfSections-1))
            self.tabel.scrollToRowAtIndexPath(indexPath, atScrollPosition: UITableViewScrollPosition.Bottom, animated: true)
        }
    }
    
    @IBAction func btnSendPressed(sender: AnyObject) {
        
        if(txtMessage.text == "")
        {
            if (UIImagePickerController.isSourceTypeAvailable(.Camera)) {
                if UIImagePickerController.availableCaptureModesForCameraDevice(.Rear) != nil {
                    let imag = UIImagePickerController()
                    imag.delegate = self
                    imag.sourceType = UIImagePickerControllerSourceType.Camera;
                    imag.mediaTypes = [kUTTypeMovie as String];
                    imag.allowsEditing = false
                    imag.videoMaximumDuration = 30.0
                    self.presentViewController(imag, animated: true, completion: nil)
                    
                    
                } else {
                    CommomMethods.showAlert("Application cannot access the camera.", view: self)
                   
                }
            } else {
                CommomMethods.showAlert("Application cannot access the camera.", view: self)
                }
            return
        }
        else
        {
            let appDel = UIApplication.sharedApplication().delegate! as! AppDelegate
            let messageID:String = (appDel.xmppStream?.generateUUID())!
            let newJID = Jid + "@" + kAppendedJidUrl
            print(newJID)
            let msText :String = txtMessage.text!
            
            let data = msText.dataUsingEncoding(NSNonLossyASCIIStringEncoding)
            let str = NSString(data: data!, encoding: NSUTF8StringEncoding)
            
            
            
            appDel.xmppcon?.sendTextMessage(str as! String,and:newJID,audioLocalPath:"",andmsgTime: "", andmsgID: messageID, andwithstream: appDel.xmppStream,andwithToUser:toUser, withFromUser:NSUserDefaults.standardUserDefaults().objectForKey(kUserName) as! String)
            
            
            sendPushMessage(txtMessage.text!)
            
            
            
            txtMessage.text = ""
    }
    
    }
    
    
    // Finished recording a video
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        print("Got a video")
        
         self.dismissViewControllerAnimated(true, completion: nil)
        
        if let pickedVideo:NSURL = (info[UIImagePickerControllerMediaURL] as? NSURL) {
            
         
            let asset = AVURLAsset(URL:pickedVideo, options: nil)
            let imgGenerator = AVAssetImageGenerator(asset: asset)
            do {
                let cgImage = try imgGenerator.copyCGImageAtTime(CMTimeMake(0, 1), actualTime: nil)
                
                // !! check the error before proceeding
                let uiImage = UIImage(CGImage: cgImage)
                
                
                
                self.PostSendVideo(uiImage, path: pickedVideo)
                
            } catch {
                
            }

            
           
            
        }
        else if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage{
            
            let vc:TOCropViewController = TOCropViewController(image: pickedImage)
            
            vc.delegate = self
            
            self .presentViewController(vc, animated: true, completion: nil)
            
            
        }
        
        
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController)
    {
        picker.dismissViewControllerAnimated(true, completion: {
            // Anything you want to happen when the user saves an video
        })
    }
    
    func getDocumentsDirectory() -> NSString {
        let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    func cropViewController(cropViewController: TOCropViewController!, didCropToImage image: UIImage!, withRect cropRect: CGRect, angle: Int) {
        
        if (image != nil){
            
            print("received")
            
            if let data = UIImagePNGRepresentation(image) {
                let filename = getDocumentsDirectory().stringByAppendingPathComponent("image.png")
                data.writeToFile(filename, atomically: true)
                
                self.PostSendImage(image, path: NSURL(fileURLWithPath: filename))
                
                
                
            }
        }
        self .dismissViewControllerAnimated(true, completion: nil)
    }
    
    func cropViewController(cropViewController: TOCropViewController!, didFinishCancelled cancelled: Bool) {
        
       self .dismissViewControllerAnimated(true, completion: nil)
    }
    
   
    
    @IBAction func openArrachment(sender: AnyObject) {
        let settingsActionSheet: UIAlertController = UIAlertController(title:nil, message:nil, preferredStyle:UIAlertControllerStyle.ActionSheet)
       
        settingsActionSheet.addAction(UIAlertAction(title:"Chose Existing Photo", style:UIAlertActionStyle.Default, handler:{ action in
            
            self.uploadCamImage(true)
           
            
        }))
        settingsActionSheet.addAction(UIAlertAction(title:"Chose Existing Video", style:UIAlertActionStyle.Default, handler:{ action in
            
            self.uploadVideo()
           
            
        }))
//        settingsActionSheet.addAction(UIAlertAction(title:"Share Location", style:UIAlertActionStyle.Default, handler:{ action in
//            
//        }))
//        settingsActionSheet.addAction(UIAlertAction(title:"Share Contact", style:UIAlertActionStyle.Default, handler:{ action in
//            
//        }))
        
        settingsActionSheet.addAction(UIAlertAction(title:"Cancel", style:UIAlertActionStyle.Cancel, handler:nil))
        presentViewController(settingsActionSheet, animated:true, completion:nil)

        
    }
    
    
    
    @IBAction func showUserProfile(sender: AnyObject) {
        
         let profileVC:ProfileDetailViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("profileVC") as! ProfileDetailViewController
        self.navigationController?.pushViewController(profileVC, animated:true)
    }
    
    
        func textFieldDidBeginEditing(textField: UITextField)
        {
            //
        }
        func textFieldShouldReturn(textField: UITextField) -> Bool
        {
            textField.resignFirstResponder()
            return true
        }
    
   
    @IBAction func swipeBackPressed(sender: AnyObject) {
        
         NSNotificationCenter.defaultCenter().postNotificationName("indexChangedBack", object: nil)
        
    }
    
    
    func  uploadCamImage(isFromPhoto:Bool)
    {
        
        if isFromPhoto==true {
            
            openLibrary()
        }
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)
        {
            print("Button capture")
            let imag = UIImagePickerController()
            imag.delegate = self
            imag.sourceType = UIImagePickerControllerSourceType.Camera
            imag.mediaTypes = [kUTTypeImage as String]
            imag.allowsEditing = false
            self.presentViewController(imag, animated: true, completion: nil)
        }
        else{
            
            openLibrary()
        }
    }
    
    func openLibrary(){
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary){
            
            let imag = UIImagePickerController()
            imag.delegate = self
            imag.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
            
            imag.allowsEditing = false
            self.presentViewController(imag, animated: true, completion: nil)
        }
    }
    
    
    func uploadVideo()
    {
        
        let imag = UIImagePickerController()
        imag.delegate = self
        imag.sourceType = UIImagePickerControllerSourceType.PhotoLibrary;
        imag.mediaTypes = [kUTTypeMovie as String]
        imag.allowsEditing = false
        self.presentViewController(imag, animated: true, completion: nil)
    }
    
    
    //Sending chat images
    
    func PostSendImage(image:UIImage, path:NSURL)
    {
        print(self.Jid)
        let newJID = self.Jid + "@" + kAppendedJidUrl
        let data = UIImageJPEGRepresentation(image, 1.0)
        let base64String = data!.base64EncodedStringWithOptions(.Encoding64CharacterLineLength)
        let postsEndpoint: String = kBaseServerUrl + "u_file"
        let appDel = UIApplication.sharedApplication().delegate! as! AppDelegate
        let messageID:String = (appDel.xmppStream?.generateUUID())!
        
        
        
        appDel.xmppcon?.addSendingImagewithUrl(messageID, andjid: newJID, withImage: base64String, andtime: "", andType: "image", withToUser: toUser, withFromUser: NSUserDefaults.standardUserDefaults().objectForKey(kUserName) as! String)
        
        
        Alamofire.upload(
            .POST,
            postsEndpoint,
            multipartFormData: { multipartFormData in
            
                multipartFormData.appendBodyPart(fileURL: path, name: "file")
                multipartFormData.appendBodyPart(data: NSUserDefaults.standardUserDefaults().objectForKey(kUserid)!.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)!, name :"user_id")
                multipartFormData.appendBodyPart(data:messageID.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)!, name :"message_id")
               
            },
            encodingCompletion: { encodingResult in
                switch encodingResult {
                    
                case .Success(let upload, _, _):
                    upload.responseJSON { Response in
                        
                        
                        if let data = Response.data as NSData!
                        {
                            let json = JSON(data:data)
                            
                            let status = json["status"].stringValue
                            
                            if (status == "ok")
                            {
                                
                                
                             
                                        
                                       
                                        appDel.xmppcon?.sendImageWithUrl("image", andjid:newJID, andwithstream:appDel.xmppStream, withImageUrl:json["file_path"].stringValue, iththumbnailImage:json["thumbnail_file_path"].stringValue, andtime:"", andmsgID:json["message_id"].stringValue, withToUser:self.toUser, withFromUser:NSUserDefaults.standardUserDefaults().objectForKey(kUserName) as! String)
                                
                                
                                self.sendPushMessage("send you an image")

                               
                                
                               
                                
                            }
                            else{
                                
                                
                                
                            }
                            
                            
                        }
                        
                    }
                    
                case .Failure(let error):
                    
                    
                    
                    dispatch_async(dispatch_get_main_queue(),
                        {
                            
                            CommomMethods.showAlert("Something went wrong please check your internet connection and try again later", view: self)
                            MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
                            
                            
                        }
                    )
                    
                    
                }
                
            }
        )

        

    
    }
    
    
    
    
    // Posting Video
    
    func PostSendVideo(image:UIImage, path:NSURL)
    {
        
        let newJID = self.Jid + "@" + kAppendedJidUrl

        let postsEndpoint: String = kBaseServerUrl + "u_file"
        let appDel = UIApplication.sharedApplication().delegate! as! AppDelegate
        let messageID:String = (appDel.xmppStream?.generateUUID())!
        
        
        
        
        appDel.xmppcon?.sendVideoData(messageID, andjid: newJID, andwithstream:appDel.xmppStream, withImage:image , andtime:"",andvideoUrl:path.absoluteString, andtype:"video",withtoUser:toUser,withFrom:NSUserDefaults.standardUserDefaults().objectForKey(kUserName) as! String)
        
        
        Alamofire.upload(
            .POST,
            postsEndpoint,
            multipartFormData: { multipartFormData in
                
                multipartFormData.appendBodyPart(fileURL: path, name: "file")
                multipartFormData.appendBodyPart(data: NSUserDefaults.standardUserDefaults().objectForKey(kUserid)!.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)!, name :"user_id")
                multipartFormData.appendBodyPart(data:messageID.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)!, name :"message_id")
                
                 multipartFormData.appendBodyPart(data:path.path!.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)!, name :"localpath")
                
            },
            encodingCompletion: { encodingResult in
                switch encodingResult {
                    
                case .Success(let upload, _, _):
                    upload.responseJSON { Response in
                        
                        
                        if let data = Response.data as NSData!
                        {
                            let json = JSON(data:data)
                            
                            let status = json["status"].stringValue
                            
                            if (status == "ok")
                            {
                                
//                            print(json["localpath"].stringValue)
//                                
//                                print(json["message_id"].stringValue)
//                                
//                               print(json["localpath"].stringValue)
                            
                                appDel.xmppcon?.sendVideoWithUrl("playVideo", andjid:newJID, andwithstream:appDel.xmppStream, withImageUrl:json["file_path"].stringValue ,localVideoPath:json["localpath"].stringValue, iththumbnailImage:image, andtime:"", andmsgID: json["message_id"].stringValue, withToUser:self.toUser, withFromUser:NSUserDefaults.standardUserDefaults().objectForKey(kUserName) as! String)
                                
                                self.sendPushMessage("send a video")
                                
                            }
                            else{
                                
                                
                                
                            }
                            
                            
                        }
                        
                    }
                    
                case .Failure(let error):
                    
//                    print(error)
                    
                    dispatch_async(dispatch_get_main_queue(),
                        {
                            
                            CommomMethods.showAlert("Something went wrong please check your internet connection and try again later", view: self)
                            MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
                            
                            
                        }
                    )
                    
                    
                }
                
            }
        )
        
        
        
        
    }

    // Receiver Media Cell Delegate
    
    func btnRecvImageVideoClicked(sender:UIButton){
        
        let touchPoint = sender.convertPoint(CGPoint.zero, toView: self.tabel)
        
        let indexPath = self.tabel .indexPathForRowAtPoint(touchPoint)
        
        let message : XMPPMessageArchiving_Message_CoreDataObject = chatArr![indexPath!.row] as! XMPPMessageArchiving_Message_CoreDataObject
        let msg : XMPPMessage = message.message

        let imgeType :String = msg.attributeForName("msgtype").stringValue()
        if(imgeType == "image")
        {
         let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("ImageViewerVC") as! ImageViewerVC
            
            let urlString :String? = msg.attributeForName("ImageUrl").stringValue()
            
            
            
            vc.imageUrl = urlString
            
        self.presentViewController(vc, animated: true, completion: nil)
            
        }
        
        else if imgeType == "playvideo"{
            
            
            let message : XMPPMessageArchiving_Message_CoreDataObject = chatArr![indexPath!.row] as! XMPPMessageArchiving_Message_CoreDataObject
            let msg : XMPPMessage = message.message
//            print("PlayVideo")
            let urlString :String? = msg.attributeForName("videoUrl").stringValue()
            
            

            moviePlayerCntroller = MPMoviePlayerViewController()
            
            moviePlayerCntroller = MPMoviePlayerViewController(contentURL:  NSURL(string:urlString!))
            
            
            moviePlayerCntroller?.moviePlayer.fullscreen = true
            moviePlayerCntroller?.moviePlayer.controlStyle = .Fullscreen
            
            moviePlayerCntroller.moviePlayer.prepareToPlay()
            
            self.presentMoviePlayerViewControllerAnimated(moviePlayerCntroller)
            
            
            moviePlayerCntroller.moviePlayer.play()
            

            
        }
    
    }
    // Sender Media Cell Delegate
    func btnImageVideoClicked(sender:UIButton){
        
        let touchPoint = sender.convertPoint(CGPoint.zero, toView: self.tabel)
        
        let indexPath = self.tabel .indexPathForRowAtPoint(touchPoint)
        
        let message : XMPPMessageArchiving_Message_CoreDataObject = chatArr![indexPath!.row] as! XMPPMessageArchiving_Message_CoreDataObject
        let msg : XMPPMessage = message.message
        
        let imgeType :String = msg.attributeForName("msgtype").stringValue()
        if(imgeType == "image")
        {
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("ImageViewerVC") as! ImageViewerVC
            
            var urlString :String?
            
            if msg.attributeForName("ImageUrl") == nil {
                
                let img: String = (msg.elementForName("attachment")?.stringValue())!
                
                vc.imageData = img
                
                
            }
            else{
                
                urlString = msg.attributeForName("ImageUrl").stringValue()
                
                
            }
            
            vc.imageUrl = urlString
            
            self.presentViewController(vc, animated: true, completion: nil)
            
        }
        else if imgeType == "playvideo" || imgeType == "video"{
            
            
            let message : XMPPMessageArchiving_Message_CoreDataObject = chatArr![indexPath!.row] as! XMPPMessageArchiving_Message_CoreDataObject
            let msg : XMPPMessage = message.message
           // print("PlayVideo")
            let urlString :String? = msg.attributeForName("videoLocalUrl").stringValue()
            
           // print(urlString)
            
            moviePlayerCntroller = MPMoviePlayerViewController()
            
            //print(NSURL(fileURLWithPath:urlSƒtring!))
            
       moviePlayerCntroller = MPMoviePlayerViewController(contentURL:  NSURL(fileURLWithPath:urlString!))
            
            moviePlayerCntroller?.moviePlayer.fullscreen = true
            moviePlayerCntroller?.moviePlayer.controlStyle = .Fullscreen
            
            moviePlayerCntroller.moviePlayer.prepareToPlay()
            
            self.presentMoviePlayerViewControllerAnimated(moviePlayerCntroller)
            
            
            moviePlayerCntroller.moviePlayer.play()
            
        }
        
    }
    
    func sendPushMessage(msg:String){
        
        let str = kBaseServerUrl + "s_message"
        let fullUrl  = NSURL(string: str)
        
        let newPost = ["phone_number":Jid,"user_id":NSUserDefaults.standardUserDefaults().objectForKey(kUserid)!,"message":msg] as [String:AnyObject]
        
        
        let request = Alamofire.request(.POST, fullUrl!, parameters: newPost, encoding: .URL)
        request.responseJSON
            { response in
                
                if let anError = response.result.error
                {
                    print("error calling POST on /posts")
                    print(anError)
                    print(anError.localizedDescription)
                    print(anError.debugDescription)
                    print(anError.localizedFailureReason)
                    
                    MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
                }
                else if let data = response.data as NSData!
                {
                    
                    //for testing
                    
                    let json = JSON(data:data)
                    
                    let status = json["status"].stringValue
                    
                    if (status == "ok")
                    {
                        print("OK")
                    }
                    MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
                    
                    
                    
                    
                    
                }
                
                
        }

    }
    
}
