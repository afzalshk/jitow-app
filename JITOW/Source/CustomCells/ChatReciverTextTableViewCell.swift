//
//  ChatReciverTextTableViewCell.swift
//  JITOW
//
//  Created by Irfan Malik on 2/15/16.
//  Copyright © 2016 LC. All rights reserved.
//

import UIKit

class ChatReciverTextTableViewCell: UITableViewCell {

    @IBOutlet weak var bgImageView: UIImageView!
    @IBOutlet weak var lbl_messageText: UILabel!
    
    
    override func awakeFromNib()
    {
    
    }
    
    override func layoutSubviews()
    {
        super.layoutSubviews()
        let  msgSize :CGSize = TabelViewCell.getSizeforText(self.lbl_messageText.text, andSize:self.contentView)
        
        
        self.bgImageView.frame = CGRectMake(CGRectGetMinX(self.contentView.frame) + 14.0,
            CGRectGetMinY(self.contentView.frame) + 15.0,
            msgSize.width + 30.0,
            msgSize.height + 14.0);
        
        self.lbl_messageText.frame = CGRectMake(CGRectGetMinX(self.bgImageView.frame) + 15.0,
            CGRectGetMinY(self.bgImageView.frame) + 5.0,
            msgSize.width,
            msgSize.height);
        self.bgImageView.image = TabelViewCell.recieverStreatchImage()
    }
}
