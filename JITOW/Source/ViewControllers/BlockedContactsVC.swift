//
//  BlockedContactsVC.swift
//  JITOW
//
//  Created by Mac103 on 5/2/16.
//  Copyright © 2016 LC. All rights reserved.
//

import Foundation
import UIKit
import MBProgressHUD
import Alamofire
class BlockedContactsVC: UIViewController,BlockContactCellDelegate {
    
    @IBOutlet weak var tblBlockContact: UITableView!
    var allContacts:NSMutableArray = NSMutableArray()
    
    override func viewDidLoad() {
        
        self.navigationController?.navigationBarHidden = false
        
        customBackgroundButton()
        
       
        
        
        getAllContacts()
        
        
    }
    override func viewDidDisappear(animated: Bool) {
        
        self.navigationController?.navigationBarHidden = true
    }
    
    func customBackgroundButton(){
        
        self.title = "All Blocked Users"
        self.navigationItem.hidesBackButton = true
        let myBackButton:UIButton = UIButton(type: UIButtonType.Custom)
        myBackButton.addTarget(self, action: "popToRoot:", forControlEvents: UIControlEvents.TouchUpInside)
        myBackButton.setTitle("", forState: UIControlState.Normal)
        myBackButton.setImage(UIImage(named: "arrow-left"), forState: UIControlState.Normal)
        myBackButton.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
        myBackButton.sizeToFit()
        let myCustomBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myBackButton)
        self.navigationItem.leftBarButtonItem  = myCustomBackButtonItem
    }
    func popToRoot(sender:UIBarButtonItem){
        self.navigationController!.popViewControllerAnimated(true)
    }
    
    
    //MARK: - UitabelView
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        
        
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
    
        return allContacts.count
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
   
    
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! BlockContactCell
        
        
        let u:UserModel = allContacts.objectAtIndex(indexPath.row) as! UserModel
        
        cell.lblUserName.text = u.username as? String
        
        cell.delegate = self
        
        return cell
        
    }
    
    func getAllContacts(){
        
        MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        
        let str = kBaseServerUrl + "g_blocklist"
        let fullUrl  = NSURL(string: str)
        
        let userid = NSUserDefaults.standardUserDefaults().objectForKey(kUserid) as? String
        
        print(userid)
        
        let newPost = ["user_id":userid!]as [String:AnyObject]
        
        
        
        let request = Alamofire.request(.POST, fullUrl!, parameters: newPost, encoding: .URL)
        request.responseJSON
            { response in
                
                if let anError = response.result.error
                {
                    
                    
                    
                    dispatch_async(dispatch_get_main_queue(),
                        {
                            
                            CommomMethods.showAlert("something went wrong please check your internet connection and try again later", view: self)
                            MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
                            
                            
                        }
                    )
                    
                    print("error calling POST on /posts")
                    print(anError)
                    
                }
                else if let data = response.data as NSData!
                {
                    
                    
                    let json = JSON(data:data)
                    
                    let status = json["status"].stringValue
                    
                    if (status == "ok")
                    {
                        
                        self.allContacts.removeAllObjects()
                        
                        let pendingRequests : Array = json["response"].arrayValue
                        
                        print(pendingRequests)
                        
                        if pendingRequests.count > 0{
                            
                            
                            for d in pendingRequests{
                                
                                
                                let user = UserModel()
                                
                                if let str:NSString = d["id"].string{
                                    
                                    user.userid = str
                                    
                                }
                                if let str:NSString = d["username"].string{
                                    
                                    user.username = str
                                    
                                }
                                
                                
                                if let str:NSString = d["email"].string{
                                    
                                    user.email = str
                                    
                                }
                                if let str:NSString = d["dob"].string{
                                    
                                    user.dob = str
                                }
                                if let str:NSString = d["phone_number"].string{
                                    
                                    user.phoneNumber = str
                                }
                                if let str:NSString = d["video_path"].string{
                                    
                                    user.videoUrl = str
                                    
                                }
                                
                                self.allContacts.addObject(user)
                                
                            }
                            
                            self.tblBlockContact.reloadData()
                            
                            
                        }
                        
                        
                        
                        
                        
                        
                    }
                    
                    
                    dispatch_async(dispatch_get_main_queue(),
                        {
                            
                            MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
                            
                            
                        }
                    )
                    
                    
                }
                
                
        }
        
        
        
    }
    
    
    func unblockContact(sender:UIButton){
        
        let touchPoint = sender.convertPoint(CGPoint.zero, toView: self.tblBlockContact)
        
        let indexPath = self.tblBlockContact .indexPathForRowAtPoint(touchPoint)
        
       
        
        let user:UserModel = allContacts.objectAtIndex((indexPath?.row)!) as! UserModel
        
        
        let str = kBaseServerUrl + "nb_contact"
        let fullUrl  = NSURL(string: str)
        
        let newPost = ["contact":(user.phoneNumber as? String)!,"user_id":NSUserDefaults.standardUserDefaults().objectForKey(kUserid)!] as [String:AnyObject]
        
        
        let request = Alamofire.request(.POST, fullUrl!, parameters: newPost, encoding: .URL)
        request.responseJSON
            { response in
                
                if let anError = response.result.error
                {
                    print(anError)
                    
                }
                else if let data = response.data as NSData!
                {
                    
                    
                    let json = JSON(data:data)
                    
                    let status = json["status"].stringValue
                    
                    if (status == "ok")
                    {
                        
                        
                    }
                    
                    
                    
                }
                
                
        }
        
        
        
        
        
        
        
        
        allContacts.removeObjectAtIndex((indexPath?.row)!)
        
        if allContacts.count > 0 {
            
        
            self.tblBlockContact.beginUpdates()
            self.tblBlockContact!.deleteRowsAtIndexPaths([indexPath!], withRowAnimation: UITableViewRowAnimation.Fade)
            self.tblBlockContact.endUpdates()
            
            
            
        }
        else{
            
           
            
            self.tblBlockContact.reloadData()
            
            
        }
        
    }


}