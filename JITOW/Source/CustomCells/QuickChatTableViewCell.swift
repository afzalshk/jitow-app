//
//  QuickChatTableViewCell.swift
//  JITOW
//
//  Created by Irfan Malik on 2/11/16.
//  Copyright © 2016 LC. All rights reserved.
//

import UIKit

class QuickChatTableViewCell: UITableViewCell {

   
    @IBOutlet weak var view_read: UIView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var view_container: UIView!
    @IBOutlet weak var profile_img: UIImageView!
    @IBOutlet weak var imgStatus: UIImageView!
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
        // Do any additional setup after loading the view, typically from a nib.
        view_container.layer.cornerRadius = 4
        view_container.layer.borderWidth = 1.0
        view_container.layer.borderColor =  UIColor(red: 217.0/255, green: 217.0/255, blue: 217.0/255, alpha: 1.0).CGColor
        
        profile_img.layer.borderWidth = 1.0
        profile_img.layer.borderColor = UIColor.clearColor().CGColor
        profile_img.layer.cornerRadius = 10.0
        
        
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
   
}
