//
//  XMPPConnection.m
//  iPhoneXMPP
//
//  Created by Mac101 on 8/11/15.
//  Copyright (c) 2015 XMPPFramework. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "XMPPConnection.h"
#import "XMPPMessageDeliveryReceipts.h"
#import "XMPPMessageArchiving_Message_CoreDataObject.h"
#import "XMPPUserMemoryStorageObject.h"
#import "XMPPRosterMemoryStorage.h"
#import "XmppOfflineFriends.h"
#import "XMPPMessage.h"
#import "XMPPJID.h"





@implementation XMPPConnection
{

}
-(void)setupEarySetting:(XMPPStream *)stream
{
    
    
    // Setup xmpp stream
    //
    // The XMPPStream is the base class for all activity.
    // Everything else plugs into the xmppStream, such as modules/extensions and delegates.
    
    
    
#if !TARGET_IPHONE_SIMULATOR
    {
        // Want xmpp to run in the background?
        //
        // P.S. - The simulator doesn't support backgrounding yet.
        //        When you try to set the associated property on the simulator, it simply fails.
        //        And when you background an app on the simulator,
        //        it just queues network traffic til the app is foregrounded again.
        //        We are patiently waiting for a fix from Apple.
        //        If you do enableBackgroundingOnSocket on the simulator,
        //        you will simply see an error message from the xmpp stack when it fails to set the property.
        
        stream.enableBackgroundingOnSocket = YES;
    }
#endif
    
    // Setup reconnect
    //
    // The XMPPReconnect module monitors for "accidental disconnections" and
    // automatically reconnects the stream for you.
    // There's a bunch more information in the XMPPReconnect header file.
    
    xmppReconnect = [[XMPPReconnect alloc] init];
    
    // Setup roster
    //
    // The XMPPRoster handles the xmpp protocol stuff related to the roster.
    // The storage for the roster is abstracted.
    // So you can use any storage mechanism you want.
    // You can store it all in memory, or use core data and store it on disk, or use core data with an in-memory store,
    // or setup your own using raw SQLite, or create your own storage mechanism.
    // You can do it however you like! It's your application.
    // But you do need to provide the roster with some storage facility.
    
    xmppRosterStorage = [[XMPPRosterCoreDataStorage alloc] init];
    //	xmppRosterStorage = [[XMPPRosterCoreDataStorage alloc] initWithInMemoryStore];
    
    xmppRoster = [[XMPPRoster alloc] initWithRosterStorage:xmppRosterStorage];
    
    xmppRoster.autoFetchRoster = YES;
    xmppRoster.autoAcceptKnownPresenceSubscriptionRequests = YES;
    
    // Setup vCard support
    //
    // The vCard Avatar module works in conjuction with the standard vCard Temp module to download user avatars.
    // The XMPPRoster will automatically integrate with XMPPvCardAvatarModule to cache roster photos in the roster.
    
    xmppvCardStorage = [XMPPvCardCoreDataStorage sharedInstance];
    xmppvCardTempModule = [[XMPPvCardTempModule alloc] initWithvCardStorage:xmppvCardStorage];
    
    xmppvCardAvatarModule = [[XMPPvCardAvatarModule alloc] initWithvCardTempModule:xmppvCardTempModule];
    
    // Setup capabilities
    //
    // The XMPPCapabilities module handles all the complex hashing of the caps protocol (XEP-0115).
    // Basically, when other clients broadcast their presence on the network
    // they include information about what capabilities their client supports (audio, video, file transfer, etc).
    // But as you can imagine, this list starts to get pretty big.
    // This is where the hashing stuff comes into play.
    // Most people running the same version of the same client are going to have the same list of capabilities.
    // So the protocol defines a standardized way to hash the list of capabilities.
    // Clients then broadcast the tiny hash instead of the big list.
    // The XMPPCapabilities protocol automatically handles figuring out what these hashes mean,
    // and also persistently storing the hashes so lookups aren't needed in the future.
    //
    // Similarly to the roster, the storage of the module is abstracted.
    // You are strongly encouraged to persist caps information across sessions.
    //
    // The XMPPCapabilitiesCoreDataStorage is an ideal solution.
    // It can also be shared amongst multiple streams to further reduce hash lookups.
    
    xmppCapabilitiesStorage = [XMPPCapabilitiesCoreDataStorage sharedInstance];
    xmppCapabilities = [[XMPPCapabilities alloc] initWithCapabilitiesStorage:xmppCapabilitiesStorage];
    
    xmppCapabilities.autoFetchHashedCapabilities = YES;
    xmppCapabilities.autoFetchNonHashedCapabilities = NO;
    
    // Activate xmpp modules
    
    [xmppReconnect         activate:stream];
    [xmppRoster            activate:stream];
    [xmppvCardTempModule   activate:stream];
    [xmppvCardAvatarModule activate:stream];
    [xmppCapabilities      activate:stream];
    
    // Add ourself as a delegate to anything we may be interested in
    
    [xmppRoster addDelegate:self delegateQueue:dispatch_get_main_queue()];
    
    
    
    
    // Optional:
    //
    // Replace me with the proper domain and port.
    // The example below is setup for a typical google talk account.
    //
    // If you don't supply a hostName, then it will be automatically resolved using the JID (below).
    // For example, if you supply a JID like 'user@quack.com/rsrc'
    // then the xmpp framework will follow the xmpp specification, and do a SRV lookup for quack.com.
    //
    // If you don't specify a hostPort, then the default (5222) will be used.
    
    //	[xmppStream setHostName:@"talk.google.com"];
    //	[xmppStream setHostPort:5222];
    
    
    // You may need to alter these settings depending on the server you're connecting to
    customCertEvaluation = YES;
    
    
    XMPPMessageDeliveryReceipts* xmppMessageDeliveryRecipts = [[XMPPMessageDeliveryReceipts alloc] initWithDispatchQueue:dispatch_get_main_queue()];
    xmppMessageDeliveryRecipts.autoSendMessageDeliveryReceipts = YES;
    xmppMessageDeliveryRecipts.autoSendMessageDeliveryRequests = YES;
    [xmppMessageDeliveryRecipts activate:stream];
    
    
    
    
    
    
}


-(void)sendMessage :(NSString *)msg and :(NSString *)jid  andwithstream:(XMPPStream *)stream withImage:(UIImage *)imagePic andtime:(NSString *)time andtype:(NSString *)type
{
        NSString *base64String;
        NSXMLElement *message = [NSXMLElement elementWithName:@"message"];
        [message addAttributeWithName:@"to" stringValue:jid];
        [message addAttributeWithName:@"id" stringValue:msg];
        NSXMLElement *body = [NSXMLElement elementWithName:@"body"];
    
        if ([type isEqualToString:@"video"])
        {
            [message addAttributeWithName:@"type" stringValue:@"video"];
        }
        else
        {
            [message addAttributeWithName:@"type" stringValue:@"image"];
        }
    
        [body setStringValue:@"testing image"];
        if (![time isEqualToString:@""])
        {
            [message addAttributeWithName:@"time" stringValue:time];
        }
        else
        {
            [message addAttributeWithName:@"time" stringValue:@""];
        }
    

        if([imagePic isKindOfClass:[UIImage class]])
            
        {
           NSXMLElement *attachment = [NSXMLElement elementWithName:@"attachment"];
            NSData *dataPic =  UIImageJPEGRepresentation(imagePic, 0.1);
            
            NSXMLElement *photo = [NSXMLElement elementWithName:@"PHOTO"];
            
            NSXMLElement *binval = [NSXMLElement elementWithName:@"BINVAL"];
            
            [photo addChild:binval];
            
            base64String = [dataPic base64EncodedStringWithOptions:0];
            
            [attachment setStringValue:base64String];
            
            [message addChild:attachment];
            
        }
      [message addChild:body];
    [stream sendElement:message];
}



#pragma mark TextMessage

#pragma mark TextMessage

-(void)sendTextMessage :(NSString *)msg and:(NSString *)jid audioLocalPath:(NSString *)localurl andmsgTime:(NSString *)time andmsgID:(NSString*)msgID andwithstream:(XMPPStream *)stream andwithToUser:(NSString*)sendToUser withFromUser:(NSString*)fromUser
{
    
    NSXMLElement *body = [NSXMLElement elementWithName:@"body"];
    [body setStringValue:msg];
    NSXMLElement *message = [NSXMLElement elementWithName:@"message"];
    [message addAttributeWithName:@"sendToUser" stringValue:sendToUser];
    [message addAttributeWithName:@"sendFromUser" stringValue:fromUser];
    [message addAttributeWithName:@"id" stringValue:msgID];
    [message addAttributeWithName:@"type" stringValue:@"chat"];
    [message addAttributeWithName:@"msgtype" stringValue:@"chat"];
    [message addAttributeWithName:@"to" stringValue:jid];
    [message addAttributeWithName:@"time" stringValue:time];
    [message addAttributeWithName:@"localurl" stringValue:localurl];
    [message addChild:body];
    [stream sendElement:message];
}


#pragma mark imageDataSend

-(void)sendVideoData:(NSString *)msgID andjid:(NSString *)jid  andwithstream:(XMPPStream *)stream withImage:(UIImage *)imagePic andtime:(NSString *)time  andvideoUrl:(NSString *)videoUrl andtype:(NSString *)type withtoUser:(NSString*)sendToUser withFrom:(NSString*)fromUser
{
    NSXMLElement *body = [NSXMLElement elementWithName:@"body"];
    [body setStringValue:@"testing image"];
    NSXMLElement *message = [NSXMLElement elementWithName:@"message"];
    [message addAttributeWithName:@"id" stringValue:msgID];
    [message addAttributeWithName:@"type" stringValue:@"chat"];
    [message addAttributeWithName:@"msgtype" stringValue:@"video"];
    [message addAttributeWithName:@"sendToUser" stringValue:sendToUser];
    [message addAttributeWithName:@"sendFromUser" stringValue:fromUser];
    [message addAttributeWithName:@"to" stringValue:jid];
    [message addAttributeWithName:@"time" stringValue:time];
    [message addAttributeWithName:@"videoLocalUrl" stringValue:videoUrl];
    [message addChild:body];
    
    NSXMLElement *attachment = [NSXMLElement elementWithName:@"attachment"];
    NSData *dataPic =  UIImageJPEGRepresentation(imagePic, 0.1);
    NSString *base64String = [dataPic base64EncodedStringWithOptions:0];
    [attachment setStringValue:base64String];
    [message addChild:attachment];
    
    [stream sendElement:message];
}



#pragma mark ImageSend
-(void)sendImageWithUrl:(NSString *)msg andjid:(NSString *)jid  andwithstream:(XMPPStream *)stream withImageUrl:(NSString *)ImageUrl iththumbnailImage:(NSString *)thumbnail andtime:(NSString *)time andmsgID:(NSString *)msgID withToUser:(NSString*)user withFromUser:(NSString*)fromUser
{
    NSXMLElement *body = [NSXMLElement elementWithName:@"body"];
    [body setStringValue:msg];
    NSXMLElement *message = [NSXMLElement elementWithName:@"message"];
    [message addAttributeWithName:@"id" stringValue:msgID];
    [message addAttributeWithName:@"type" stringValue:@"chat"];
    
//    [message addAttributeWithName:user stringValue:@"sendToUser"];
//    
//    [message addAttributeWithName:fromUser stringValue:@"sendFromUser"];
    
     [message addAttributeWithName:@"msgtype" stringValue:@"image"];
    [message addAttributeWithName:@"to" stringValue:jid];
    [message addAttributeWithName:@"time" stringValue:@""];
    [message addAttributeWithName:@"ImageUrl" stringValue:ImageUrl];
    [message addAttributeWithName:@"thumbnail" stringValue:thumbnail];
    [message addChild:body];
    
    NSXMLElement *attachment = [NSXMLElement elementWithName:@"attachment"];
    [attachment setStringValue:@""];
    [message addChild:attachment];
    
    [stream sendElement:message];
}

-(void)sendMapWithUrl:(NSString *)msg andjid:(NSString *)jid  andwithstream:(XMPPStream *)stream withImageUrl:(NSString *)ImageUrl iththumbnailImage:(NSString *)thumbnail andtime:(NSString *)time andmsgID:(NSString *)msgID andlocation:(NSString *)locationUrl
{
    NSXMLElement *body = [NSXMLElement elementWithName:@"body"];
    [body setStringValue:msg];
    NSXMLElement *message = [NSXMLElement elementWithName:@"message"];
    [message addAttributeWithName:@"id" stringValue:msgID];
    [message addAttributeWithName:@"type" stringValue:@"chat"];
    [message addAttributeWithName:@"msgtype" stringValue:@"mape"];
    [message addAttributeWithName:@"to" stringValue:jid];
    [message addAttributeWithName:@"time" stringValue:time];
    [message addAttributeWithName:@"mapeUrl" stringValue:locationUrl];
    [message addAttributeWithName:@"ImageUrl" stringValue:ImageUrl];
    [message addAttributeWithName:@"thumbnail" stringValue:thumbnail];
    [message addChild:body];
    
    NSXMLElement *attachment = [NSXMLElement elementWithName:@"attachment"];
    [attachment setStringValue:@""];
    [message addChild:attachment];
    
    [stream sendElement:message];
}

#pragma mark VideoSend

-(void)sendVideoWithUrl:(NSString *)msg andjid:(NSString *)jid  andwithstream:(XMPPStream *)stream withImageUrl:(NSString *)VideoUrl  localVideoPath:(NSString *)localVideoUrl iththumbnailImage:(UIImage *)thumbnail andtime:(NSString *)time andmsgID:(NSString *)msgID withToUser:(NSString*)user withFromUser:(NSString*)fromUser
{
    NSXMLElement *body = [NSXMLElement elementWithName:@"body"];
    [body setStringValue:msg];
    
    NSXMLElement *message = [NSXMLElement elementWithName:@"message"];
    [message addAttributeWithName:@"id" stringValue:msgID];
    [message addAttributeWithName:@"type" stringValue:@"chat"];
//    [message addAttributeWithName:user stringValue:@"sendToUser"];
//    
//    [message addAttributeWithName:fromUser stringValue:@"sendFromUser"];
    
    [message addAttributeWithName:@"msgtype" stringValue:@"playvideo"];
    [message addAttributeWithName:@"to" stringValue:jid];
    [message addAttributeWithName:@"videoUrl" stringValue:VideoUrl];
    [message addAttributeWithName:@"time" stringValue:@""];
    [message addAttributeWithName:@"videoLocalUrl" stringValue:localVideoUrl];
    
    
    [message addChild:body];
    
    NSXMLElement *attachment = [NSXMLElement elementWithName:@"attachment"];
    NSData *dataPic =  UIImageJPEGRepresentation(thumbnail, 0.1);
    NSString *base64String = [dataPic base64EncodedStringWithOptions:0];
    [attachment setStringValue:base64String];
    [message addChild:attachment];
    
    NSXMLElement *audiodata = [NSXMLElement elementWithName:@"audiodata"];
    [audiodata addAttributeWithName:@"xmlns" stringValue:@"http://jabber.org/protocol/chatstates"];
    [audiodata addAttributeWithName:@"msgId" stringValue:msgID];
    [message addChild:audiodata];
    
    
    
    [stream sendElement:message];
}


#pragma mark AudioSend

-(void)sendaudioWithUrl:(NSString *)msg andjid:(NSString *)jid  andwithstream:(XMPPStream *)stream withaudioUrl:(NSString *)audio  andtime:(NSString *)time  audioLocalPath:(NSString *)localPath andmsgID:(NSString *)msgID
{
    
    NSXMLElement *message = [NSXMLElement elementWithName:@"message"];
    [message addAttributeWithName:@"id" stringValue:msgID];
    [message addAttributeWithName:@"type" stringValue:@"chat"];
    [message addAttributeWithName:@"msgtype" stringValue:@"playaudio"];
    [message addAttributeWithName:@"to" stringValue:jid];
    [message addAttributeWithName:@"audioUrl" stringValue:audio];
    [message addAttributeWithName:@"time" stringValue:time];
    [message addAttributeWithName:@"localurl" stringValue:localPath];
    
    
    NSXMLElement *audiodata = [NSXMLElement elementWithName:@"audiodata"];
    [audiodata addAttributeWithName:@"xmlns" stringValue:@"http://jabber.org/protocol/chatstates"];
    [audiodata addAttributeWithName:@"msgId" stringValue:msgID];
    [message addChild:audiodata];
    
    
    NSXMLElement *body = [NSXMLElement elementWithName:@"body"];
    [body setStringValue:msg];
    [message addChild:body];
    
    [stream sendElement:message];
}








-(void)recievemesages:(NSString *)userID andJid:(XMPPStream *)stream
{
    NSXMLElement *iq1 = [NSXMLElement elementWithName:@"iq"];
    [iq1 addAttributeWithName:@"type" stringValue:@"get"];
    [iq1 addAttributeWithName:@"id" stringValue:@"pk1"];
    
    NSXMLElement *retrieve = [NSXMLElement elementWithName:@"retrieve" xmlns:@"urn:xmpp:archive"];
    
    [retrieve addAttributeWithName:@"with" stringValue:userID];
    NSXMLElement *set = [NSXMLElement elementWithName:@"set" xmlns:@"http://jabber.org/protocol/rsm"];
    NSXMLElement *max = [NSXMLElement elementWithName:@"max" stringValue:@"100"];
    
    [iq1 addChild:retrieve];
    [retrieve addChild:set];
    [set addChild:max];
    [stream sendElement:iq1];
    
}




-(void)setnewMessage:(XMPPStream *)stream
{
    //[DDLog addLogger:[DDTTYLogger sharedInstance] withLogLevel:XMPP_LOG_FLAG_SEND_RECV];
    XMPPMessageArchivingCoreDataStorage *xmppMessageArchivingStorage = [XMPPMessageArchivingCoreDataStorage sharedInstance];
    XMPPMessageArchiving *xmppMessageArchivingModule = [[XMPPMessageArchiving alloc] initWithMessageArchivingStorage:xmppMessageArchivingStorage];
    [xmppMessageArchivingModule setClientSideMessageArchivingOnly:YES];
    [xmppMessageArchivingModule activate:stream];
    [xmppMessageArchivingModule addDelegate:self delegateQueue:dispatch_get_main_queue()];
}



-(NSArray *)loadarchivemsg :(NSString *)userID andJid:(XMPPStream *)stream withChatBool:(BOOL)isYes;
{
    XMPPMessageArchivingCoreDataStorage *_xmppMsgStorage = [XMPPMessageArchivingCoreDataStorage sharedInstance];
    NSManagedObjectContext *moc = [_xmppMsgStorage mainThreadManagedObjectContext];
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"XMPPMessageArchiving_Message_CoreDataObject"
                                                         inManagedObjectContext:moc];
  
    
    
    NSFetchRequest *readrequest = [[NSFetchRequest alloc]init];
    [readrequest setEntity:entityDescription];
    NSError *error;
    NSString *status = @"recived";
    NSSortDescriptor *sortescriptor = [NSSortDescriptor sortDescriptorWithKey:@"timestamp" ascending:YES];
    readrequest.sortDescriptors = @[sortescriptor];
    NSPredicate *  readpredicate = [NSPredicate predicateWithFormat:@"status == %@",status];
    readrequest.predicate =readpredicate;
    NSArray *readArr  = [moc executeFetchRequest:readrequest error:&error];
    if (readArr.count>0)
    {
        for (int i =0; i<readArr.count; i++)
        {
            XMPPMessageArchiving_Message_CoreDataObject *archivedMessage =[readArr objectAtIndex:i];
            
            if (isYes==YES) {
                
                archivedMessage.status =@"seen";
                NSError *saveError = nil;
                [moc save:&saveError];
            }
            
            
            
            
            
        }
        
        NSXMLElement *message = [NSXMLElement elementWithName:@"message"];
        [message addAttributeWithName:@"type" stringValue:@"chat"];
        [message addAttributeWithName:@"to" stringValue: userID];
        NSXMLElement *composing = [NSXMLElement elementWithName:@"read"];
        [composing addAttributeWithName:@"xmlns" stringValue:@"http://jabber.org/protocol/chatstates"];
        [message addChild:composing];
        [stream sendElement:message];
    }
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    [request setEntity:entityDescription];
    //   [request setFetchLimit:20];
    
    //NSError *error;
    NSString *predicateFrmt = @"bareJidStr == %@";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt,userID];
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"timestamp" ascending:YES];
    request.sortDescriptors = @[sortDescriptor];
    request.predicate = predicate;
    NSArray *chatArr  = [moc executeFetchRequest:request error:&error];
    return  chatArr;
}


-(void)changeImageTimer:(NSString *)msgID TimeValue:(NSString *)time;
{
    XMPPMessageArchivingCoreDataStorage *_xmppMsgStorage = [XMPPMessageArchivingCoreDataStorage sharedInstance];
    NSManagedObjectContext *moc = [_xmppMsgStorage mainThreadManagedObjectContext];
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"XMPPMessageArchiving_Message_CoreDataObject"
                                                         inManagedObjectContext:moc];
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    [request setEntity:entityDescription];
    //   [request setFetchLimit:20];
    
    NSError *error;
    NSString *predicateFrmt = @"msgId == %@";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt,msgID];
    request.predicate = predicate;
    NSArray *chatArr  = [moc executeFetchRequest:request error:&error];
    if (chatArr.count>0)
    {
        XMPPMessageArchiving_Message_CoreDataObject *archivedMessage =[chatArr objectAtIndex:0];
        XMPPMessage * message = archivedMessage.message;
        [message removeAttributeForName:@"time"];
        
        if([time isEqualToString:@"0"])
            [message addAttributeWithName:@"time" stringValue:@"notShow"];
        else
            [message addAttributeWithName:@"time" stringValue:time];
        
        archivedMessage.message =message;
        [moc save:&error];
    }
    
}
-(void)SendFriendRequest :(NSString *)userID withNickname:(NSString *)optionalName
{
    XMPPJID *newBuddy = [XMPPJID jidWithString:userID];
    [xmppRoster addUser:newBuddy withNickname:optionalName];
}





#pragma mark Core Data
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (NSManagedObjectContext *)managedObjectContext_roster
{
    return [xmppRosterStorage mainThreadManagedObjectContext];
}


#pragma mark NSFetchedResultsController
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (NSFetchedResultsController *)fetchedResultsController
{
    fetchedResultsController = nil;
    if (fetchedResultsController == nil)
    {
        NSManagedObjectContext *moc = [self managedObjectContext_roster];
        
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"XMPPUserCoreDataStorageObject"
                                                  inManagedObjectContext:moc];
        
        NSSortDescriptor *sd1 = [[NSSortDescriptor alloc] initWithKey:@"sectionNum" ascending:YES];
        NSSortDescriptor *sd2 = [[NSSortDescriptor alloc] initWithKey:@"displayName" ascending:YES];
        NSSortDescriptor *sd3 = [[NSSortDescriptor alloc] initWithKey:@"jidStr" ascending:YES];
      
        
   //     NSArray *sortDescriptors = @[sd1, sd2];
          NSArray *sortDescriptors = @[sd2, sd3];
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        [fetchRequest setEntity:entity];
        [fetchRequest setSortDescriptors:sortDescriptors];
        [fetchRequest setFetchBatchSize:10];
        
        
      
        
        fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                       managedObjectContext:moc
                                                                         sectionNameKeyPath:@"sectionNum"
                                                                                  cacheName:nil];
        [fetchedResultsController setDelegate:self];
        NSError *error = nil;
        if (![fetchedResultsController performFetch:&error])
        {
            
        }
        
    }
    
    
   

    return fetchedResultsController;
}



- (void)FetchFriends
{
    
    NSXMLElement *query = [NSXMLElement elementWithName:@"query" xmlns:@"jabber:iq:roster"];
    XMPPIQ *iq = [XMPPIQ iq];
    [iq addAttributeWithName:@"id" stringValue:@"ec2-52-34-41-17.us-west-2.compute.amazonaws.com"];
    [iq addAttributeWithName:@"to" stringValue:@"test11"];
    [iq addAttributeWithName:@"type" stringValue:@"get"];
    [iq addChild:query];
    [xmppStream sendElement:iq];
    
}


-(NSMutableArray *)returnMessageThreads
{

    NSMutableArray * messagesArr =[[NSMutableArray alloc]init];
    XMPPMessageArchivingCoreDataStorage *_xmppMsgStorage = [XMPPMessageArchivingCoreDataStorage sharedInstance];
    NSManagedObjectContext *moc = [_xmppMsgStorage mainThreadManagedObjectContext];
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"XMPPMessageArchiving_Contact_CoreDataObject"
                                                         inManagedObjectContext:moc];
    NSFetchRequest *readrequest = [[NSFetchRequest alloc]init];
    
    [readrequest setEntity:entityDescription];
    NSError *error;
    NSArray *readArr  = [moc executeFetchRequest:readrequest error:&error];
    if (readArr.count>0)
    {
        for (int i =0; i<readArr.count; i++)
        {
            XMPPMessageArchiving_Contact_CoreDataObject *archivedMessage =[readArr objectAtIndex:i];
            NSString * lastMessage= archivedMessage.mostRecentMessageBody;
            NSLog(@"%@", archivedMessage.bareJidStr);
            
            NSArray *messagesArray = [self loadarchivemsg:archivedMessage.bareJidStr andJid:self.senderStream withChatBool:NO];
            
            if (messagesArray.count>0) {
                
                if ([messagesArray lastObject]!=nil) {
                    
                XMPPMessageArchiving_Message_CoreDataObject *message = [messagesArray lastObject];
                    NSLog(@"what's here %@",message.status);
              
                    archivedMessage.status = message.status;
                    
                    
                }
                

                
            }
           
            
            // changing
            
            if (![lastMessage isEqualToString:@""])
            {
                [messagesArr addObject:archivedMessage];
            }
        }
    }
  
    return messagesArr;


}

-(void)deleteThread:(NSString *)jid
{
    dispatch_async(dispatch_get_main_queue(), ^{
    XMPPMessageArchivingCoreDataStorage *_xmppMsgStorage = [XMPPMessageArchivingCoreDataStorage sharedInstance];
    NSManagedObjectContext *moc = [_xmppMsgStorage mainThreadManagedObjectContext];
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"XMPPMessageArchiving_Contact_CoreDataObject"
                                                         inManagedObjectContext:moc];
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    [request setEntity:entityDescription];
   
    
    NSError *error;
    NSString *predicateFrmt = @"bareJidStr == %@";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt,jid];
    request.predicate = predicate;
    NSArray *chatArr  = [moc executeFetchRequest:request error:&error];
    if (chatArr.count>0)
    {
         XMPPMessageArchiving_Contact_CoreDataObject *_xmppMsgStorage =[chatArr objectAtIndex:0];
         [moc deleteObject:_xmppMsgStorage];
         [moc save:&error];
    }
    
    [self deleteConversation:jid];
    });
}

-(void)deleteConversation:(NSString *)jid
{

    dispatch_async(dispatch_get_main_queue(), ^{
    XMPPMessageArchivingCoreDataStorage *_xmppMsgStorage = [XMPPMessageArchivingCoreDataStorage sharedInstance];
    NSManagedObjectContext *moc = [_xmppMsgStorage mainThreadManagedObjectContext];
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"XMPPMessageArchiving_Message_CoreDataObject"
                                                         inManagedObjectContext:moc];
    NSFetchRequest *readrequest = [[NSFetchRequest alloc]init];
    [readrequest setEntity:entityDescription];
    NSError *error;
    
    NSString *predicateFrmt = @"bareJidStr == %@";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt,jid];
    readrequest.predicate = predicate;

    NSSortDescriptor *sortescriptor = [NSSortDescriptor sortDescriptorWithKey:@"timestamp" ascending:YES];
    readrequest.sortDescriptors = @[sortescriptor];
    NSArray *readArr  = [moc executeFetchRequest:readrequest error:&error];
    for (int i=0; i<readArr.count; i++)
    {
        XMPPMessageArchiving_Message_CoreDataObject *_xmppMsgStorage =[readArr objectAtIndex:i];
        [moc deleteObject:_xmppMsgStorage];
        [moc save:&error];
    }
    });
}


-(NSArray *)getSingleUser:(NSString*)jid
{
    XMPPMessageArchivingCoreDataStorage *_xmppMsgStorage = [XMPPMessageArchivingCoreDataStorage sharedInstance];
    NSManagedObjectContext *moc = [_xmppMsgStorage mainThreadManagedObjectContext];
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"XMPPMessageArchiving_Contact_CoreDataObject"
                                                         inManagedObjectContext:moc];
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    [request setEntity:entityDescription];
    NSError *error;
    NSString *predicateFrmt = @"bareJidStr == %@";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt,jid];
    request.predicate = predicate;
    NSArray *contact_Arr  = [moc executeFetchRequest:request error:&error];
    return  contact_Arr;
}




#pragma mark LocalDataBaseManage
#pragma mark Conacts Manage
-(void)clearRoster
{
    XMPPMessageArchivingCoreDataStorage *_xmppMsgStorage = [XMPPMessageArchivingCoreDataStorage sharedInstance];
    NSManagedObjectContext *moc = [_xmppMsgStorage mainThreadManagedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"XmppOfflineFriends" inManagedObjectContext:moc];
    [fetchRequest setEntity:entity];
    NSError *error;
    NSArray *fetchedObjects = [moc executeFetchRequest:fetchRequest error:&error];
    if (fetchedObjects.count>0)
    {
        for (int i =0; i<fetchedObjects.count; i++)
        {
            [moc deleteObject:[fetchedObjects objectAtIndex:i]];
            [moc save:&error];
            
        }
    }
    
}



-(void)addRosterUsers:(NSArray*)arr
{
    [self clearRoster];
    XMPPMessageArchivingCoreDataStorage *_xmppMsgStorage = [XMPPMessageArchivingCoreDataStorage sharedInstance];

    for (int i =0; i<arr.count; i++)
    {
        NSManagedObjectContext *moc = [_xmppMsgStorage mainThreadManagedObjectContext];
        NSManagedObject *offflineUser = [NSEntityDescription
                                         insertNewObjectForEntityForName:@"XmppOfflineFriends"
                                         inManagedObjectContext:moc];
        NSDictionary *dic = [arr objectAtIndex:i];
        [offflineUser setValue:[dic valueForKey:@"fullname"] forKey:@"bareJidStr"];
        [offflineUser setValue:@"0" forKey:@"subscriptionType"];
        [offflineUser setValue:[dic valueForKey:@"fullname"] forKey:@"fullname"];
        [offflineUser setValue:[dic valueForKey:@"message"] forKey:@"message"];
        [offflineUser setValue:[dic valueForKey:@"pin"] forKey:@"pin"];
        [offflineUser setValue:[dic valueForKey:@"status"] forKey:@"status"];
        
        NSURL *url = [NSURL URLWithString:[dic valueForKey:@"user_thumbnail"]];
        NSData *data = [NSData dataWithContentsOfURL:url];
        
        [offflineUser setValue:data forKey:@"image"];

        [moc insertObject:offflineUser];
        NSError *error;
        [moc save:&error];

    }
}



-(NSArray *)getRosterUser
{
    
    XMPPMessageArchivingCoreDataStorage *_xmppMsgStorage = [XMPPMessageArchivingCoreDataStorage sharedInstance];
    NSManagedObjectContext *moc = [_xmppMsgStorage mainThreadManagedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"XmppOfflineFriends" inManagedObjectContext:moc];
    [fetchRequest setEntity:entity];
    NSError *error;
    NSArray *fetchedObjects = [moc executeFetchRequest:fetchRequest error:&error];
    return fetchedObjects;

}


#pragma mark UserProfile Manage
-(void)addLoginUserProfile :(NSDictionary *)userProfile
{

    XMPPMessageArchivingCoreDataStorage *_xmppMsgStorage = [XMPPMessageArchivingCoreDataStorage sharedInstance];
    NSManagedObjectContext *moc = [_xmppMsgStorage mainThreadManagedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"LoginUser" inManagedObjectContext:moc];
    [fetchRequest setEntity:entity];
    NSError *error;
    NSArray *fetchedObjects = [moc executeFetchRequest:fetchRequest error:&error];
    if (fetchedObjects.count>0 && fetchedObjects.count<2)
    {
//        LoginUser *loginUser = [fetchedObjects objectAtIndex:0];
//        loginUser.bareJidStr = [userProfile valueForKey:@"fullname"];
//        loginUser.fullname = [userProfile valueForKey:@"fullname"];
//        loginUser.message = [userProfile valueForKey:@"message"];
//        loginUser.pin = [userProfile valueForKey:@"pin"];
//        loginUser.status = [userProfile valueForKey:@"status"];
//        loginUser.userid = [userProfile valueForKey:@"userid"];
//        
//        NSURL *url = [NSURL URLWithString:[userProfile valueForKey:@"user_thumbnail"]];
//        NSData *data = [NSData dataWithContentsOfURL:url];
//        loginUser.image = data;
//        
//         NSError *error;
//        [moc save:&error];
    }
    else
    {
        
        NSManagedObject *loginUser = [NSEntityDescription
                                         insertNewObjectForEntityForName:@"LoginUser"
                                         inManagedObjectContext:moc];
        
        [loginUser setValue:[userProfile valueForKey:@"fullname"] forKey:@"bareJidStr"];
        [loginUser setValue:[userProfile valueForKey:@"fullname"] forKey:@"fullname"];
        [loginUser setValue:[userProfile valueForKey:@"message"] forKey:@"message"];
        [loginUser setValue:[userProfile valueForKey:@"pin"] forKey:@"pin"];
        [loginUser setValue:[userProfile valueForKey:@"status"] forKey:@"status"];
        [loginUser setValue:[userProfile valueForKey:@"userid"] forKey:@"userid"];
        
        NSURL *url = [NSURL URLWithString:[userProfile valueForKey:@"user_thumbnail"]];
        NSData *data = [NSData dataWithContentsOfURL:url];
        [loginUser setValue:data forKey:@"image"];
        
        [moc insertObject:loginUser];
        NSError *error;
        [moc save:&error];
        
        
    }
}

-(NSArray *)getLoginUserProfile
{
    XMPPMessageArchivingCoreDataStorage *_xmppMsgStorage = [XMPPMessageArchivingCoreDataStorage sharedInstance];
    NSManagedObjectContext *moc = [_xmppMsgStorage mainThreadManagedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"LoginUser" inManagedObjectContext:moc];
    [fetchRequest setEntity:entity];
    NSError *error;
    NSArray *fetchedObjects = [moc executeFetchRequest:fetchRequest error:&error];
    if (fetchedObjects.count>0)
    {
        return fetchedObjects;
    }
    else
    {
        NSArray *login = [NSArray arrayWithObjects:@"asd",@"as", nil];
        return login;
    }
    
}

-(NSArray *)getUserProfile:(NSString*)jid;
{
    XMPPMessageArchivingCoreDataStorage *_xmppMsgStorage = [XMPPMessageArchivingCoreDataStorage sharedInstance];
    NSManagedObjectContext *moc = [_xmppMsgStorage mainThreadManagedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"XmppOfflineFriends" inManagedObjectContext:moc];
    [fetchRequest setEntity:entity];
    NSError *error;
    NSString *predicateFrmt = @"fullname == %@";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt,jid];
    fetchRequest.predicate = predicate;

    NSArray *fetchedObjects = [moc executeFetchRequest:fetchRequest error:&error];
    if (fetchedObjects.count>0)
    {
        return fetchedObjects;
    }
    else
    {
        NSArray *login = [NSArray arrayWithObjects:@"asd",@"as", nil];
        return login;
    }
    
}




-(void)addSendingImagewithUrl :(NSString *)msgId andjid:(NSString *)jid  withImage:(NSString *)imagePic andtime:(NSString *)time andType:(NSString*)type withToUser:(NSString*)user withFromUser:(NSString*)fromUser
{
   
    NSXMLElement *message = [NSXMLElement elementWithName:@"message"];
    [message addAttributeWithName:@"to" stringValue:jid];
    [message addAttributeWithName:@"id" stringValue:msgId];
    
    [message addAttributeWithName:@"sendToUser" stringValue:user];
    [message addAttributeWithName:@"sendFromUser" stringValue:fromUser];
    
    NSXMLElement *body = [NSXMLElement elementWithName:@"body"];
    if ([type isEqualToString:@"video"])
    {
            [message addAttributeWithName:@"type" stringValue:@"chat"];
            [message addAttributeWithName:@"msgtype" stringValue:@"video"];
    }
    else
    {
            [message addAttributeWithName:@"type" stringValue:@"chat"];
            [message addAttributeWithName:@"msgtype" stringValue:@"image"];
    }
    [body setStringValue:@"testing image"];
    if (![time isEqualToString:@""])
    {
        [message addAttributeWithName:@"time" stringValue:time];
    }
    else
    {
        [message addAttributeWithName:@"time" stringValue:@""];
    }
    
    
//    if([type isEqualToString:@"image"])
//    {
        NSXMLElement *attachment = [NSXMLElement elementWithName:@"attachment"];
        NSXMLElement *photo = [NSXMLElement elementWithName:@"PHOTO"];
        NSXMLElement *binval = [NSXMLElement elementWithName:@"BINVAL"];
        [photo addChild:binval];
        [attachment setStringValue:imagePic];
        [message addChild:attachment];
        
//    }
    [message addChild:body];
    
    
    XMPPMessageArchivingCoreDataStorage *_xmppMsgStorage = [XMPPMessageArchivingCoreDataStorage sharedInstance];
    NSManagedObjectContext *moc = [_xmppMsgStorage mainThreadManagedObjectContext];
        XMPPMessageArchiving_Message_CoreDataObject *archivedMessage = [NSEntityDescription
                            insertNewObjectForEntityForName:@"XMPPMessageArchiving_Message_CoreDataObject"
                                         inManagedObjectContext:moc];
   
        archivedMessage.status = @"Sending";
   
    XMPPMessage * msg = [XMPPMessage messageFromElement:message];
    archivedMessage.message = msg;
    archivedMessage.body = msg.body;
    archivedMessage.bareJid = [XMPPJID jidWithString:jid];
    archivedMessage.streamBareJidStr = jid;
    archivedMessage.msgId = [[message attributeForName:@"id"] stringValue];
    
    NSDate *timestamp = [NSDate date];
    if (timestamp)
        archivedMessage.timestamp = timestamp;
    else
        archivedMessage.timestamp = [[NSDate alloc] init];
    
    archivedMessage.thread = @"";//[[message elementForName:@"thread"] stringValue];
    archivedMessage.isOutgoing = YES;//isOutgoing;
    archivedMessage.isComposing = @"";//isComposing;
    
    [moc insertObject:archivedMessage];
    NSError *error;
    [moc save:&error];

}

-(void)deleteSendingMessage:(NSString *)msgId
{
    XMPPMessageArchivingCoreDataStorage *_xmppMsgStorage = [XMPPMessageArchivingCoreDataStorage sharedInstance];
    NSManagedObjectContext *moc = [_xmppMsgStorage mainThreadManagedObjectContext];
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"XMPPMessageArchiving_Message_CoreDataObject"
                                                         inManagedObjectContext:moc];
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    [request setEntity:entityDescription];
    //   [request setFetchLimit:20];
    
    NSError *error;
    NSString *predicateFrmt = @"msgId == %@";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt,msgId];
    request.predicate = predicate;
    NSArray *chatArr  = [moc executeFetchRequest:request error:&error];
    if (chatArr.count>0)
    {
        [moc deleteObject:[chatArr objectAtIndex:0]];
        [moc save:&error];
        
    }


}

-(void)accceptFriendRequest:(NSString *)fromJid
{
    
    XMPPJID *newBuddy = [XMPPJID jidWithString:fromJid];
   [xmppRoster acceptPresenceSubscriptionRequestFrom:newBuddy andAddToRoster:YES];
}

- (NSInteger)sectioncount :(NSFetchedResultsController *)fetchedResultsControlle
{
    return 1;
}

-(NSInteger)numberofRows :(NSFetchedResultsController *)fetchedResultsControlle  andnumberOfRowsInSection:(NSInteger)sectionIndex
{
    NSArray *sections =  [fetchedResultsControlle sections];
    
    
    NSInteger sum = 0;
    
    for (int i=0; i<sections.count; i++){

        id <NSFetchedResultsSectionInfo> sectionInfo = sections[i];
        sum = sum + sectionInfo.numberOfObjects;
        
    }

    return  sum;
   


}

-(void)sendMessage :(NSString *)msg and :(NSString *)jid  andwithstream:(XMPPStream *)stream
{
    NSString *messageID=[stream generateUUID];
    NSXMLElement *body = [NSXMLElement elementWithName:@"body"];
    [body setStringValue:msg];
    NSXMLElement *message = [NSXMLElement elementWithName:@"message"];
    [message addAttributeWithName:@"id" stringValue:messageID];
    [message addAttributeWithName:@"type" stringValue:@"chat"];
    [message addAttributeWithName:@"to" stringValue:jid];
    [message addChild:body];
//    XMPPMessage * newMessage = [XMPPMessage messageFromElement:message];
  //  XMPPMessageArchivingCoreDataStorage *_xmppMsgStorage = [XMPPMessageArchivingCoreDataStorage sharedInstance];
    //[_xmppMsgStorage addNewUser:newMessage outgoing:YES xmppStream:stream ];
    [stream sendElement:message];
}




-(void)sendContact :(NSString *)name andEmail:(NSString *)email andPhone:(NSString *)phone  and:(NSString *)jid andmsgID:(NSString*)msgID andwithstream:(XMPPStream *)stream
{
    NSXMLElement *body = [NSXMLElement elementWithName:@"body"];
    [body setStringValue:@"contact"];
    NSXMLElement *message = [NSXMLElement elementWithName:@"message"];
    
    [message addAttributeWithName:@"name" stringValue:name];
    [message addAttributeWithName:@"email" stringValue:email];
    [message addAttributeWithName:@"phone" stringValue:phone];
    
    [message addAttributeWithName:@"id" stringValue:msgID];
    [message addAttributeWithName:@"type" stringValue:@"chat"];
    [message addAttributeWithName:@"to" stringValue:jid];
    
    [message addChild:body];
    [stream sendElement:message];
}


-(XMPPUserCoreDataStorageObject *)nameforrow :(NSFetchedResultsController *)fetchedResultsControlle andnumberOfRowsInSectio:(NSIndexPath *)indexPath
{
    XMPPUserCoreDataStorageObject *user = [fetchedResultsControlle objectAtIndexPath:indexPath];
    return user;
}


-(void)ReciveMessage:(XMPPStream *)sender didReceiveMessage:(XMPPMessage *)message
{
    XMPPUserCoreDataStorageObject *user = [xmppRosterStorage userForJID:[message from]
                                                             xmppStream:sender
                                                   managedObjectContext:[self managedObjectContext_roster]];


}

-(NSString *)returnMessageId :(XMPPMessage *)message
{
    
    NSArray * TEMParr = [message elementsForName:@"received"];
    DDXMLElement * recv_message=[TEMParr objectAtIndex:0];
    NSString * elementId=[recv_message  attributeForName:@"id"].stringValue;
    return elementId;

}



-(void)addMessageStatus:(NSString *)jid andwith :(NSString *)msgid
{
    XMPPMessageArchivingCoreDataStorage *_xmppMsgStorage = [XMPPMessageArchivingCoreDataStorage sharedInstance];
   
    XMPPMessageArchiving_Message_CoreDataObject * message = [_xmppMsgStorage contactWithBareJidStr:@"shella@ec2-52-34-41-17.us-west-2.compute.amazonaws.com" streamBareJidStr:@"max@ec2-52-34-41-17.us-west-2.compute.amazonaws.com" managedObjectContext:[_xmppMsgStorage mainThreadManagedObjectContext]];
    
    NSLog(@"%@",message);
    
    

}

-(void)sendComosing:(NSString *)jid  andwithstream:(XMPPStream *)stream
{
    NSXMLElement *message = [NSXMLElement elementWithName:@"message"];
    [message addAttributeWithName:@"type" stringValue:@"chat"];
    [message addAttributeWithName:@"to" stringValue: jid];
    
    NSXMLElement *composing = [NSXMLElement elementWithName:@"composing"];
    [composing addAttributeWithName:@"xmlns" stringValue:@"http://jabber.org/protocol/chatstates"];
    
    [message addChild:composing];
    
    [stream sendElement:message];
}

-(void)retreveMessage:(NSString *)jid  andwithstream:(XMPPStream *)stream andmsgID:(NSString *)msgId
{
    NSXMLElement *message = [NSXMLElement elementWithName:@"message"];
    [message addAttributeWithName:@"type" stringValue:@"chat"];
    [message addAttributeWithName:@"to" stringValue: jid];
    
    NSXMLElement *composing = [NSXMLElement elementWithName:@"reverse"];
    [composing addAttributeWithName:@"xmlns" stringValue:@"http://jabber.org/protocol/chatstates"];
     [composing addAttributeWithName:@"msgId" stringValue:msgId];
    
    [message addChild:composing];
    
    [stream sendElement:message];
}

#pragma mark - Get User Status
-(void)getUserPresense:(NSString *)jid andStream:(XMPPStream *)stream
{
    NSXMLElement *presence = [NSXMLElement elementWithName:@"presence"];
    [presence addAttributeWithName:@"to" stringValue:jid];
    [presence addAttributeWithName:@"type" stringValue:@"subscribe"];
    [xmppStream sendElement:presence];
}


@end
