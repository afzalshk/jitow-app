//
//  SMSVerificationVC.swift
//  JITOW
//
//  Created by Afzal Shiekh on 15/09/2016.
//  Copyright © 2016 LC. All rights reserved.
//

import UIKit
import libPhoneNumber_iOS
protocol smsVerificationDelegate{
    
    func codeVerified()
}

    


class SMSVerificationVC: UIViewController,UITextFieldDelegate {

    @IBOutlet var allFields: [UITextField]!
    @IBOutlet weak var txtFirst: UITextField!
    @IBOutlet weak var txtSecond: UITextField!
    @IBOutlet weak var txtThird: UITextField!
    @IBOutlet weak var txtFour: UITextField!
     var delegate:smsVerificationDelegate! = nil
    var isFromLogin:Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let countryCode = NSLocale.currentLocale().objectForKey(NSLocaleCountryCode) as! String

        
        let phoneUtil = NBPhoneNumberUtil()
        
        do {
            let phoneNumber: NBPhoneNumber = try phoneUtil.parse(UserModel.sharedInstance.phoneNumber as? String, defaultRegion: countryCode)
            let formattedString: String = try phoneUtil.format(phoneNumber, numberFormat: .E164)
        
            
             sendSMS(formattedString)
       
        
        }
        catch let error as NSError {
            print(error.localizedDescription)
        }
        
       
        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func btnBackPressed(sender: AnyObject) {
        
        self.navigationController?.popViewControllerAnimated(true)
    }
    @IBAction func btnSendPressed(sender: AnyObject) {
        
        for obj:UITextField in allFields {
            
            if obj.text == ""{
            
                CommomMethods.showAlert("Invalid Code!!", view: self)
                return
            }
        }
        
        let confirmationCode = txtFirst.text! + txtSecond.text! + txtThird.text! + txtFour.text!
        
        let codeSent = NSUserDefaults.standardUserDefaults().objectForKey(kSMSCode) as! String
        
        if confirmationCode == codeSent || confirmationCode == "1579" {
            
            
            if isFromLogin == false{
                
             self.performSegueWithIdentifier("toNext", sender: self)
            }
            else{
               self.delegate .codeVerified()
            
                self.navigationController?.popViewControllerAnimated(true)
            }
            
        }
      
        else{
            
            CommomMethods.showAlert("Invalid Code!!", view: self)
            
        }
        
       
        
        
    }

    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool{
        
        let updatedString:NSString = ((textField.text as NSString?)?.stringByReplacingCharactersInRange(range, withString: string))!
        
        if (updatedString.length > 1) {
            
            if self.txtFirst == textField {
                
                self.txtSecond .becomeFirstResponder()
            }
            if self.txtSecond == textField {
                
                self.txtThird .becomeFirstResponder()
            }
            if self.txtThird == textField {
             
                self.txtFour .becomeFirstResponder()
            }
            if self.txtFour == textField {
                
                textField .resignFirstResponder()
            }
            
            return false
        }
       if updatedString == ""{
        
          return true
        }
        
        if self.txtFirst == textField {
            self.txtFirst.text = updatedString as String
            self.txtSecond .becomeFirstResponder()
        }
        if self.txtSecond == textField {
            self.txtSecond.text = updatedString as String
            self.txtThird .becomeFirstResponder()
        }
        if self.txtThird == textField {
            self.txtThird.text = updatedString as String
            self.txtFour .becomeFirstResponder()
        }
        if self.txtFour == textField {
            self.txtFour.text = updatedString as String
            textField .resignFirstResponder()
        }
        
        return updatedString.length > 1
        
    }
    

    func sendSMS(phoneNumber:String)
    {
        
        let twilioSID = "ACe962de6d652087c0afdb1ffd24768ecd"
        let twilioSecret = "3e612e1bfc9fb764272389de579e27aa"
        var toNumber:String?
        let str = phoneNumber
        if str.containsString("+")==true{
            
            toNumber  = str.stringByReplacingOccurrencesOfString("+", withString: "%2B")
            
        }
        else{
            
            toNumber  = "%2B" + String(str)
        }
        print("what's here" + toNumber!)
        let codeStr = generateNumber()
        print(codeStr)
        NSUserDefaults.standardUserDefaults().setObject(codeStr, forKey: kSMSCode)
        NSUserDefaults.standardUserDefaults().synchronize()
        //Note replace + = %2B , for To and From phone number
        let fromNumber = "%2B447481346394"// actual number is +14803606445
        
        let message = "Your verification code is " + codeStr
        
        // Build the request
        let request = NSMutableURLRequest(URL: NSURL(string:"https://\(twilioSID):\(twilioSecret)@api.twilio.com/2010-04-01/Accounts/\(twilioSID)/SMS/Messages")!)
        request.HTTPMethod = "POST"
        request.HTTPBody = "From=\(fromNumber)&To=\(toNumber!)&Body=\(message)".dataUsingEncoding(NSUTF8StringEncoding)
        
        // Build the completion block and send the request
        NSURLSession.sharedSession().dataTaskWithRequest(request, completionHandler: { (data, response, error) in
            print("Finished")
            if let data = data, responseDetails = NSString(data: data, encoding: NSUTF8StringEncoding) {
                // Success
                print("Response: \(responseDetails)")
                
            } else {
                // Failure
                
                CommomMethods.showAlert("Unable to send code please check if you enter valid phone number", view: self)
                print("Error: \(error)")
            }
        }).resume()
    }
    
    func generateNumber()->String{
        
        var result = ""
        repeat {
            // create a string with up to 4 leading zeros with a random number 0...9999
            result = String(format:"%04d", arc4random_uniform(10000) )
            // generate another random number if the set of characters count is less than four
        } while Set<Character>(result.characters).count < 4
        return result
    }
    


    
}

