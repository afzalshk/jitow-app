//
//  EmojiListVC.swift
//  JITOW
//
//  Created by Mac103 on 5/4/16.
//  Copyright © 2016 LC. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import MBProgressHUD
protocol selectEmojiDelegate{
    
    
    func selectedEmoji(sender:Int)
    
    
}


class EmojiListVC: UIViewController {
    
    var videoEmoji:VideoModel?
    var delegate:selectEmojiDelegate! = nil
    
    @IBOutlet weak var myCollView: UICollectionView!
    var emojiList:NSMutableArray = NSMutableArray()
    
    override func viewDidLoad() {
        
        getAllEmojisList()
        self.navigationController?.navigationBarHidden = false
        
        customBackgroundButton()
        
        
        
        
    }
    
    func getAllEmojisList(){
        
        
                let emojiRanges = [
                    0x1F600...0x1F640,
                    0x1F645...0x1F649,
                    0x1F680...0x1F699
                ]
        
                for range in emojiRanges {
                    for i in range {
                        //let c = String(UnicodeScalar(i))
                        emojiList.addObject(i)
                        
                    }
                }
        
        
        
    }
    
    
    //CollectionView DataSource
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        
        
        return emojiList.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell{
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Emoji",
                                                                         forIndexPath: indexPath) as! EmojiCollectionViewCell
        
        
        let i  = emojiList.objectAtIndex(indexPath.item) as! Int
        
        
        
        let c = String(UnicodeScalar(i))
        
        cell.lblEmoji.text = c
        
        
        return cell
        
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        
        MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        let i  = emojiList.objectAtIndex(indexPath.item) as! Int
        
        
        
        let str = kBaseServerUrl + "a_emoji"
        let fullUrl  = NSURL(string: str)
        
        let newPost = ["video_id":(videoEmoji?.video_id as? String)!,"user_id":NSUserDefaults.standardUserDefaults().objectForKey(kUserid)!, "emoji":String(i)] as [String:AnyObject]
        
        
        let request = Alamofire.request(.POST, fullUrl!, parameters: newPost, encoding: .URL)
        request.responseJSON
            { response in
                
                if let anError = response.result.error
                {
                    
                    print(anError)
                    
                    dispatch_async(dispatch_get_main_queue(),
                        {
                            
                            CommomMethods.showAlert("something went wrong please check your internet connection and try again later", view: self)
                            
                            
                            MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
                            
                            
                        }
                    )
                    
                    
                    
                }
                else if let data = response.data as NSData!
                {
                    
                    
                    let json = JSON(data:data)
                    
                    let status = json["status"].stringValue
                    
                    if (status == "ok")
                    {
                        self.navigationController?.popViewControllerAnimated(true)
                        self.delegate?.selectedEmoji(i)
                        
                    }
                    else{
                        
                        dispatch_async(dispatch_get_main_queue(),
                            {
                                
                                CommomMethods.showAlert("you can react once on a scoop ", view: self)
                                
                                MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
                                
                                
                            }
                        )
                        

                    }
                    
                    
                }
                
                
        }
        
        
        

        
        
    }
    
    func customBackgroundButton(){
        
        
        self.navigationItem.hidesBackButton = true
        let myBackButton:UIButton = UIButton(type: UIButtonType.Custom)
        myBackButton.addTarget(self, action: "popToRoot:", forControlEvents: UIControlEvents.TouchUpInside)
        myBackButton.setTitle("", forState: UIControlState.Normal)
        myBackButton.setImage(UIImage(named: "arrow-left"), forState: UIControlState.Normal)
        myBackButton.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
        myBackButton.sizeToFit()
        let myCustomBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myBackButton)
        self.navigationItem.leftBarButtonItem  = myCustomBackButtonItem
    }
    
    func popToRoot(sender:UIBarButtonItem){
        self.navigationController!.popViewControllerAnimated(true)
    }
}
