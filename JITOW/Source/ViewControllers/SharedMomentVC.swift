//
//  SharedMomentVC.swift
//  JITOW
//
//  Created by Mac103 on 4/18/16.
//  Copyright © 2016 LC. All rights reserved.
//

import Foundation
import UIKit
import AssetsLibrary
import Alamofire
import MBProgressHUD

class SharedMomentVC: UIViewController,UITableViewDelegate,UITableViewDataSource,SharedMomentContactsDelegate,UISearchBarDelegate {
    
    @IBOutlet weak var tblContacts: UITableView!
    var videoUrl:NSURL?
    var thumbnailUrl:NSURL?
    var captionText:String?
    var selectedContacts:NSMutableArray = NSMutableArray()

    var filteredContacts:NSMutableArray = NSMutableArray()
    var isFromFiltered:Bool = false
    var allContacts:NSMutableArray = NSMutableArray()
    var searchBarButtonItem:UIBarButtonItem?
    var myCustomBackButtonItem:UIBarButtonItem?
     var searchBar = UISearchBar()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(videoUrl)
        print(captionText)
        self.navigationController?.navigationBarHidden = false
        self.navigationController?.navigationBar.translucent = false
        
        searchBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Search, target: self, action: "btnSearchPressed")
        
       
        searchBarButtonItem?.tintColor = UIColor(red: 39.0/255, green: 62.0/255, blue: 90.0/255, alpha: 1.0)
        
        self.navigationItem.rightBarButtonItem = searchBarButtonItem
        
        print(thumbnailUrl)
        print(videoUrl)
        
        self.title = "Share As.."
        customBackgroundButton()
        
        searchBar.delegate = self
        searchBar.searchBarStyle = UISearchBarStyle.Default
        searchBar.showsCancelButton = true
        
        searchBar.returnKeyType = UIReturnKeyType.Done

        searchBar.enablesReturnKeyAutomatically = false
        
        
        if captionText==nil{
            
            captionText = ""
        }
        
        getAllContacts()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func customBackgroundButton(){
        
        self.navigationItem.hidesBackButton = true
        let myBackButton:UIButton = UIButton(type: UIButtonType.Custom)
        myBackButton.addTarget(self, action: "popToRoot:", forControlEvents: UIControlEvents.TouchUpInside)
        myBackButton.setTitle("", forState: UIControlState.Normal)
        myBackButton.setImage(UIImage(named: "arrow-left"), forState: UIControlState.Normal)
       myBackButton.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
        myBackButton.sizeToFit()
        myCustomBackButtonItem = UIBarButtonItem(customView: myBackButton)
        self.navigationItem.leftBarButtonItem  = myCustomBackButtonItem
    }
    func popToRoot(sender:UIBarButtonItem){
        self.navigationController!.popViewControllerAnimated(true)
    }
    @IBAction func btnDownloadPressed(sender: AnyObject) {
        
        ALAssetsLibrary().writeVideoAtPathToSavedPhotosAlbum(videoUrl) { (url, error) in
            
            if error == nil{
            
                CommomMethods.showAlert("Video Saved Successfully", view: self)
            
                
            }
            else{
                
                CommomMethods.showAlert("Something went wrong please try again", view: self)
            }
        }
        
    }
    @IBAction func btnSharePressed(sender: AnyObject) {
        
        shareVideo()
        
    }
    
    func shareVideo(){
        
        var contactStr:String?
        
        if selectedContacts.count > 0{
            
            let arr = selectedContacts as NSArray
            
            contactStr = arr.componentsJoinedByString(",")
            contactStr = "[" + contactStr! + "]"
            print(contactStr)
            

        }
        else{
            
            contactStr = ""
        }
        MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        
        let str = kBaseServerUrl + "s_moment"
        let fullUrl  = NSURL(string: str)
        
        Alamofire.upload(
            .POST,
            fullUrl!,
            multipartFormData: { multipartFormData in
                
                if let url = self.videoUrl{
                    
                    multipartFormData.appendBodyPart(fileURL: url, name: "file")
                    
                }
                
                if let url1 = self.thumbnailUrl{
                    
                    multipartFormData.appendBodyPart(fileURL: url1, name: "imagefile")
                    
                }
                
                
                multipartFormData.appendBodyPart(data: NSUserDefaults.standardUserDefaults().objectForKey(kUserid)!.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)!, name :"user_id")
                
                multipartFormData.appendBodyPart(data:self.captionText!.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)!, name :"caption")
 
                multipartFormData.appendBodyPart(data:contactStr!.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)!, name :"contacts_id")
            },
            encodingCompletion: { encodingResult in
                switch encodingResult {
                    
                case .Success(let upload, _, _):
                    upload.responseJSON { Response in
                        
                        
                        if let data = Response.data as NSData!
                        {
                            let json = JSON(data:data)
                            print(json)
                            let status = json["status"].stringValue
                            
                            if (status == "ok")
                            {
                                
                                
                                dispatch_async(dispatch_get_main_queue(),
                                    {
                                        
                                    
                                        MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
                                     
                                       
                                        
                                        
                                        
                                    }
                                )
                                
                              self.navigationController?.popViewControllerAnimated(true)
                                
                                NSNotificationCenter.defaultCenter().postNotificationName("reloadData", object: nil)  

                                
                            }
                            else{
                                
                                
                                dispatch_async(dispatch_get_main_queue(),
                                    {
                                        
                                        CommomMethods.showAlert(json["message"].stringValue, view: self)
                                        MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
                                        
                                        
                                    }
                                )
                                
                                
                            }
                            
                            
                        }
                        
                    }
                    
                case .Failure(let error):
                    
                    print(error)
                    
                    dispatch_async(dispatch_get_main_queue(),
                        {
                            
                            CommomMethods.showAlert("Something went wrong please check your internet connection and try again later", view: self)
                            MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
                            
                            
                        }
                    )
                    
                    
                }
                
            }
        )
    }
    

    //Get Contacts
    
    
    func getAllContacts(){
        
        MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        
        let str = kBaseServerUrl + "getcontact"
        let fullUrl  = NSURL(string: str)
        
        let userid = NSUserDefaults.standardUserDefaults().objectForKey(kUserid) as? String
        
        print(userid)
        
        let newPost = ["user_id":userid!]as [String:AnyObject]
        
        
        
        let request = Alamofire.request(.POST, fullUrl!, parameters: newPost, encoding: .URL)
        request.responseJSON
            { response in
                
                if let anError = response.result.error
                {
                    
                    
                    
                    dispatch_async(dispatch_get_main_queue(),
                        {
                            
                            CommomMethods.showAlert("something went wrong please check your internet connection and try again later", view: self)
                            MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
                            
                            
                        }
                    )
                    
                    print("error calling POST on /posts")
                    print(anError)
                    
                }
                else if let data = response.data as NSData!
                {
                    
                    
                    let json = JSON(data:data)
                    
                    let status = json["status"].stringValue
                    
                    if (status == "ok")
                    {
                        
                        self.allContacts.removeAllObjects()
                        
                        let pendingRequests : Array = json["response"].arrayValue
                        
                        print(pendingRequests)
                        
                        if pendingRequests.count > 0{
                            
                            
                            for d in pendingRequests{
                                
                                
                                let user = UserModel()
                                
                                if let str:NSString = d["id"].string{
                                    
                                    user.userid = str
                                    
                                }
                                if let str:NSString = d["username"].string{
                                    
                                    user.username = str
                                    
                                }
                                
                                
                                if let str:NSString = d["email"].string{
                                    
                                    user.email = str
                                    
                                }
                                if let str:NSString = d["dob"].string{
                                    
                                    user.dob = str
                                }
                                if let str:NSString = d["phone_number"].string{
                                    
                                    user.phoneNumber = str
                                }
                                if let str:NSString = d["video_path"].string{
                                    
                                    user.videoUrl = str
                                    
                                }
                                
                                self.allContacts.addObject(user)
                                
                            }
                            
                            
                         
                            
                            
                        }
                        
                        dispatch_async(dispatch_get_main_queue(),
                            {
                                
                                self.tblContacts.reloadData()
                                
                                MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
                                
                                
                            }
                        )
                       
                        
                        
                        
                    }
                    
                    
                    dispatch_async(dispatch_get_main_queue(),
                        {
                            
                            MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
                            
                            
                        }
                    )
                    
                    
                }
                
                
        }
        
        
        
    }
    
    
    //MARK: - UitabelView
    
 
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
        if isFromFiltered{
            
            return filteredContacts.count
        }
        
        return allContacts.count
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! SharedMomentContacts

        
            cell.selectionStyle = UITableViewCellSelectionStyle.None
        
        var user:UserModel?
        
        
        if isFromFiltered{
            
            user = filteredContacts.objectAtIndex(indexPath.row) as? UserModel
        }

        else{
            
            user = allContacts.objectAtIndex(indexPath.row) as? UserModel
            
        }
        
        cell.btnSelectContact.selected = false
        
        for usrid in selectedContacts{
            
            if usrid as! String == user!.userid{
                
                cell.btnSelectContact.selected = true
                break
            }
        }
        
        
        
           cell.delegate = self

           cell.lblUserName.text = user!.username as? String
        
            return cell
            
    
    }
 //Shared Moment Contact Cell Delegate
    
    func selectContact(sender:UIButton){
        
        let touchPoint = sender.convertPoint(CGPoint.zero, toView: self.tblContacts)
        
        let indexPath = self.tblContacts .indexPathForRowAtPoint(touchPoint)
        
        var user:UserModel!
        
        if isFromFiltered{
            
            user = filteredContacts.objectAtIndex(indexPath!.row) as? UserModel
        }
            
        else{
            
            user = allContacts.objectAtIndex(indexPath!.row) as? UserModel
            
        }

        
    
        if sender.selected == true{
            
       
            for usrid in selectedContacts{
                
                if usrid as! String == user.userid{
                    
                    selectedContacts.removeObject(usrid)
                    break
                }
            }
            
            
            
           
            
           
        }
        else{

            selectedContacts.addObject(user.userid!)
            
          
        
        
        }
        
    }
    
    
    func btnSearchPressed(){
        
        showSearchBar()
        
    }
    
    
    func showSearchBar() {
        
        self.navigationItem.titleView = searchBar
        self.navigationItem.setLeftBarButtonItem(nil, animated: true)
         navigationItem.setRightBarButtonItem(nil, animated: true)
        UIView.animateWithDuration(0.5, animations: {
            
            }, completion: { finished in
                self.searchBar.becomeFirstResponder()
        })
    }
    
    func hideSearchBar() {
        
        isFromFiltered = false
        
        searchBar.text = ""
        navigationItem.setLeftBarButtonItem(myCustomBackButtonItem, animated: true)
        
         navigationItem.setRightBarButtonItem(searchBarButtonItem, animated: true)
        
        UIView.animateWithDuration(0.3, animations: {
            self.navigationItem.titleView = nil
            
            }, completion: { finished in
                
        })
        
        self.tblContacts.reloadData()
    }

    
    
    //MARK: UISearchBarDelegate
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        hideSearchBar()
    }
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
       
        hideSearchBar()
   
    }
    func searchBar(searchBar: UISearchBar, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        
        isFromFiltered = true
        
        var txtAfterUpdate:NSString = searchBar.text! as NSString
        
        txtAfterUpdate = txtAfterUpdate.stringByReplacingCharactersInRange(range, withString: text)
        
        print(txtAfterUpdate.lowercaseString)
        
        let array = self.allContacts as NSArray as! [UserModel]
        
        let filtered:NSArray  = array.filter( { (user: UserModel) -> Bool in
            return user.username!.containsString(txtAfterUpdate.lowercaseString as String)
        })
        
        filteredContacts = filtered.mutableCopy() as! NSMutableArray
        
        self.tblContacts.reloadData()
        
        
        
        
        return true
    }
    
    
}