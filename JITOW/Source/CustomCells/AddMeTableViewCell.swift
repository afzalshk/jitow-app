//
//  AddMeTableViewCell.swift
//  JITOW
//
//  Created by Irfan Malik on 2/19/16.
//  Copyright © 2016 LC. All rights reserved.
//

import UIKit
protocol AddMeCellDelegate{
    
    func updateRequestStatus(sender:UIButton,status:Int)
}
class AddMeTableViewCell: UITableViewCell {

    @IBOutlet weak var lblUserName: UILabel!
   
    @IBOutlet weak var btnReject: UIButton!
    
    @IBOutlet weak var btnAccept: UIButton!
    
    var delegate:AddMeCellDelegate! = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func btnAcceptPressed(sender: UIButton) {
        
        delegate?.updateRequestStatus(sender, status: 1)
    }
   
    @IBAction func btnRejectPressed(sender: UIButton) {
        
         delegate?.updateRequestStatus(sender, status: 0)
    }
}
