//
//  TabelViewCell.m
//  yupApp
//
//  Created by Irfan Malik on 9/7/15.
//  Copyright (c) 2015 LC. All rights reserved.
//

#import "TabelViewCell.h"
#import "UIImage+Stretchable.h"
@implementation TabelViewCell
{


}



//ChatViewCell Setting
#pragma mark - public method
+(CGSize) getSizeforText:(NSString *)message andSize:(UIView *)contentView
{
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    context.minimumScaleFactor = 0.8;
//    NSUserDefaults *userDefault =[NSUserDefaults standardUserDefaults];
//    NSString * fontSize = [userDefault valueForKey:@"fontSize"];
    UIFont *font = [UIFont fontWithName:@"Helvetica" size:16.0];;
//    if (fontSize)
//    {
//        font= [UIFont fontWithName:@"Helvetica" size:[fontSize floatValue]];
//    }
//    else
//    {
//        font= [UIFont fontWithName:@"Helvetica" size:16.0];
//    }
    
    NSLog(@"what's here %f",contentView.frame.size.width);
   
    CGRect frame = [message boundingRectWithSize:CGSizeMake((contentView.frame.size.width - 110.0), 2000)
                                         options:NSStringDrawingUsesLineFragmentOrigin
                                      attributes:@{ NSFontAttributeName : font}
                                         context:context];
    return frame.size;
}




+(CGSize)SizeforText:(NSString *)message andSize:(UIView *)view
{
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    context.minimumScaleFactor = 0.8;
    UIFont *font =  [UIFont fontWithName:@"Helvetica" size:16.0];
    
    
    
    CGRect frame = [message boundingRectWithSize:CGSizeMake((view.frame.size.width - 100.0), 2000)
                                         options:NSStringDrawingUsesLineFragmentOrigin
                                      attributes:@{ NSFontAttributeName : font}
                                         context:context];
    return frame.size;
}


+(UIImage *)streatchImage
{
    UIImage *img=[UIImage stretchableImageWithName:@"bg_chatmessage"
                            extension:@"png"
                               topCap:20
                              leftCap:8
                            bottomCap:13
                          andRightCap:26];
    
    return img;

}



+(UIImage *)recieverStreatchImage
{
    UIImage *img=[UIImage stretchableImageWithName:@"bg_chatmessage_others"
                                         extension:@"png"
                                            topCap:20
                                           leftCap:8
                                         bottomCap:13
                                       andRightCap:26];
    
    return img;
    
}

@end
