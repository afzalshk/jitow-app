//
//  FriendsMomentCell.swift
//  JITOW
//
//  Created by Mac103 on 4/21/16.
//  Copyright © 2016 LC. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage
protocol FriendsMomentCellDelegate{
    
    
    func showHide(sender:UIButton)
    func hide(sender:UIButton)
    func firstVideoButton(sender:UIButton)
    
    
}


class FriendsMomentCell: UITableViewCell{

    
    var delegate:FriendsMomentCellDelegate! = nil
    
    var newUser:UserModel = UserModel()
    @IBOutlet weak var imgFirstThumbnail: UIImageView!
    @IBOutlet weak var lblCaption: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var btnShow: UIButton!
    @IBOutlet weak var imgVideoIcon: UIImageView!
    @IBOutlet weak var btnNextHeight: NSLayoutConstraint!
    @IBOutlet weak var collHeight: NSLayoutConstraint!
    
    @IBOutlet weak var cellCollView: CustomCollectionView!
    @IBOutlet weak var btnHide: UIButton!
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
        
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?)
    {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)!
    }
    // Set Data
    
    func setData(user:UserModel){
        
        
        if user.isHidden == false{
            
            self.collHeight.constant = 0
            self.cellCollView.hidden = true
            self.btnHide.hidden = true
            self.btnNextHeight.constant = 0
            self.imgVideoIcon.hidden = true

            self.lblTime.textColor = UIColor.lightGrayColor()
            
            self.btnShow.hidden = false
            
        }
        else{
            
            
            self.collHeight.constant = 51
            self.cellCollView.hidden = false
            self.btnHide.hidden = false
            self.btnNextHeight.constant = 30
            self.imgVideoIcon.hidden = false
            self.lblTime.textColor = UIColor.redColor()
            self.btnShow.hidden = true
            
        }
    
    self.lblUserName.text = user.username as? String
        
    if user.momentsArray.count > 0{
    
    let v:VideoModel = (user.momentsArray.objectAtIndex(0) as? VideoModel)!
        
        
    
    self.lblCaption.text = v.caption as? String
        
        if user.isHidden == false{
            
//            let f:NSDateFormatter = NSDateFormatter()
//            f.timeZone = NSTimeZone.localTimeZone()
//            f.dateFormat = "yyyy-MM-dd hh:mma"
//            
//            let startDate = f.dateFromString((v.date as? String)!)
            
           // self.lblTime.text = CommomMethods.getElapsedInterval(startDate!)
            self.lblTime.text = "4d"
        
        }
        else{
            
            lblTime.text = String(user.momentsArray.count)
            
        }
        
    
    imgFirstThumbnail.sd_setImageWithURL(NSURL(string: (v.thumbnail_url as? String)!), placeholderImage: UIImage(named: "chat_imgplacehold"))
        
        
       
        
        
       
}

    }
    
    func setCollViewDelegate(delegate:UICollectionViewDelegate,dataSoure:UICollectionViewDataSource, indexPath:NSIndexPath){
        
        self.cellCollView.delegate = delegate
        self.cellCollView.dataSource = dataSoure
        self.cellCollView.indexPath = indexPath
        
         self.cellCollView .reloadData()
        
        
    }
    
    @IBAction func btnHidePressed(sender: UIButton) {
        
        delegate?.hide(sender)
    
    }
    @IBAction func btnShowHidePressed(sender: UIButton) {
        
        
        delegate?.showHide(sender)
        
        
        
    }
    
    @IBAction func btnFirstVideoClicked(sender: UIButton) {
        
        delegate?.firstVideoButton(sender)
        
    }
}

class videosCollectionViewCell: UICollectionViewCell{
    
    @IBOutlet weak var imgCollCell: UIImageView!
    
    
}

class CustomCollectionView:UICollectionView{
    
    var indexPath:NSIndexPath?
    
}