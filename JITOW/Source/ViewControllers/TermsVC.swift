//
//  TermsVC.swift
//  JITOW
//
//  Created by Afzal Shiekh on 13/08/2016.
//  Copyright © 2016 LC. All rights reserved.
//

import UIKit

class TermsVC: UIViewController {
    @IBOutlet weak var termsWebView: UIWebView!

    var isFromPrivacy:Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        var documentName:String?
        
        if isFromPrivacy == true {
            
            documentName = "privacy"
            
        }
        else{
            
            documentName = "terms"
        }
        
        if let pdf = NSBundle.mainBundle().URLForResource(documentName, withExtension: "pdf", subdirectory: nil, localization: nil)  {
            let req = NSURLRequest(URL: pdf)
            termsWebView.loadRequest(req)
            
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func btnBackPressed(sender: AnyObject) {
        
        self.navigationController?.popViewControllerAnimated(true)
    }

}
