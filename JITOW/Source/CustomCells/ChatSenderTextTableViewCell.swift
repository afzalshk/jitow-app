//
//  ChatSenderTextTableViewCell.swift
//  JITOW
//
//  Created by Irfan Malik on 2/15/16.
//  Copyright © 2016 LC. All rights reserved.
//

import UIKit

class ChatSenderTextTableViewCell: UITableViewCell {

    @IBOutlet weak var bgImageView: UIImageView!
    @IBOutlet weak var lbl_messageText: UILabel!
    @IBOutlet weak var lbl_status: UILabel!
    
    
    override func awakeFromNib()
    {
        
        
    }
    
    override func layoutSubviews()
    {
        super.layoutSubviews()
        
        let  messageSize :CGSize = TabelViewCell.getSizeforText(self.lbl_messageText.text, andSize:self.contentView)
        
        self.bgImageView.frame = CGRectMake(CGRectGetMaxX(self.contentView.frame) - messageSize.width - 55.0,
            CGRectGetMinY(self.contentView.frame) + 15,
            messageSize.width + 30.0,
            messageSize.height  + 14.0);
        
        self.lbl_messageText.frame = CGRectMake(CGRectGetMinX(self.bgImageView.frame) + 10.0,
            CGRectGetMinY(self.bgImageView.frame) + 0.0,
            messageSize.width,  messageSize.height + 8.0);
        
        self.lbl_status.frame.origin = CGPointMake(CGRectGetMinX(self.bgImageView.frame) + 10.0,  self.lbl_messageText.frame.origin.y + self.lbl_messageText.frame.size.height + 3)
        
        
        self.bgImageView.image = TabelViewCell.streatchImage()
        
    }
    
}
