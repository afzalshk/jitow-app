//
//  ReceiverMediaCell.swift
//  JITOW
//
//  Created by Mac103 on 4/12/16.
//  Copyright © 2016 LC. All rights reserved.
//

import Foundation

protocol ReceiverMediaCellDelegate{
    
    func btnRecvImageVideoClicked(sender:UIButton)
    
}

class ReceiverMediaCell : UITableViewCell
{
    
    @IBOutlet weak var imgPlaceHolder: UIImageView!
    @IBOutlet weak var animation_img_view: UIImageView!
    
    @IBOutlet weak var playIcon: UIImageView!
    
    @IBOutlet weak var btnTapImageOrVideo: UIButton!
    
    var delegate:ReceiverMediaCellDelegate! = nil
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?)
    {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)!
    }
    
    //MARK: - Loader Animation
    func showCustomLoadIndicator()
    {
        
        if self.animation_img_view.layer.animationForKey("SpinAnimation") == nil
        {
            self.animation_img_view.layer.addAnimation(self.animationToRotateImage(), forKey:"SpinAnimation")
        }
        
        self.animation_img_view.hidden = false
    }
    
    func removeCustomLoadIndicator()
    {
        
        self.animation_img_view.hidden = true
    }
    
    func animationToRotateImage() -> CABasicAnimation {
        let animation: CABasicAnimation = CABasicAnimation(keyPath:"transform.rotation.z")
        animation.fromValue = NSNumber(float:0.0)
        animation.toValue = NSNumber(double:(2*M_PI))
        animation.duration = 0.6
        animation.repeatCount = Float.infinity//INFINITY
        return animation
    }
    
    @IBAction func btnTapImagePressed(sender: UIButton) {
        
        delegate?.btnRecvImageVideoClicked(sender)
    }
    
}