//
//  FriendsMomentsVC.swift
//  JITOW
//
//  Created by Mac103 on 4/20/16.
//  Copyright © 2016 LC. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import MBProgressHUD

class FriendsMomentsVC: UIViewController,UITableViewDelegate,UITableViewDataSource,FriendsMomentCellDelegate,UICollectionViewDelegate,UICollectionViewDataSource {

    //identifier toMoments
    
    var allFriendsMoments:NSMutableArray = NSMutableArray()
    @IBOutlet weak var tblMoments: UITableView!
    var selectedVideo:VideoModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
         NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("popBack"), name:"indexChanged", object: nil);
        
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        
           getFriendsMoments()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if allFriendsMoments.count > 0{
            
            return allFriendsMoments.count
        }
        return 0
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
    
        if allFriendsMoments.count > 0{
            
            return 1
        }
        return 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! FriendsMomentCell
       cell.layer.cornerRadius = 10.0
        
        let u:UserModel = allFriendsMoments.objectAtIndex(indexPath.section) as! UserModel
        cell.delegate = self
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        

      cell.setData(u)
        
        
       return cell
    
    
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        let user:UserModel = allFriendsMoments.objectAtIndex(indexPath.section) as! UserModel
        if user.isHidden == false || user.momentsArray.count==1 {
         
            return 58
        }
        
        return 110
    
    }
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if section == 0{
            return 0
        }
        return 10
    }
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        return UIView(frame:CGRect.zero)
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        
        if let newCell:FriendsMomentCell = cell as? FriendsMomentCell{
            
            newCell.setCollViewDelegate(self, dataSoure: self, indexPath: indexPath)
            
        }
        
        
    }
    func showHide(sender:UIButton){
        
        
        
        let touchPoint = sender.convertPoint(CGPoint.zero, toView: self.tblMoments)
        
        let indexPath = self.tblMoments .indexPathForRowAtPoint(touchPoint)
        
        
        let user:UserModel = self.allFriendsMoments.objectAtIndex(indexPath!.section) as! UserModel
        
        if user.momentsArray.count==1{
            
            return
        }
        
        
        user.isHidden = true
        
        allFriendsMoments.replaceObjectAtIndex((indexPath?.section)!, withObject: user)
        
        self.tblMoments .beginUpdates()
        
        self.tblMoments.reloadRowsAtIndexPaths([indexPath!], withRowAnimation: UITableViewRowAnimation.Fade)
        
        self.tblMoments.endUpdates()
        
        
    }
    
    
    
    
    func hide(sender:UIButton){
        
        
        let touchPoint = sender.convertPoint(CGPoint.zero, toView: self.tblMoments)
        
        let indexPath = self.tblMoments .indexPathForRowAtPoint(touchPoint)
    
        
        let user:UserModel = self.allFriendsMoments.objectAtIndex(indexPath!.section) as! UserModel
        
       user.isHidden = false
        
        allFriendsMoments.replaceObjectAtIndex((indexPath?.section)!, withObject: user)
        
        
        self.tblMoments .beginUpdates()
        
        self.tblMoments.reloadRowsAtIndexPaths([indexPath!], withRowAnimation: UITableViewRowAnimation.Fade)
        
        self.tblMoments.endUpdates()
        
    }
    
    func firstVideoButton(sender:UIButton){
        
        
        let touchPoint = sender.convertPoint(CGPoint.zero, toView: self.tblMoments)
        
        let indexPath = self.tblMoments .indexPathForRowAtPoint(touchPoint)
        
        
        let user:UserModel = self.allFriendsMoments.objectAtIndex(indexPath!.section) as! UserModel
      
        
        
        
        let video:VideoModel = user.momentsArray.objectAtIndex(0) as! VideoModel
        
        
        selectedVideo = video
        
        self.performSegueWithIdentifier("toPlayVideo", sender: self)
        
        
    }
    func popBack(){
        
      self.willMoveToParentViewController(self.parentViewController)
    self.didMoveToParentViewController(self.parentViewController)
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
        
    }
   
    
    // Get Friends Moments
    
    func getFriendsMoments(){
        
        MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        
        let str = kBaseServerUrl + "g_cmoment"
        let fullUrl  = NSURL(string: str)
        
        let userid = NSUserDefaults.standardUserDefaults().objectForKey(kUserid) as? String
        
        
        let newPost = ["user_id":userid!]as [String:AnyObject]
        
        
        

        
        
        
        let request = Alamofire.request(.POST, fullUrl!, parameters: newPost, encoding: .URL)
        request.responseJSON
            { response in
                
                if let anError = response.result.error
                {
                    
                    
                    
                    dispatch_async(dispatch_get_main_queue(),
                        {
                            
                            CommomMethods.showAlert("something went wrong please check your internet connection and try again later", view: self)
                            MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
                            
                            
                        }
                    )
                    
                    print("error calling POST on /posts")
                    print(anError)
                    
                }
                else if let data = response.data as NSData!
                {
                    
                    
                    let json = JSON(data:data)
                    
                    let status = json["status"].stringValue
                    
                    if (status == "ok")
                    {
                        
                        self.allFriendsMoments.removeAllObjects()
                        
                        let pendingRequests : Array = json["response"].arrayValue
                        
                        
                        if pendingRequests.count > 0{
                            
                            
                            for d in pendingRequests{
                                
                               
                                let user = UserModel()
                                
                                if let str:NSString = d["user_id"].string{
                                    
                                    user.userid = str
                                    
                                }
                                if let str:NSString = d["user_name"].string{
                                    
                                    user.username = str
                                    
                                }
                                
                               
                                
                                
                                if let arr:NSArray = d["videos"].arrayObject{
                                    
                                    if arr.count > 0 {
                                        
                                        
                                        for index in 0..<arr.count {
                                            
                                            let video = VideoModel()
                                            
                                            let dict:NSDictionary = arr.objectAtIndex(index) as! NSDictionary
                                            
                                            
                                            if let str:NSString = dict["caption"]as? NSString{
                                                
                                                video.caption = str
                                                
                                            }
                                            
                                            if let str:NSString = dict["emoji_num"]as? NSString{
                                                
                                                if str != ""{
                                                    
                                                    var newstr = str.stringByReplacingOccurrencesOfString("[", withString: "")
                                                    
                                                    newstr = newstr.stringByReplacingOccurrencesOfString("]", withString: "")
                                                    
                                                    
                                                    
                                                    let emojis = newstr.componentsSeparatedByString(",")
                                                    
                                                    for str1 in emojis{
                                                        
                                                        
                                                        video.emojiList.addObject(str1)
                                                        
                                                        
                                                    }
                                                    
                                                }
                                                
                                                
                                            }
                                            
                                            
                                            
                                            if let str:NSString = dict["file_path"]as? NSString{
                                                
                                                video.videoUrl = str
                                                
                                            }
                                            
                                            if let str:NSString = dict["file_thumbnail_path"]as? NSString{
                                                
                                                video.thumbnail_url = str
                                                
                                            }
                                            
                                            if let str:NSString = dict["date"] as? NSString{
                                                
                                                video.date = str
                                                
                                            }
                                            
                                            if let str:NSString = dict["id"]as? NSString{
                                                
                                                video.video_id = str
                                                
                                            }
                                            
                                            if let str:NSString = dict["emoji"]as? NSString{
                                                
                                                video.emojiCount = str
                                                
                                            }
                                            
                                            if let str:NSString = dict["views"]as? NSString{
                                                
                                                video.sceneCount = str
                                                
                                            }
                                            
                                            
                                           
                                            
                                            
                                            user.momentsArray .addObject(video)
                                            
                                            
                                            
                                            
                                        }
                                        
                                    }
                                   
                            
                                    
                                    
                                }
                                
                                self.allFriendsMoments.addObject(user)
                                
                               
                                
                            }
                            
                            
                         
                            
                            dispatch_async(dispatch_get_main_queue(),
                                {
                                    
                                    self.tblMoments.reloadData()
                                    
                                    
                                }
                            )
                            
                            
                        }
                        
                        
                        
                        
                        
                        
                    }
                    
                    
                    dispatch_async(dispatch_get_main_queue(),
                        {
                            
                            MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
                            
                            
                        }
                    )
                    
                    
                }
                
                
        }
        
        
        
    }
    
    
    //CollectionView DataSource
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        
        let index = (collectionView as! CustomCollectionView).indexPath
        
        let user:UserModel = allFriendsMoments.objectAtIndex((index?.section)!) as! UserModel
        
        if user.momentsArray.count>1{
        
            return user.momentsArray.count-1
        }
        
        return 0
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell{
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Video",
                                                                         forIndexPath: indexPath) as! videosCollectionViewCell
        
        let index = (collectionView as! CustomCollectionView).indexPath
        
        let user:UserModel = allFriendsMoments.objectAtIndex((index?.section)!) as! UserModel
        
        
        let video:VideoModel = user.momentsArray.objectAtIndex(indexPath.item+1) as! VideoModel
        
        
        
        cell.imgCollCell.sd_setImageWithURL(NSURL(string: (video.thumbnail_url as? String)!), placeholderImage: UIImage(named:"chat_imgplacehold"))
        
        
        
        return cell
        
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        let index = (collectionView as! CustomCollectionView).indexPath
        
        let user:UserModel = allFriendsMoments.objectAtIndex((index?.section)!) as! UserModel
        
        
        let video:VideoModel = user.momentsArray.objectAtIndex(indexPath.item+1) as! VideoModel
        

        selectedVideo = video
        
        self.performSegueWithIdentifier("toPlayVideo", sender: self)
        
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "toPlayVideo"{
            
            let vc:PlayVideoVC = segue.destinationViewController as! PlayVideoVC
            
            vc.video = selectedVideo
            
    }
}
        
}