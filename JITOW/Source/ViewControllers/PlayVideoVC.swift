//
//  PlayVideoVC.swift
//  JITOW
//
//  Created by Mac103 on 4/25/16.
//  Copyright © 2016 LC. All rights reserved.
//

import Foundation
import UIKit
import MobileCoreServices
import MediaPlayer
import Alamofire
import AssetsLibrary
import MBProgressHUD




class PlayVideoVC: UIViewController,selectEmojiDelegate,UITableViewDataSource,UITableViewDelegate {
 
    
    @IBOutlet var doubleTap: UITapGestureRecognizer!
    @IBOutlet var singleTap: UITapGestureRecognizer!
    @IBOutlet weak var tblEmoji: UITableView!
    @IBOutlet weak var btnShowEmoji: UIButton!
    @IBOutlet weak var btnDownload: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var lblEmoji: UILabel!
    @IBOutlet weak var lblScene: UILabel!
    @IBOutlet weak var imgEmoji: UIImageView!
    @IBOutlet weak var imgScene: UIImageView!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var viewVideoPlayer: UIView!
    var moviePlayer: MPMoviePlayerController?
    var video:VideoModel?
    var isFromUser:Bool = false
    override func viewDidLoad() {
        
        
        
         NSTimer.scheduledTimerWithTimeInterval(0.3, target: self, selector: Selector("increaseView"), userInfo: nil, repeats:false)
        
        if isFromUser == true{
            singleTap.enabled = false
            btnDelete.hidden = false
            btnDownload.hidden = false
        }
        else{
            
           singleTap.enabled = true
           btnDelete.hidden = true
           btnDownload.hidden = true
        }
        
        lblScene.text = video?.sceneCount as? String
        lblEmoji.text = video?.emojiCount as? String
        tblEmoji.hidden = true

        
    singleTap.requireGestureRecognizerToFail(doubleTap)
        
        
    }
    override func viewDidDisappear(animated: Bool) {
        
        if moviePlayer?.playbackState  == MPMoviePlaybackState.Playing || moviePlayer?.playbackState  == MPMoviePlaybackState.Paused || moviePlayer?.playbackState  == MPMoviePlaybackState.Interrupted || moviePlayer?.playbackState  == MPMoviePlaybackState.SeekingForward || moviePlayer?.playbackState  == MPMoviePlaybackState.SeekingBackward {
            
            moviePlayer?.stop()
            moviePlayer = nil
            
            
        }
        
        
    }
    override func viewWillAppear(animated: Bool) {
        
        self.navigationController?.navigationBarHidden = true
        playVideo()
        
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    @IBAction func btnCancelPressed(sender: AnyObject) {
        
        self.navigationController?.popViewControllerAnimated(true)
        
    }
    
    
    func playVideo(){
        
        
        
        let url:NSURL = NSURL(string: video?.videoUrl as! String)!
 
        moviePlayer = MPMoviePlayerController(contentURL:url)
        if let player = moviePlayer {
            player.view.frame = viewVideoPlayer.bounds
            player.view.frame.size.height = UIScreen.mainScreen().bounds.size.height
            
            player.backgroundView.backgroundColor = UIColor.clearColor()
            
           player.view.backgroundColor = UIColor.clearColor()
            
            player.prepareToPlay()
            player.scalingMode = .AspectFill
            player.controlStyle = MPMovieControlStyle.None
            viewVideoPlayer.addSubview(player.view)
            viewVideoPlayer.bringSubviewToFront(btnCancel)
            viewVideoPlayer.bringSubviewToFront(imgEmoji)
            viewVideoPlayer.bringSubviewToFront(imgScene)
            viewVideoPlayer.bringSubviewToFront(lblEmoji)
            viewVideoPlayer.bringSubviewToFront(lblScene)
            viewVideoPlayer.bringSubviewToFront(btnDelete)
            viewVideoPlayer.bringSubviewToFront(btnDownload)
            
            viewVideoPlayer.bringSubviewToFront(btnShowEmoji)
            
            viewVideoPlayer.bringSubviewToFront(tblEmoji)
            
            
            player.play()
        }
    }

    @IBAction func btnDeletePressed(sender: AnyObject) {
        
        
        MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
        
        
        let str = kBaseServerUrl + "d_smoment"
        let fullUrl  = NSURL(string: str)
        
        let newPost = ["video_id":(video!.video_id as? String)!,"user_id":NSUserDefaults.standardUserDefaults().objectForKey(kUserid)!] as [String:AnyObject]
        
        
        let request = Alamofire.request(.POST, fullUrl!, parameters: newPost, encoding: .URL)
        request.responseJSON
            { response in
                
                if let anError = response.result.error
                {
                    print("error calling POST on /posts")
                    print(anError)
                    
                    MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
                }
                else if let data = response.data as NSData!
                {
                    
                    
                    let json = JSON(data:data)
                    
                    let status = json["status"].stringValue
                    
                    if (status == "ok")
                    {
                        
                        NSNotificationCenter.defaultCenter().postNotificationName("reloadData", object: nil)
                        
                        self.navigationController?.popViewControllerAnimated(true)
                        
                        
                    }
                    
                    
                    
                    
                    
                    MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
                    
                    
                    
                    
                    
                }
                
                
        }
        
        
        
        
        
        
        
        
        
    }
    
    @IBAction func btnDownloadPressed(sender: AnyObject) {
        
     
         saveToCameraRoll()
        
    }

    
    func saveToCameraRoll(){
        
        MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        var localFileUrl: String?
        
        
        Alamofire.download(.GET, NSURL(string: video?.videoUrl as! String)!) { (temporaryURL, response) -> NSURL in
            let fileManager = NSFileManager.defaultManager()
            let directoryURL = try! fileManager.URLForDirectory(.DocumentDirectory, inDomain: .UserDomainMask, appropriateForURL: nil, create: false)
           
            let finalPath = directoryURL.URLByAppendingPathComponent("\(NSUUID()).\(response.suggestedFilename!)")
            localFileUrl = finalPath.absoluteString
        
            return finalPath
            
            }.response { _, response, _, error in
                if let error = error {
                    
                    //print("failed with error: \(error)")
                    
                    MBProgressHUD.hideHUDForView(self.view, animated: true)
                    
               
                } else if let statusCode = response?.statusCode where statusCode != 200 {
                    
                   // print("unsuccessful with statusCode: \(statusCode)")
                    
                    MBProgressHUD.hideHUDForView(self.view, animated: true)
               
                
                } else {
                    
                    if let path = localFileUrl {
                        let isVideoCompatible = UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(path)
                        print("bool: \(isVideoCompatible)")
                        
                        let library = ALAssetsLibrary()
                        
                        library.writeVideoAtPathToSavedPhotosAlbum(NSURL(string: path), completionBlock: { (url, error) -> Void in
                            // Done! Go check your camera roll
                            
                            MBProgressHUD.hideHUDForView(self.view, animated: true)
                        })
                    }
                }
        }
    }
    
    func increaseView(){
        
        let str = kBaseServerUrl + "a_view"
        let fullUrl  = NSURL(string: str)
        
        let newPost = ["views":1,"user_id":NSUserDefaults.standardUserDefaults().objectForKey(kUserid)!, "video_id":video?.video_id as! String] as [String:AnyObject]
        
        
        let request = Alamofire.request(.POST, fullUrl!, parameters: newPost, encoding: .URL)
        request.responseJSON
            { response in
                
                if let anError = response.result.error
                {
                   
                }
                else if let data = response.data as NSData!
                {
                    
                    
                    let json = JSON(data:data)
                    
                    let status = json["status"].stringValue
                    
                    if (status == "ok")
                    {
                        
                    
                        
                    }
                   
                    
                    
                    
                }
                
                
        }

    }
    
  
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "toEmoji"{
        
            let vc:EmojiListVC = (segue.destinationViewController as? EmojiListVC)!
            vc.delegate = self
            vc.videoEmoji = video
            
        
        }
            
            
    }
    
    func selectedEmoji(sender:Int){
        
        video?.emojiList.addObject("\(sender)")
        var s = video?.emojiCount?.intValue
        s = s! + 1
        video?.emojiCount = "\(s!)"
        self.lblEmoji.text = "\(s!)"
        
        tblEmoji.reloadData()
        tblEmoji.hidden = false
        
        
    }
    
    // Emoji TableView
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return (video?.emojiList.count)!
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
        
        let str1:NSString = video?.emojiList.objectAtIndex(indexPath.row) as! NSString
        
        cell.textLabel?.text = String(UnicodeScalar(str1.integerValue))
        cell.textLabel?.font = UIFont.systemFontOfSize(30)
        cell.backgroundColor = UIColor.clearColor()
        
        return cell
    }
    
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        return UIView(frame:CGRect.zero)
        
    }
    
    @IBAction func doubleTapPressed(sender: AnyObject) {
        
        tblEmoji.hidden = !tblEmoji.hidden
    }
    @IBAction func singleTapPressed(sender: AnyObject) {
        
         self.performSegueWithIdentifier("toEmoji", sender: self)
    }
}