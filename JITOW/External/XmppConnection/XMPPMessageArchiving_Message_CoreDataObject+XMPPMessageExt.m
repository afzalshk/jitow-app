//
//  XMPPMessageArchiving_Message_CoreDataObject+XMPPMessageExt.m
//  yupApp
//
//  Created by Mac101 on 8/20/15.
//  Copyright (c) 2015 LC. All rights reserved.
//

#import "XMPPMessageArchiving_Message_CoreDataObject+XMPPMessageExt.h"

@implementation XMPPMessageArchiving_Message_CoreDataObject (XMPPMessageExt)



-(void)post
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"pushNotification" object:self];
}

- (void)didSave
{
    [self performSelectorOnMainThread:@selector(post) withObject:nil waitUntilDone:NO];
    ;
}


@end
