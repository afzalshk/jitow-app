//
//  MediaContactTableViewCell.swift
//  JITOW
//
//  Created by Irfan Malik on 2/11/16.
//  Copyright © 2016 LC. All rights reserved.
//

import UIKit

class MediaContactTableViewCell: UITableViewCell {

    @IBOutlet weak var lbl_title: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.layer.cornerRadius = 4

        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
