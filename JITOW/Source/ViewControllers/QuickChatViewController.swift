//
//  QuickChatViewController.swift
//  JITOW
//
//  Created by Irfan Malik on 2/10/16.
//  Copyright © 2016 LC. All rights reserved.
//

import UIKit
import Alamofire
import QuartzCore
import MBProgressHUD

class QuickChatViewController: UIViewController, UITableViewDataSource,UserContactsDelegate,UISearchBarDelegate {
    
    
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var container_view: UIView!
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var tabel: UITableView!
    
    var allDataArray:NSMutableArray = NSMutableArray()
    var allContacts:NSMutableArray = NSMutableArray()
    
    var filteredData:NSMutableArray = NSMutableArray()
    var isFromFiltered:Bool = false
    
    
    
    var isPerformSegue :Bool = false
    var rowNo: NSIndexPath?
    let appDel = UIApplication.sharedApplication().delegate! as! AppDelegate
    
    var isFromContact:Bool =  false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        container_view.hidden = true
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("getData"), name:"pushNotification", object: nil);
        
         NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("hideChat"), name:"indexChanged", object: nil);
        
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("hideChatForBack"), name:"indexChangedBack", object: nil);
        
        
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("getAllContacts"), name:"reloadContactsData", object: nil);
        
        
        searchBar.delegate = self
      //  searchBar.searchBarStyle = UISearchBarStyle.Default
        searchBar.showsCancelButton = true
        
        searchBar.returnKeyType = UIReturnKeyType.Done
        
        searchBar.enablesReturnKeyAutomatically = false
        
        
        searchBar.hidden = true
        
         
        
        getAllContacts()
        
       
        
       
        
        
    }

    override func viewWillAppear(animated: Bool) {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidDisappear(animated: Bool) {
        
        hideSearchBar()
        
    }
    
    //MARK: - UitabelView
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        
        if isFromFiltered == true{
            
            return filteredData.count
        }
        return allDataArray.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
      
        var fullData:NSMutableArray = NSMutableArray()
        if isFromFiltered == true{
            
            fullData = filteredData
        }
        else{
            
            fullData = allDataArray
            
        }
        
        
      let arr = fullData.objectAtIndex(section)
            
        return arr.count
    
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        
        var fullData:NSMutableArray = NSMutableArray()
        if isFromFiltered == true{
            
            fullData = filteredData
        }
        else{
            
            fullData = allDataArray
            
        }
        
        
        if fullData.objectAtIndex(indexPath.section).firstObject!! .isKindOfClass(XMPPMessageArchiving_Contact_CoreDataObject){
           
            let messagesArr:NSMutableArray = fullData.objectAtIndex(indexPath.section) as! NSMutableArray
            
            
            var cell:QuickChatTableViewCell? = self.tabel.dequeueReusableCellWithIdentifier("Cell") as? QuickChatTableViewCell
            if (cell == nil) {
                let nib:NSArray = NSBundle.mainBundle().loadNibNamed("QuickChatTableViewCell", owner: self, options: nil)
                cell = nib.objectAtIndex(0) as? QuickChatTableViewCell
            }
            cell?.selectionStyle = UITableViewCellSelectionStyle.None
            
            let message :XMPPMessageArchiving_Contact_CoreDataObject = messagesArr[indexPath.row] as! XMPPMessageArchiving_Contact_CoreDataObject
            
            print(message.bareJid.user)
            print(message.streamBareJidStr)
            print(message.bareJidStr)
            
            let userName :String?
            let str = message.bareJidStr
            if let comma = str.characters.indexOf("@")
            {
                let substr = str[str.startIndex..<comma]
                userName = substr
            }
            else
            {
                userName = str
            }
            
          
            
            
            
            let time = CommomMethods.getElapsedInterval(message.mostRecentMessageTimestamp) as String
            
            
            
            
            let data = message.mostRecentMessageBody.dataUsingEncoding(NSUTF8StringEncoding)
            
            let str1 = NSString(data: data!, encoding: NSNonLossyASCIIStringEncoding)
            
            
            
            
            
            cell?.lblMessage.text = str1 as? String
            cell?.lblTime.text = time
            
            
            
            print(message.status)
            
            
            if(message.mostRecentMessageOutgoing.boolValue == true){
                
                cell?.view_read.hidden = true
                cell?.imgStatus.hidden = false
                
                if message.status == "Seen"{
                    
                    
                    cell?.imgStatus.image = UIImage(named: "quickchat-view")
                    
                    
                    
                }
                else{
                    
                    
                    cell?.imgStatus.image = UIImage(named: "quickchat-forward")
                    
                    
                    
                }

                
                
            }
            else{
                
                cell?.imgStatus.hidden = true
                cell?.view_read.hidden = false
                
                if message.status == "seen"{
                    
                    cell?.view_read.hidden = true
                    
                    
                    
                    
                }
                else{
                    
                    
                    
                    
                    cell?.view_read.hidden = false
                    
                }

                
            }
            
            for index in 0..<allContacts.count {
                
                let user:UserModel = (allContacts.objectAtIndex(index) as? UserModel)!
                
                if user.phoneNumber == userName{
                    
                    cell?.lblUserName.text = user.username as? String
                    
                    
                    break
                }
                
            }
            
           // cell?.profile_img.image = UIImage(named: "chat_imgplacehold")
            
            
            return cell!
            
        }
        else if fullData.objectAtIndex(indexPath.section).firstObject!!.isKindOfClass(UserModel){
            
            let contactTabel = fullData.objectAtIndex(indexPath.section)
        
            var cell1:ContactTableViewCell? = self.tabel.dequeueReusableCellWithIdentifier("contactCell") as? ContactTableViewCell
            if (cell1 == nil) {
                let nib:NSArray = NSBundle.mainBundle().loadNibNamed("ContactTableViewCell", owner: self, options: nil)
                cell1 = nib.objectAtIndex(0) as? ContactTableViewCell
            }
            cell1?.selectionStyle = UITableViewCellSelectionStyle.None
            
            print(indexPath.row)
            
                    
           let user = contactTabel.objectAtIndex(indexPath.row) as! UserModel
            
            
            
            cell1?.lblUserName.text = user.username as? String
            
            cell1?.delegate = self
            
            return cell1!
            
            
        
        }
        
    
        
        return UITableViewCell()
        
}
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        
        self.hideSearchBar()
        

        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        var fullData:NSMutableArray = NSMutableArray()
        if isFromFiltered == true{
            
            fullData = filteredData
        }
        else{
            
            fullData = allDataArray
            
        }
        
         if fullData.objectAtIndex(indexPath.section).firstObject!! .isKindOfClass(XMPPMessageArchiving_Contact_CoreDataObject){
        
            var isBlocked:Bool = true
            
            let messagesArr:NSMutableArray = fullData.objectAtIndex(indexPath.section) as! NSMutableArray
            
            
            let message :XMPPMessageArchiving_Contact_CoreDataObject = messagesArr[indexPath.row] as! XMPPMessageArchiving_Contact_CoreDataObject
            
            
            
            var userName :String?
            let str = message.bareJidStr
            if let comma = str.characters.indexOf("@")
            {
                let substr = str[str.startIndex..<comma]
                userName = substr
            }
            else
            {
                userName = str
            }
            
            for index in 0..<allContacts.count {
                
                let user:UserModel = (allContacts.objectAtIndex(index) as? UserModel)!
                
                if user.phoneNumber == userName{
                    
                    isBlocked = false
                    break
                }
                
            }
            
            
            if isBlocked == true{
                
                CommomMethods.showAlert("you are n't allowed to chat with this person anymore.", view: self)
                
                return
                
            }
            else{
            
                isFromContact = false
                
            }
            
            
            
            
        
        }
         else if fullData.objectAtIndex(indexPath.section).firstObject!! .isKindOfClass(UserModel){
        
            isFromContact = true
        }
        
        
        isPerformSegue = true
        
        rowNo = indexPath
        
        performSegueWithIdentifier("chatVCsegue", sender: nil)

        
       
        
       
        
    }

    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        
        
        
        var fullData:NSMutableArray = NSMutableArray()
        if isFromFiltered == true{
            
            fullData = filteredData
        }
        else{
            
            fullData = allDataArray
            
        }
        
        if fullData.objectAtIndex(section).firstObject!! .isKindOfClass(XMPPMessageArchiving_Contact_CoreDataObject){
        
            return 0
        }
        else{
            
            return 36
        }
        
        
        
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView{
        
        let view:UIView  = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 36))
        
        let lbl: UILabel = UILabel(frame: CGRect(x: 0, y: 5, width: tableView.frame.size.width, height: 30))
        
        lbl.backgroundColor = UIColor(colorLiteralRed: 233.0/255.0, green: 86.0/255.0, blue: 102.0/255.0, alpha: 1.0)
        
        lbl.font = UIFont.systemFontOfSize(12)
        
        
        
        lbl.textColor = UIColor.whiteColor()
        
        lbl.textAlignment = NSTextAlignment.Center
        
        var fullData:NSMutableArray = NSMutableArray()
        if isFromFiltered == true{
            
            fullData = filteredData
        }
        else{
            
            fullData = allDataArray
            
        }
        
    
        if fullData.objectAtIndex(section).firstObject!! .isKindOfClass(XMPPMessageArchiving_Contact_CoreDataObject){
            
            lbl.text = "All Recent Chats"
            view.addSubview(lbl)
            
            return view
            
        }
       else if fullData.objectAtIndex(section).firstObject!! .isKindOfClass(UserModel){
            
            lbl.text = "My Quick Chat Contacts " + String(allContacts.count)
            
            view.addSubview(lbl)
            
            return view

            
        }
        
      return UIView(frame: CGRect.zero)
        
    }
    @IBAction func btnAddContactPressed(sender: AnyObject) {
    }
    
    override func shouldPerformSegueWithIdentifier(identifier: String, sender: AnyObject!) -> Bool {
        
        if identifier == "chatVCsegue"{
        
            if(isPerformSegue){
                return true
            }else{
                return false
            }
        }
        return true
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "chatVCsegue"{
        
            let chatVC  = segue.destinationViewController as! ChatViewController
            
             let isNetAvalible : Bool = Reachabilit.isConnectedToNetwork()
            
            var fullData:NSMutableArray = NSMutableArray()
            if isFromFiltered == true{
                
                fullData = filteredData
            }
            else{
                
                fullData = allDataArray
                
            }

            
            if isFromContact == false{
                

                if(isNetAvalible)
                {
                    
                    
                    
                    let messagesArr:NSMutableArray = fullData.objectAtIndex(rowNo!.section) as! NSMutableArray
                    
                    let message :XMPPMessageArchiving_Contact_CoreDataObject = messagesArr[(rowNo?.row)!] as! XMPPMessageArchiving_Contact_CoreDataObject

                    
                    let str = message.bareJidStr
                    if let comma = str.characters.indexOf("@")
                    {
                        let substr = str[str.startIndex..<comma]
                        chatVC.Jid = substr;
                        
                    }
                    else
                    {
                        chatVC.Jid = str;
                        self.navigationController!.pushViewController(chatVC, animated: false)
                    }
                    
                    chatVC.toUser = ""
                    
                }
                    
                else
                {
                    
                    
                }

                
            }
            else{
              
                
                
                
                
                if(isNetAvalible)
                {
                    
                    let contactsArray:NSMutableArray = fullData.objectAtIndex(rowNo!.section) as! NSMutableArray
                    
                    
                    let user = contactsArray.objectAtIndex(rowNo!.row) as! UserModel
                    
                    chatVC.Jid = user.phoneNumber as! String
                    
                   chatVC.toUser = ""
                
                }
                    
                else
                {
                    
                    
                }

                
                
                
            }
            
            container_view.hidden = false
            self.tabel.hidden = true
            viewHeader.hidden = true
            
        }

        }
    
    
    func hideChatForBack(){
        
        
        self.hideChat()
        getAllContacts()
    }
    
    func hideChat(){
        
        self.hideSearchBar()
        
        if container_view.hidden == false{
            
            
            for vc:UIViewController in self.childViewControllers{
                
                if vc .isKindOfClass(ChatViewController){
                    
                    vc.willMoveToParentViewController(self)
                    vc.didMoveToParentViewController(self)
                    vc.view.removeFromSuperview()
                    vc.removeFromParentViewController()
                
                    
                }
            }
            
            
    
            container_view.hidden = true
            self.tabel.hidden = false
            viewHeader.hidden = false
            
            
            
        }
                

    }
    
    
   
    
    
    func getAllContacts(){
        
        
        
        MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        
        let str = kBaseServerUrl + "getcontact"
        let fullUrl  = NSURL(string: str)
        
        let userid = NSUserDefaults.standardUserDefaults().objectForKey(kUserid) as? String
        
        print(userid)
        
        let newPost = ["user_id":userid!]as [String:AnyObject]
        
        
        
        let request = Alamofire.request(.POST, fullUrl!, parameters: newPost, encoding: .URL)
        request.responseJSON
            { response in
                
                if let anError = response.result.error
                {
                    
                    
                    
                    dispatch_async(dispatch_get_main_queue(),
                        {
                            
                            CommomMethods.showAlert("something went wrong please check your internet connection and try again later", view: self)
                            MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
                            
                            
                        }
                    )
                    
                    print("error calling POST on /posts")
                    
                    
                }
                else if let data = response.data as NSData!
                {
                    
                    
                    let json = JSON(data:data)
                    
                    let status = json["status"].stringValue
                    
                    if (status == "ok")
                    {
                        
                        self.allContacts.removeAllObjects()
                        
                        let pendingRequests : Array = json["response"].arrayValue
                        
                        print(pendingRequests)
                        
                        if pendingRequests.count > 0{
                            
                            
                            for d in pendingRequests{
                                
                                
                                let user = UserModel()
                                
                                if let str:NSString = d["id"].string{
                                    
                                    user.userid = str
                                    
                                }
                                if let str:NSString = d["username"].string{
                                    
                                    user.username = str
                                    
                                }
                                
                                
                                if let str:NSString = d["email"].string{
                                    
                                    user.email = str
                                    
                                }
                                if let str:NSString = d["dob"].string{
                                    
                                    user.dob = str
                                }
                                if let str:NSString = d["phone_number"].string{
                                    
                                    user.phoneNumber = str
                                }
                                if let str:NSString = d["video_path"].string{
                                    
                                    user.videoUrl = str
                                    
                                }
                                
                                self.allContacts.addObject(user)
                                
                            }
                            
                            
                            
                        }
                        
                        if self.allContacts.count > 0{
                        
                            self.getData()
                        }
                       
                        

                        
                        
                    }
                    
                    
                    dispatch_async(dispatch_get_main_queue(),
                        {
                            
                            MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
                            
                            
                        }
                    )
                    
                    
                }
                
                
        }
        
        
        
    }

    
    func getData(){
        
        
        
        allDataArray .removeAllObjects()
        
        
        
        var messagesArr :NSMutableArray = NSMutableArray()
        
        messagesArr = (appDel.xmppcon?.returnMessageThreads())!
        
        if messagesArr.count>0{
            
            allDataArray.addObject(messagesArr)
        }
        
        if allContacts.count>0 {
            
            allDataArray.addObject(allContacts)
        }
        
        tabel.reloadData()
        
        
        
    }

    func blockContact(sender:UIButton){
        
        let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: "Please select", message: "", preferredStyle: .ActionSheet)
        
        let cancelActionButton: UIAlertAction = UIAlertAction(title: "Cancel", style: .Cancel) { action -> Void in
            print("Cancel")
        }
        actionSheetControllerIOS8.addAction(cancelActionButton)
        
        let saveActionButton: UIAlertAction = UIAlertAction(title: "Block Contact", style: .Default)
        { action -> Void in
           
            let touchPoint = sender.convertPoint(CGPoint.zero, toView: self.tabel)
            
            let indexPath = self.tabel .indexPathForRowAtPoint(touchPoint)
            
            let arr:NSMutableArray = (self.allDataArray.objectAtIndex((indexPath?.section)!) as? NSMutableArray)!
            
            let user:UserModel = arr.objectAtIndex((indexPath?.row)!) as! UserModel
            
            
            let str = kBaseServerUrl + "b_contact"
            let fullUrl  = NSURL(string: str)
            
            let newPost = ["contact":(user.phoneNumber as? String)!,"user_id":NSUserDefaults.standardUserDefaults().objectForKey(kUserid)!] as [String:AnyObject]
            
            
            let request = Alamofire.request(.POST, fullUrl!, parameters: newPost, encoding: .URL)
            request.responseJSON
                { response in
                    
                    if let anError = response.result.error
                    {
                        
                        
                    }
                    else if let data = response.data as NSData!
                    {
                        
                        
                        let json = JSON(data:data)
                        
                        let status = json["status"].stringValue
                        
                        if (status == "ok")
                        {
                            
                            
                        }
                        
                        
                        
                    }
                    
                    
            }
            
            
            
            
            
            
            
            
            arr.removeObjectAtIndex((indexPath?.row)!)
            
            
            if arr.count > 0 {
                
                
                self.allDataArray.replaceObjectAtIndex((indexPath?.section)!, withObject: arr)
                
                self.tabel.beginUpdates()
                self.tabel!.deleteRowsAtIndexPaths([indexPath!], withRowAnimation: UITableViewRowAnimation.Fade)
                self.tabel.endUpdates()
                
                
                
            }
            else{
                
                self.allDataArray.removeObjectAtIndex((indexPath?.section)!)
                
                self.tabel.reloadData()
                
                
            }

        }
        actionSheetControllerIOS8.addAction(saveActionButton)
        
        
        self.presentViewController(actionSheetControllerIOS8, animated: true, completion: nil)
        
        
        
        
        
    }
    
    @IBAction func btnSearchPressed(sender: AnyObject) {
        
        showSearchBar()
    }
    
    
    func showSearchBar() {
        
        searchBar.hidden = false
        searchBar.becomeFirstResponder()
       
    }
    
    func hideSearchBar() {
        
        isFromFiltered = false
        
        searchBar.text = ""
        
        searchBar.resignFirstResponder()
     
        searchBar.hidden = true
        
        self.tabel .reloadData()
    }
    
    
    
    //MARK: UISearchBarDelegate
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        hideSearchBar()
    }
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        
        hideSearchBar()
        
    }
    func searchBar(searchBar: UISearchBar, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        
        isFromFiltered = true
        
        var txtAfterUpdate:NSString = searchBar.text! as NSString
        
        txtAfterUpdate = txtAfterUpdate.stringByReplacingCharactersInRange(range, withString: text)
        
        print(txtAfterUpdate.lowercaseString)
        
        var filteredContacts:NSArray = NSArray()
        
        var filteredChats:NSArray = NSArray()
        
        var arrContacts:[UserModel] = NSArray() as! [UserModel]
        
        var arrChats:[XMPPMessageArchiving_Contact_CoreDataObject] = NSArray() as! [XMPPMessageArchiving_Contact_CoreDataObject]
        
        
        for index in 0..<allDataArray.count {
        
            
            if allDataArray.objectAtIndex(index).firstObject!! .isKindOfClass(XMPPMessageArchiving_Contact_CoreDataObject){
                
            
                arrChats = allDataArray.objectAtIndex(index) as! NSArray as! [XMPPMessageArchiving_Contact_CoreDataObject]
            
            }
            
    else if allDataArray.objectAtIndex(index).firstObject!!.isKindOfClass(UserModel){
            
              arrContacts = allDataArray.objectAtIndex(index) as! NSArray as! [UserModel]
                
              
                
            }
            
        }
        
        filteredContacts  = arrContacts.filter( { (user: UserModel) -> Bool in
            return user.username!.containsString(txtAfterUpdate.lowercaseString as String)
        })
        
        filteredChats  = arrChats.filter( { (user: XMPPMessageArchiving_Contact_CoreDataObject) -> Bool in
            
            let userName :String?
            let str = user.bareJidStr
            if let comma = str.characters.indexOf("@")
            {
                let substr = str[str.startIndex..<comma]
                userName = substr
            }
            else
            {
                userName = str
            }
            
            
            var usser:UserModel?
            
            for index in 0..<allContacts.count {
                
                let userr:UserModel = (allContacts.objectAtIndex(index) as? UserModel)!
                
                if userr.phoneNumber == userName{
                    
                    usser = userr
                    break
                }
                
            }
            
            return usser!.username!.containsString(txtAfterUpdate.lowercaseString as String)
            
           
        })
        
        
        filteredData.removeAllObjects()
        
        if filteredChats.count > 0 {
            
            filteredData.addObject(filteredChats.mutableCopy())
        }
        
        
        if filteredContacts.count > 0 {
            
            filteredData.addObject(filteredContacts.mutableCopy())
        }
        
        
        
        self.tabel .reloadData()
        
        
        
        return true
    }

    
    
}
