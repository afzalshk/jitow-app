//
//  ProfileContainerViewController.swift
//  JITOW
//
//  Created by Irfan Malik on 2/18/16.
//  Copyright © 2016 LC. All rights reserved.
//

import UIKit
import MobileCoreServices
import AVFoundation
import MediaPlayer

class ProfileContainerViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,CustomOverlayDelegate,videoMergedDelegate {

    @IBOutlet weak var setting_Contaier: UIView!
    @IBOutlet weak var notification_Container: UIView!
    @IBOutlet var collectionOfLabels: Array<UILabel>?
    
    @IBOutlet weak var addMe_Container: UIView!
    
    
    @IBOutlet weak var myMomentsContainer: UIView!
    var isPerformSegue:Bool = false
    
    var picker = UIImagePickerController()
    var mediaUrl:NSURL?
    var thumbnail_Url:NSURL?
    var timer:NSTimer?
    var duration:Int = 0
    var moviePlayerCntroller:MPMoviePlayerViewController!
    var txtCaption:String?
    var isVideoOpened:Bool = false
    
    @IBOutlet weak var viewBottom: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        notification_Container.hidden = true
        
        addMe_Container.hidden = true
        
        setting_Contaier.hidden = true
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("hideMoment"), name:"segmentTapped", object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("popBack"), name:"indexChanged", object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("backToProfile"), name:"momentShared", object: nil)
        
        
        viewBottom.layer.cornerRadius = 5.0
        viewBottom.clipsToBounds = true
        
        if UIScreen.mainScreen().bounds.size.height > 568 {
            
            if #available(iOS 8.2, *) {
                
                for lbl:UILabel in collectionOfLabels!{
                    
                    
                    lbl.font = UIFont.systemFontOfSize(9, weight: UIFontWeightMedium)
                }
                
                
            } else {
                // Fallback on earlier versions
                
                for lbl:UILabel in collectionOfLabels!{
                    
                    
                    lbl.font = UIFont.systemFontOfSize(9)
                }
                
            }
        }
        else{
            
            if #available(iOS 8.2, *) {
                
                for lbl:UILabel in collectionOfLabels!{
                    
                    
                    lbl.font = UIFont.systemFontOfSize(7, weight: UIFontWeightMedium)
                }
                
            } else {
                // Fallback on earlier versions
                
                for lbl:UILabel in collectionOfLabels!{
                    
                    
                    lbl.font = UIFont.systemFontOfSize(7)
                }
                
                
            }
            
            
        }

        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(animated: Bool) {
        
        self.navigationController?.navigationBarHidden = true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func changeTabViews(sender: UIButton) {
        
        
            if(sender.tag == 100){
                
                if setting_Contaier.hidden == true{
                    isVideoOpened = false
                    setting_Contaier.hidden = false
                    notification_Container.hidden = true
                    
                    addMe_Container.hidden = true
                    
                    myMomentsContainer.hidden = true
                    
                    hideAddedMe()
                    hideNotifications()
                    
                    isPerformSegue = true
                    
                    self.performSegueWithIdentifier("toSetting", sender: self)
                }
                
                
            }
            else if(sender.tag == 101){
                
                if notification_Container.hidden == true{
                    isVideoOpened = false
                    hideSetting()
                    hideAddedMe()
                    setting_Contaier.hidden = true
                    notification_Container.hidden = false
                    addMe_Container.hidden = true
                    myMomentsContainer.hidden = true
                    
                    self.performSegueWithIdentifier("toNotification", sender: self)
                }
                
            }
            else if(sender.tag == 102){
                
                if isVideoOpened == false{
                
                    self.showVideControl()
                    
                }
                
                
                
            }
            else{
                
                if addMe_Container.hidden == true{
                    
                     hideSetting()
                    hideNotifications()
                    isVideoOpened = false
                    setting_Contaier.hidden = true
                    notification_Container.hidden = true
                    addMe_Container.hidden = false
                    myMomentsContainer.hidden = true
                    isPerformSegue = true
                    
                    self.performSegueWithIdentifier("AddMe", sender: self)
                    
                }
                
               
                
                
            }
        
        
        
    }
    override func shouldPerformSegueWithIdentifier(identifier: String, sender: AnyObject!) -> Bool {
        
        if identifier == "AddMe" || identifier == "toSetting" || identifier == "toNotification"{
            
            if(isPerformSegue){
                return true
            }else{
                return false
            }
        }
        return true
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "toSharing"{
            
            print(segue.destinationViewController)
        
            let vc = segue.destinationViewController as! SharedMomentVC
            
            vc.videoUrl = mediaUrl
            vc.captionText = txtCaption
            vc.thumbnailUrl = thumbnail_Url
        
        }
    }

    func showVideControl(){
        
        isVideoOpened = true
        if UIImagePickerController.availableCaptureModesForCameraDevice(.Rear) != nil {
            picker = UIImagePickerController() //make a clean controller
            picker.allowsEditing = false
            picker.sourceType = UIImagePickerControllerSourceType.Camera
            picker.mediaTypes = [kUTTypeMovie as String];
            picker.cameraDevice = .Rear
            picker.cameraCaptureMode = .Video
            picker.showsCameraControls = false
            picker.videoMaximumDuration = 15.0
            picker.cameraFlashMode = UIImagePickerControllerCameraFlashMode.Off
            
            self.picker.delegate = self
            
            //customView stuff
            let customViewController = CustomOverlayViewController(
                nibName:"CustomOverlayController",
                bundle: nil
            )
            let customView:CustomOverlayView = customViewController.view as! CustomOverlayView
            customView.frame = self.picker.view.frame
            customView.delegate = self
            picker.modalPresentationStyle = .FullScreen
            presentViewController(picker,
                                  animated: true,
                                  completion: {
                                    self.picker.cameraOverlayView = customView
                }
            )
            
        }
        

    }

    // CustomOverlayView Delegate
    
    func imagePickerController(
        picker: UIImagePickerController,
        didFinishPickingMediaWithInfo info: [String : AnyObject])
    {
        
       
        removeTimer()
        
        if let pickedVideo:NSURL = (info[UIImagePickerControllerMediaURL] as? NSURL) {
            
            let customView = picker.cameraOverlayView as! CustomOverlayView
            
            customView.btnCapture.selected = false
            
            mediaUrl = pickedVideo
            
            
            var err: NSError? = nil
            let asset = AVURLAsset(URL:mediaUrl!, options: nil)
            let imgGenerator = AVAssetImageGenerator(asset: asset)
            do {
                imgGenerator.appliesPreferredTrackTransform = true
                let cgImage = try imgGenerator.copyCGImageAtTime(CMTimeMake(0, 1), actualTime: nil)
                
                // !! check the error before proceeding
                let uiImage = UIImage(CGImage: cgImage)
                
                
                    
                    if let data = UIImagePNGRepresentation(uiImage) {
                        let filename = getDocumentsDirectory().stringByAppendingPathComponent("image.png")
                        data.writeToFile(filename, atomically: true)
                        
                        thumbnail_Url = NSURL(fileURLWithPath: filename)
                    }
               
            } catch {
                
            }
            
            
            
            
            
        }
    }
    //What to do if the image picker cancels.
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true,
                                      completion: nil)
    }
    
    func didCancel(sender:UIButton,overlayView:CustomOverlayView){
        
        mediaUrl = nil
        picker.dismissViewControllerAnimated(true,
                                             completion: nil)
        
        isVideoOpened = false
        
        
    }
    func didShoot(sender:UIButton,overlayView:CustomOverlayView){
        
        if sender.selected == true{
            
            removeTimer()
            picker.stopVideoCapture()
        }
        else{
            
            picker.startVideoCapture()
            
          timer =   NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: Selector("changeDuration"), userInfo: nil, repeats:true)
            
            
            
            
            
        }
        
    }
    func removeTimer(){
        
        timer?.invalidate()
        timer = nil
        duration = 0;
    }
    
    func changeDuration(){
        
        duration = duration + 1
        
        if duration > 15{
            
            removeTimer()
            
            return
        }
        
         let customView = picker.cameraOverlayView as! CustomOverlayView
        
        if duration > 9{
         
            customView.lblDuration.text = "00." + "\(duration)"
        }
        else{
            
            customView.lblDuration.text = "00.0" + "\(duration)"
        }
        
    }
    func didOnOffFlash(sender:UIButton,overlayView:CustomOverlayView){
        
        if sender.selected == true{
            
            picker.cameraFlashMode = UIImagePickerControllerCameraFlashMode.Off
        }
        else{
            
            picker.cameraFlashMode = UIImagePickerControllerCameraFlashMode.On
            
            
        }
        
        
    }
    
    func clickTextView(sender:UIButton,overlayView:CustomOverlayView){
        
        
        if sender.selected == true{
            
            overlayView.txtViewCaption.userInteractionEnabled = false
            overlayView.txtViewCaption .resignFirstResponder()
        }
        else{
            
            
            if mediaUrl != nil{
                
                overlayView.txtViewCaption.userInteractionEnabled = true
                overlayView.txtViewCaption .becomeFirstResponder()
                
            }
            else{
                
                CommomMethods.showAlert("please capture video to add caption", view: picker)
                
                
            }
            
            
            
        }
        
        
        
    }
    
    func moveToFrontCam(sender:UIButton,overlayView:CustomOverlayView){
        
        if sender.selected == true{
        
            picker.cameraDevice = .Rear
        }
        else{
            
          picker.cameraDevice = .Front
        }
    }

    func previewVideo(sender:UIButton,overlayView:CustomOverlayView){
        
        if mediaUrl != nil{
            
            if overlayView.txtViewCaption.text == ""{
            
                if timer != nil {
                    
                    return
                }
                self.videoTextMerged(mediaUrl, withBool: true)
                
            }
            
            else{
                
                let m:MergeVideoTextVC = MergeVideoTextVC()
                m.delegate = self
                m.MixVideoWithText(mediaUrl, withString: overlayView.txtViewCaption.text, withCheck:true)
                
            }
            
        }
        else{
            
            CommomMethods.showAlert("please capture video to preview", view: picker)
            
            
        }
        

        
    }
    

    func moveToShare(sender:UIButton,overlayView:CustomOverlayView){
        
        if sender.selected == false{
            
            if mediaUrl != nil{
                
                
                if overlayView.txtViewCaption.text == ""{
                    
                    if timer != nil {
                        
                        return
                    }
                    self.videoTextMerged(mediaUrl, withBool: false)
                    
                }
                    
                else{
                    
                    let m:MergeVideoTextVC = MergeVideoTextVC()
                    m.delegate = self
                    m.MixVideoWithText(mediaUrl, withString: overlayView.txtViewCaption.text, withCheck:false)
                    
                }
                
            }
            else{
                
                CommomMethods.showAlert("please capture video to Share", view: picker)
            }
            

            
        }
        
        
        sender.selected = !sender.selected
        
    }
    
    func videoTextMerged(url: NSURL!, withBool isFromPreview: Bool) {
        
        if isFromPreview == true{
            
            moviePlayerCntroller = MPMoviePlayerViewController()
            
            
            
            moviePlayerCntroller = MPMoviePlayerViewController(contentURL:  url)
            
            
            
            
            moviePlayerCntroller?.moviePlayer.fullscreen = true
            moviePlayerCntroller?.moviePlayer.controlStyle = .Fullscreen
            
            moviePlayerCntroller.moviePlayer.prepareToPlay()
            
            picker.presentMoviePlayerViewControllerAnimated(moviePlayerCntroller)
            
            
            moviePlayerCntroller.moviePlayer.play()
        }
        else{
            
            mediaUrl = url
            
            picker.dismissViewControllerAnimated(true,
                                                 completion: nil)
            
            isVideoOpened = false
            
            self.performSegueWithIdentifier("toSharing", sender: self)
            
            
            
        }
        
    }
    
    func hideMoment(){
        
        
        
        if myMomentsContainer.hidden == true{
            
            hideSetting()
            hideAddedMe()
            hideNotifications()
            
            myMomentsContainer.hidden = false
            setting_Contaier.hidden = true
            
            notification_Container.hidden = true
            
            addMe_Container.hidden = true
            
            
        }
            
        
        
        
        
        
        
    }
    
    
    func popBack(){
        
        self.willMoveToParentViewController(self.parentViewController)
        self.didMoveToParentViewController(self.parentViewController)
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
        
    }
    
    
    func hideAddedMe(){
        
        for vc:UIViewController in self.childViewControllers{
            
            if vc .isKindOfClass(AddmeViewController){
                
                vc.willMoveToParentViewController(self)
                vc.didMoveToParentViewController(self)
                vc.view.removeFromSuperview()
                vc.removeFromParentViewController()
                
                
            }
        }
    }
    
    func hideSetting(){
        
        for vc:UIViewController in self.childViewControllers{
            
            if vc .isKindOfClass(MyprofileViewController){
                
                vc.willMoveToParentViewController(self)
                vc.didMoveToParentViewController(self)
                vc.view.removeFromSuperview()
                vc.removeFromParentViewController()
                
                
            }
        }
        
        
        

        
    }
    
    func hideNotifications(){
        
        for vc:UIViewController in self.childViewControllers{
            
            if vc .isKindOfClass(NotificationViewController){
                
                vc.willMoveToParentViewController(self)
                vc.didMoveToParentViewController(self)
                vc.view.removeFromSuperview()
                vc.removeFromParentViewController()
                
                
            }
        }
        
        
        
        
        
    }
    
    func backToProfile(){
        
       
        
        if myMomentsContainer.hidden == false{
            
            NSNotificationCenter.defaultCenter().postNotificationName("reloadData", object: nil)
        
        }
    }
    
    func getDocumentsDirectory() -> NSString {
        let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
}

