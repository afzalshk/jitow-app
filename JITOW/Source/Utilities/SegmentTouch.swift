//
//  SegmentTouch.swift
//  JITOW
//
//  Created by Mac103 on 4/27/16.
//  Copyright © 2016 LC. All rights reserved.
//

import Foundation

class SegmentedControlExistingSegmentTapped : UISegmentedControl
{
    // captures existing selected segment on touchesBegan
    var oldValue : Int!
    
    override func touchesBegan( touches: Set<UITouch>, withEvent event: UIEvent? )
    {
        self.oldValue = self.selectedSegmentIndex
        super.touchesBegan( touches , withEvent: event )
    }
    
    // This was the key to make it work as expected
    override func touchesEnded( touches: Set<UITouch>, withEvent event: UIEvent? )
    {
        super.touchesEnded( touches , withEvent: event )
        
        if self.oldValue == self.selectedSegmentIndex
        {
            if self.oldValue == 1{
                
                NSNotificationCenter.defaultCenter().postNotificationName("segmentTapped", object: nil)
            }
            
        }
    }
}