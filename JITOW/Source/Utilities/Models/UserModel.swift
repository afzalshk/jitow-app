//
//  UserModel.swift
//  JITOW
//
//  Created by Yosmite on 2/22/16.
//  Copyright © 2016 LC. All rights reserved.
//

import Foundation

class UserModel: NSObject {
    
    var username: NSString?
    var userid: NSString?
    var password: NSString?
    var dob: NSString?
    var email: NSString?
    var phoneNumber: NSString?
    var videoUrl: NSString?
    var totalContacts: NSString?
    var isHidden: Bool = false
    var momentsArray: NSMutableArray = NSMutableArray()
    static let sharedInstance = UserModel()
    
   
    
}

class VideoModel: NSObject {
    
    var caption: NSString?
    var thumbnail_url: NSString?
    var date: NSString?
    var emojiCount: NSString?
    var sceneCount: NSString?
    var videoUrl: NSString?
    var emojiList: NSMutableArray = NSMutableArray()
    var video_id: NSString?
    var senderName: NSString?

    
    
    
}