//
//  XMPPConnection.h
//  iPhoneXMPP
//
//  Created by Mac101 on 8/11/15.
//  Copyright (c) 2015 XMPPFramework. All rights reserved.
//


#import "GCDAsyncSocket.h"
#import "XMPP.h"
#import "XMPPLogging.h"
#import "XMPPReconnect.h"
#import "XMPPCapabilitiesCoreDataStorage.h"
#import "XMPPRosterCoreDataStorage.h"
#import "XMPPvCardAvatarModule.h"
#import "XMPPvCardCoreDataStorage.h"
#import "XMPPMessageArchivingCoreDataStorage.h"
#import "DDLog.h"
#import "DDTTYLogger.h"
#import <CFNetwork/CFNetwork.h>



@interface XMPPConnection : NSObject <NSFetchedResultsControllerDelegate>

{
    XMPPStream *xmppStream;
    XMPPReconnect *xmppReconnect;
    XMPPRoster *xmppRoster;
    XMPPRosterCoreDataStorage *xmppRosterStorage;
    XMPPvCardCoreDataStorage *xmppvCardStorage;
    XMPPvCardTempModule *xmppvCardTempModule;
    XMPPvCardAvatarModule *xmppvCardAvatarModule;
    XMPPCapabilities *xmppCapabilities;
    XMPPCapabilitiesCoreDataStorage *xmppCapabilitiesStorage;
    
    	BOOL customCertEvaluation;
    NSFetchedResultsController *fetchedResultsController;

    
}

@property(strong, nonatomic) NSString * reciverJid;
@property(strong, nonatomic) XMPPStream * senderStream;


- (void)FetchFriends;

-(void)sendMessage :(NSString *)msg and :(NSString *)jid  andwithstream:(XMPPStream *)stream withImage:(UIImage *)imagePic andtime:(NSString *)time andtype:(NSString *)type;

-(void)sendMessage :(NSString *)msg and :(NSString *)jid  andwithstream:(XMPPStream *)stream;
-(void)recievemesages:(NSString *)userID andJid:(XMPPStream *)stream;
-(void)setnewMessage:(XMPPStream *)stream;
-(NSArray *)loadarchivemsg :(NSString *)userID andJid:(XMPPStream *)stream withChatBool:(BOOL)isYes;
-(void)setupEarySetting :(XMPPStream *)stream;
-(void)SendFriendRequest :(NSString *)userID withNickname:(NSString *)optionalName;
- (NSFetchedResultsController *)fetchedResultsController;
-(void)accceptFriendRequest:(NSString *)fromJid;
-(void)ReciveMessage:(XMPPStream *)sender didReceiveMessage:(XMPPMessage *)message;
-(NSString *)returnMessageId :(XMPPMessage *)message;
-(void)addMessageStatus:(NSString *)jid andwith:(NSString *)msgid;
-(void)sendComosing:(NSString *)jid  andwithstream:(XMPPStream *)stream;
-(void)retreveMessage:(NSString *)jid  andwithstream:(XMPPStream *)stream andmsgID:(NSString *)msgId;
-(NSMutableArray *)returnMessageThreads;


#pragma mark  offlineDataManaged
-(void)addRosterUsers:(NSArray*)arr;
-(NSArray *)getRosterUser;
-(void)addLoginUserProfile :(NSDictionary *)userProfile;
-(NSArray *)getLoginUserProfile;
-(NSArray *)getUserProfile:(NSString*)jid;


-(void)deleteThread:(NSString *)jid;
-(void)changeImageTimer:(NSString *)msgID TimeValue:(NSString *)time;


#pragma mark  NewMethods

-(void)sendTextMessage :(NSString *)msg and:(NSString *)jid audioLocalPath:(NSString *)localurl andmsgTime:(NSString *)time andmsgID:(NSString*)msgID andwithstream:(XMPPStream *)stream andwithToUser:(NSString*)sendToUser withFromUser:(NSString*)fromUser;



-(void)sendContact :(NSString *)name andEmail:(NSString *)email andPhone:(NSString *)phone  and:(NSString *)jid andmsgID:(NSString*)msgID andwithstream:(XMPPStream *)stream;

-(void)sendImageWithUrl:(NSString *)msg andjid:(NSString *)jid  andwithstream:(XMPPStream *)stream withImageUrl:(NSString *)ImageUrl iththumbnailImage:(NSString *)thumbnail andtime:(NSString *)time andmsgID:(NSString *)msgID withToUser:(NSString*)user withFromUser:(NSString*)fromUser;

-(void)sendMapWithUrl:(NSString *)msg andjid:(NSString *)jid  andwithstream:(XMPPStream *)stream withImageUrl:(NSString *)ImageUrl iththumbnailImage:(NSString *)thumbnail andtime:(NSString *)time andmsgID:(NSString *)msgID andlocation:(NSString *)locationUrl;

-(void)sendVideoData:(NSString *)msgID andjid:(NSString *)jid  andwithstream:(XMPPStream *)stream withImage:(UIImage *)imagePic andtime:(NSString *)time  andvideoUrl:(NSString *)videoUrl andtype:(NSString *)type withtoUser:(NSString*)sendToUser withFrom:(NSString*)fromUser;


-(void)sendVideoWithUrl:(NSString *)msg andjid:(NSString *)jid  andwithstream:(XMPPStream *)stream withImageUrl:(NSString *)VideoUrl  localVideoPath:(NSString *)localVideoUrl iththumbnailImage:(UIImage *)thumbnail andtime:(NSString *)time andmsgID:(NSString *)msgID withToUser:(NSString*)user withFromUser:(NSString*)fromUser;



-(void)sendaudioWithUrl:(NSString *)msg andjid:(NSString *)jid  andwithstream:(XMPPStream *)stream withaudioUrl:(NSString *)audio  andtime:(NSString *)time audioLocalPath:(NSString *)localPath  andmsgID:(NSString *)msgID;


-(void)sendImageData:(NSString *)msgID andjid:(NSString *)jid  andwithstream:(XMPPStream *)stream withImage:(UIImage *)imagePic andtime:(NSString *)time andtype:(NSString *)type;




//Get SingleUser
-(NSArray *)getSingleUser:(NSString*)jid;


//Local Message Save
-(void)addSendingImagewithUrl :(NSString *)msgId andjid:(NSString *)jid  withImage:(NSString *)imagePic andtime:(NSString *)time andType:(NSString*)type withToUser:(NSString*)user withFromUser:(NSString*)fromUser;
-(void)deleteSendingMessage:(NSString *)msgId;

//UItabelVirewMethod

- (NSInteger)sectioncount :(NSFetchedResultsController *)fetchedResultsControlle;
-(NSInteger)numberofRows :(NSFetchedResultsController *)fetchedResultsControlle  andnumberOfRowsInSection:(NSInteger)sectionIndex;
-(XMPPUserCoreDataStorageObject *)nameforrow :(NSFetchedResultsController *)fetchedResultsControlle andnumberOfRowsInSectio:(NSIndexPath *)indexPath;


#pragma mark - Get Driving Status /Jawad/
-(void)initializeLocationManager:(NSString *)jid andwithStream:(XMPPStream *)stream;

#pragma mark - Get User Status
-(void)getUserPresense:(NSString *)jid andStream:(XMPPStream *)stream;


@end
