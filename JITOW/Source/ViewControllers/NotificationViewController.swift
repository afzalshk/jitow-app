//
//  NotificationViewController.swift
//  JITOW
//
//  Created by Irfan Malik on 2/18/16.
//  Copyright © 2016 LC. All rights reserved.
//

import UIKit
import Alamofire
import MBProgressHUD
class NotificationViewController: UIViewController {
    
    var refreshControl: UIRefreshControl!
    @IBOutlet weak var tabel: UITableView!
    var selectedVideo:VideoModel?
    
    var allNotificationsArray:NSMutableArray = NSMutableArray()
    var isloadingMore:Bool = false
    
    var start:Int = 0
    var end:Int = 10
  
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addRefreshControl()
        activityIndicator.hidden = true
        getAllNotifications()
        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //MARK: - UitabelView
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return allNotificationsArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> NotificationTableViewCell
    {
        var cell:NotificationTableViewCell? = self.tabel.dequeueReusableCellWithIdentifier("Cell") as? NotificationTableViewCell
        if (cell == nil) {
            let nib:NSArray = NSBundle.mainBundle().loadNibNamed("NotificationTableViewCell", owner: self, options: nil)
            cell = nib.objectAtIndex(0) as? NotificationTableViewCell
        }
        cell?.selectionStyle = UITableViewCellSelectionStyle.None
        
        let v:VideoModel = allNotificationsArray.objectAtIndex(indexPath.row) as! VideoModel
        
        cell?.lblText.text = (v.senderName as? String)!  + "shared a moment with you"
        
        
        return cell!
    }
    

    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
            let video:VideoModel = allNotificationsArray.objectAtIndex(indexPath.row) as! VideoModel
        
        
            selectedVideo = video
        
            self.performSegueWithIdentifier("toPlayVideo", sender: self)
        
    }
     func scrollViewDidScroll(scrollView: UIScrollView) {
        let offset = scrollView.contentOffset
        let bounds = scrollView.bounds
        let size = scrollView.contentSize
        let inset = scrollView.contentInset
        let y = CGFloat(offset.y + bounds.size.height - inset.bottom)
        let h = CGFloat(size.height)
        
        let reload_distance = CGFloat(30)
        if(y > (h + reload_distance)) {
            
            if isloadingMore == false{
                
                isloadingMore = true
                activityIndicator.hidden = false
                activityIndicator.startAnimating()
                start = allNotificationsArray.count
                end = start + 10
                getLoadMoreNotifications()
                
            }
            
            
            
        }
    }

    
    func addRefreshControl(){
        
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Refreshing data please wait")
        refreshControl.addTarget(self, action: "refresh:", forControlEvents: UIControlEvents.ValueChanged)
        tabel.addSubview(refreshControl)
    }
    
    func refresh(sender:AnyObject) {
        
        if isloadingMore == false{
        
            start = 0
            end = 10
            isloadingMore = true
            getLoadMoreNotifications()
        }
        
    
    }
    
   
    func getAllNotifications(){
        
        
        MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        
        let str = kBaseServerUrl + "g_notification"
        let fullUrl  = NSURL(string: str)
        
        let userid = NSUserDefaults.standardUserDefaults().objectForKey(kUserid) as? String
        
        
        
        let newPost = ["user_id":userid!,"start":start,"end":end]as [String:AnyObject]
        
        
        
        let request = Alamofire.request(.POST, fullUrl!, parameters: newPost, encoding: .URL)
        request.responseJSON
            { response in
                
                if let anError = response.result.error
                {
                    
                    
                    
                    dispatch_async(dispatch_get_main_queue(),
                        {
                            
                            CommomMethods.showAlert("something went wrong please check your internet connection and try again later", view: self)
                            MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
                            
                            
                        }
                    )
                    
                    print("error calling POST on /posts")
                    print(anError)
                    
                }
                else if let data = response.data as NSData!
                {
                    
                    
                    let json = JSON(data:data)
                    
                    let status = json["status"].stringValue
                    
                    if (status == "ok")
                    {
                        
                        self.allNotificationsArray.removeAllObjects()
                        
                        let pendingRequests : Array = json["response"].arrayValue
                        
                        if pendingRequests.count > 0{
                            
                            
                            for d in pendingRequests{
                                
                                
                                let video = VideoModel()
                                
                                if let str:NSString = d["id"].string{
                                    
                                    video.video_id = str
                                    
                                }
                                if let str:NSString = d["file_path"].string{
                                    
                                    video.videoUrl = str
                                    
                                }
                                
                                
                                if let str:NSString = d["file_thumbnail_path"].string{
                                    
                                    video.thumbnail_url = str
                                    
                                }
                                if let str:NSString = d["caption"].string{
                                    
                                    video.caption = str
                                }
                                if let str:NSString = d["views"].string{
                                    
                                    video.sceneCount = str
                                }
                                if let str:NSString = d["emoji"].string{
                                    
                                    video.emojiCount = str
                                    
                                }
                                
                                if let str:NSString = d["date"].string{
                                    
                                    video.date = str
                                    
                                }

                                if let str:NSString = d["sender_name"].string{
                                    
                                    video.senderName = str
                                    
                                }

                                
                                if let str:NSString = d["emoji_num"].string {
                                    
                                    if str != ""{
                                        
                                        var newstr = str.stringByReplacingOccurrencesOfString("[", withString: "")
                                        
                                        newstr = newstr.stringByReplacingOccurrencesOfString("]", withString: "")
                                        
                                        
                                        
                                        let emojis = newstr.componentsSeparatedByString(",")
                                        
                                        for str1 in emojis{
                                            
                                            
                                            video.emojiList.addObject(str1)
                                            
                                            
                                        }
                                        
                                    }
                                    
                                    
                                }
                                
                                
                                
                                self.allNotificationsArray.addObject(video)
                                
                            }
                            
                            self.tabel.reloadData()
                            
                        }
                        
                        
                        
                        
                        
                        
                    }
                    
                    
                    dispatch_async(dispatch_get_main_queue(),
                        {
                            
                            MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
                            
                            
                        }
                    )
                    
                    
                }
                
                
        }
        
        
    }
    
    
    
    func getLoadMoreNotifications(){
        
        
        
        let str = kBaseServerUrl + "g_notification"
        let fullUrl  = NSURL(string: str)
        
        let userid = NSUserDefaults.standardUserDefaults().objectForKey(kUserid) as? String
        
        
        
        let newPost = ["user_id":userid!,"start":start,"end":end]as [String:AnyObject]
        
        
        
        let request = Alamofire.request(.POST, fullUrl!, parameters: newPost, encoding: .URL)
        request.responseJSON
            { response in
                
                if let anError = response.result.error
                {
                    
                    
                    
                    dispatch_async(dispatch_get_main_queue(),
                        {
                            
                          self.isloadingMore = false
                            
                            
                        }
                    )
                    
                    print("error calling POST on /posts")
                    print(anError)
                    
                }
                else if let data = response.data as NSData!
                {
                    
                    self.isloadingMore = false
                    let json = JSON(data:data)
                    
                    let status = json["status"].stringValue
                    
                    if (status == "ok")
                    {
                        
                        if self.start == 0 {
                            
                            self.allNotificationsArray.removeAllObjects()
                            
                            self.refreshControl.endRefreshing()
                        }
                        else{
                            
                            self.activityIndicator.hidden = true
                            self.activityIndicator.stopAnimating()
                            
                        }
                        
                        
                        let pendingRequests : Array = json["response"].arrayValue
                        
                        if pendingRequests.count > 0{
                            
                            
                            for d in pendingRequests{
                                
                                
                                let video = VideoModel()
                                
                                if let str:NSString = d["id"].string{
                                    
                                    video.video_id = str
                                    
                                }
                                if let str:NSString = d["file_path"].string{
                                    
                                    video.videoUrl = str
                                    
                                }
                                
                                
                                if let str:NSString = d["file_thumbnail_path"].string{
                                    
                                    video.thumbnail_url = str
                                    
                                }
                                if let str:NSString = d["caption"].string{
                                    
                                    video.caption = str
                                }
                                if let str:NSString = d["views"].string{
                                    
                                    video.sceneCount = str
                                }
                                if let str:NSString = d["emoji"].string{
                                    
                                    video.emojiCount = str
                                    
                                }
                                
                                if let str:NSString = d["date"].string{
                                    
                                    video.date = str
                                    
                                }
                                
                                if let str:NSString = d["sender_name"].string{
                                    
                                    video.senderName = str
                                    
                                }
                                
                                
                                if let str:NSString = d["emoji_num"].string {
                                    
                                    if str != ""{
                                        
                                        var newstr = str.stringByReplacingOccurrencesOfString("[", withString: "")
                                        
                                        newstr = newstr.stringByReplacingOccurrencesOfString("]", withString: "")
                                        
                                        
                                        
                                        let emojis = newstr.componentsSeparatedByString(",")
                                        
                                        for str1 in emojis{
                                            
                                            
                                            video.emojiList.addObject(str1)
                                            
                                            
                                        }
                                        
                                    }
                                    
                                    
                                }
                                
                                
                                
                                self.allNotificationsArray.addObject(video)
                                
                            }
                            
                            self.tabel.reloadData()
                            
                        }
                        
                        
                        
                        
                        
                        
                    }
                    
                    
                    dispatch_async(dispatch_get_main_queue(),
                        {
                            
                            MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
                            
                            
                        }
                    )
                    
                    
                }
                
                
        }
        
        
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "toPlayVideo"{
            
            let vc:PlayVideoVC = segue.destinationViewController as! PlayVideoVC
            
            vc.video = selectedVideo
            
        }
    }
    
    

    
}
