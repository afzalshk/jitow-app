//
//  ViewController.swift
//  JITOW
//
//  Created by Yosmite on 2/9/16.
//  Copyright © 2016 LC. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var btnRegister: UIButton!
    @IBOutlet weak var btnLogin: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        btnRegister.layer.cornerRadius = 10
        btnRegister.layer.borderWidth = 1.0
        btnRegister.layer.borderColor = UIColor(colorLiteralRed: 42.0/255.0, green: 81.0/255.0, blue: 101.0/255.0, alpha: 1.0).CGColor
        
        btnLogin.layer.borderWidth = 1.0
        btnLogin.layer.cornerRadius = 10
        
        btnLogin.layer.borderColor = UIColor(colorLiteralRed: 42.0/255.0, green: 81.0/255.0, blue: 101.0/255.0, alpha: 1.0).CGColor
        
        
   
        
        
    }
    
    override func viewWillAppear(animated: Bool) {
        
        self.navigationController?.navigationBarHidden = true
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


   
}

