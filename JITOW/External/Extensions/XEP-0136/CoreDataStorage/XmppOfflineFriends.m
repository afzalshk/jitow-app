//
//  XmppOfflineFriends.m
//  yupApp
//
//  Created by Irfan Malik on 9/17/15.
//  Copyright (c) 2015 LC. All rights reserved.
//

#import "XmppOfflineFriends.h"


@implementation XmppOfflineFriends

@dynamic bareJidStr;
@dynamic subscriptionType;
@dynamic image;
@dynamic pin;
@dynamic message;
@dynamic fullname;
@dynamic status;

@end
