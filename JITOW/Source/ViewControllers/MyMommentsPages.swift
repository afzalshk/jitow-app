//
//  MyMommentsPages.swift
//  JITOW
//
//  Created by Mac103 on 4/28/16.
//  Copyright © 2016 LC. All rights reserved.
//

import Foundation
import UIKit

class MyMommentsPages: UIViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    var pageViewController : UIPageViewController!
    
    var pageTitles:NSMutableArray = NSMutableArray()
    
    
    
    override func viewDidLoad() {
        
        if self.pageTitles.count > 0{
        
            setFirstState()
        }
        
        
    }
    
    
    func setFirstState() {
        
        pageViewController = self.storyboard?.instantiateViewControllerWithIdentifier("PageViewController") as! UIPageViewController
        self.pageViewController.dataSource = self
        
        let pageContentViewController = self.viewControllerAtIndex(0)
        self.pageViewController.setViewControllers([pageContentViewController!], direction: UIPageViewControllerNavigationDirection.Forward, animated: true, completion: nil)
        
        self.pageViewController.view.frame = CGRectMake(0, 0, self.view.frame.width, self.view.frame.height+40)
        self.addChildViewController(pageViewController)
        self.view.addSubview(pageViewController.view)
        self.pageViewController.didMoveToParentViewController(self)
    }
    
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
    
    var index = (viewController as! MyMomentsContentVC).pageIndex!
    index += 1
    if(index == self.pageTitles.count){
    return nil
    }
    return self.viewControllerAtIndex(index)
    
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
    
    var index = (viewController as! MyMomentsContentVC).pageIndex!
    if(index == 0){
    return nil
    }
    index -= 1
    return self.viewControllerAtIndex(index)
    
    }
    
    func viewControllerAtIndex(index : Int) -> UIViewController? {
    if((self.pageTitles.count == 0) || (index >= self.pageTitles.count)) {
    return nil
    }
    let pageContentViewController = self.storyboard?.instantiateViewControllerWithIdentifier("MyMomentsContentVC") as! MyMomentsContentVC
    
    
    pageContentViewController.videoObj = self.pageTitles[index] as! VideoModel
    pageContentViewController.pageIndex = index
    return pageContentViewController
  
    }
    
    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int {
    return pageTitles.count
    }
    
    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int {
    return 0
    }
}